export CRAYPE_LINK_TYPE=dynamic
module purge
module load PrgEnv-cray
module load amd-mixed/5.3.0
module load craype-x86-trento
spack load /3nwpk7defoaetpsty5uexjgwa3blll3u pdiplugin-user-code pdiplugin-set-value pdiplugin-decl-hdf5 pdiplugin-trace pdiplugin-mpi pdiplugin-deisa py-h5py
export PDI_PLUGIN_PATH=/lus/home/NAT/gda2207/SHARED/spack_gda2207/spack-installs/__spack_path_placeholder__/__spack_path_placeholder__/__spack_path_pla/pdiplugin-user-code-1.5.5-gcc-11.2.0-n6cu/lib
export PDI_PLUGIN_PATH=/lus/home/NAT/gda2207/SHARED/spack_gda2207/spack-installs/__spack_path_placeholder__/__spack_path_placeholder__/__spack_path_pla/pdiplugin-set-value-1.5.5-gcc-11.2.0-nxao/lib:$PDI_PLUGIN_PATH
export PDI_PLUGIN_PATH=/lus/home/NAT/gda2207/SHARED/spack_gda2207/spack-installs/__spack_path_placeholder__/__spack_path_placeholder__/__spack_path_pla/pdiplugin-decl-hdf5-1.5.5-gcc-11.2.0-gq2h/lib:$PDI_PLUGIN_PATH
export PDI_PLUGIN_PATH=/lus/home/NAT/gda2207/SHARED/spack_gda2207/spack-installs/__spack_path_placeholder__/__spack_path_placeholder__/__spack_path_pla/pdiplugin-trace-1.5.5-gcc-11.2.0-dtl3/lib:$PDI_PLUGIN_PATH
export PDI_PLUGIN_PATH=/lus/home/NAT/gda2207/SHARED/spack_gda2207/spack-installs/__spack_path_placeholder__/__spack_path_placeholder__/__spack_path_pla/pdiplugin-mpi-1.5.5-gcc-11.2.0-xhd7/lib:$PDI_PLUGIN_PATH
export PDI_PLUGIN_PATH=/lus/home/NAT/gda2207/SHARED/spack_gda2207/spack-installs/__spack_path_placeholder__/__spack_path_placeholder__/__spack_path_pla/pdiplugin-deisa-develop-gcc-11.2.0-jehf/lib:$PDI_PLUGIN_PATH
#export KOKKOS_TOOLS_LIBS=$WORKDIR/ark-2-mhd/lib/kokkos-tools/src/tools/simple-kernel-timer/kp_kernel_timer.so
