cmake \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_CXX_STANDARD=17 \
    -DEuler_ENABLE_PDI=ON \
    -DKokkos_ENABLE_SERIAL=ON \
    -DKokkos_ARCH_ZEN3=ON \
    -DKokkos_ENABLE_HIP=ON \
    -DKokkos_ARCH_VEGA90A=ON \
    -DSESSION=MPI_SESSION \
    ..
