SCHEFILE=../output/scheduler.json
#ln -s ../simulation.yml
#ln -s ../reduction.py
dask-scheduler --scheduler-file=$SCHEFILE &
while ! [ -f $SCHEFILE ]; do
    sleep 3
done
dask-worker --local-directory /tmp --scheduler-file=$SCHEFILE &
python3 ../in_situ_fft.py
