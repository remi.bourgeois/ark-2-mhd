import sys

def write_ini(num_restart, mx, my, mz, tEnd, cfl, muscl_enabled, all_regime_correction):

    f = open("restart_"+str(num_restart)+".ini", "w")

    f.write("[run]\nsolver=godunov\nriemann=MHD3W_optimized\n")
    f.write("tEnd="+str(tEnd)+"\n")
    f.write("nStepmax=100000000\ninfo=1000\n")
    f.write("cfl="+str(cfl)+"\n")
    f.write("all_regime_correction="+str(all_regime_correction)+"\n")
    f.write("muscl_enabled="+str(muscl_enabled)+"\n")
    f.write("slope_type=1.0\n")

    if (int(num_restart)==0):
        f.write("random_perturbation=true\n")
        f.write("restart=false\n")
    else:
        f.write("restart=true\n")
        f.write("restart_filename=restart.h5\n") #############

    f.write("\n[mesh]\n")
    f.write("nx="+str(nx)+"\n"+"ny="+str(ny)+"\n"+"nz="+str(nz)+"\n")
    f.write("mx="+str(mx)+"\n"+"my="+str(my)+"\n"+"mz="+str(mz)+"\n")

    f.write("boundary_type_xmin=3\nboundary_type_xmax=3\nboundary_type_ymin=3\nboundary_type_ymax=3\nboundary_type_zmin=0\nboundary_type_zmax=0\n")

    f.write("\n[hydro]\ng_z=-1\nconvection_source_term_enabled=true\nQ_source_term=true\nR_source_term=true\nH_source_term=true\n")
    f.write("\n[thermo]\ngamma=1.4\nmmw1 = 10\nmmw2 = 14\nkB = 1.0\n")

    f.write("\n[output]\n")
    f.write("prefix="+prefix+"\n")
    f.write("nOutput=0\n")
    f.write("n_mean=0\n")
    f.write("n_profile=0\n")
    f.write("n_slice=0\n")
    f.write("dt_io=0\n")
    f.write("dt_mean=0\n")
    f.write("dt_profile=0\n")
    f.write("dt_slice=0\n")
    f.write("format=appended\ntype=pdi\n\n[problem]\nname=convection\ndimension=3\n")
    f.write("amplitude_seed = 0.00001\nHT = -1.2\nQA = -0.001\nRX = -0.6\ndensity_bottom=10.0\nT_bottom = 10\nBx_bottom = 0.00001\nX_bottom  = 0.98\ngrad_T  = 1.3\ngrad_X  = -0.96\n")

    f.close()

def write_config(num_restart, num_workers, mz, nz,prefix):
    f = open("deisa_config"+".yml", "w")
    f.write("workers: "+num_workers+"\n")
    f.write("mz: "+str(mz)+"\n")
    f.write("nz: "+str(nz)+"\n")
    f.write("prefix: "+"'"+prefix+"'"+"\n")
    f.write("num_restart: "+num_restart)
    f.close()

def write_job_deisa():
    f = open("job_deisa_.sh","w")
    f.write("#!/bin/bash\n#SBATCH -J ARK\n#SBATCH --constraint=MI250\n#SBATCH --account=gda2207\n")
    nproc_simu=int(mx)*int(my)*int(mz)
    nnodes=int(nproc_simu/8) +2 + int(num_workers)
    if (nproc_simu%8 !=0 ):
        nnodes=nnodes+1
    f.write("#SBATCH --nodes="+str(nnodes)+"\n")
    ntasks=nproc_simu+2 + int(num_workers)
    f.write("#SBATCH --ntasks="+str(ntasks)+"\n")
    f.write("#SBATCH --time="+"---WARNING---"+"\n")
    f.write("#SBATCH --gres=gpu:8\n#SBATCH --exclusive\n#SBATCH --error=%x_%j.e\n#SBATCH --output=%x_%j.o\n#SBATCH --exclude=x1004c2s3b0n0\n#SBATCH --threads-per-core=1\n\n")
    #f.write("module purge\nmodule load PrgEnv-amd/8.3.3 amd/5.2.3 cray-libsci/22.11.1.2\nmodule load craype-x86-trento\nmodule load libfabric/1.15.2.0 craype/2.7.19 #PrgEnv-cray/8.3.3\nmodule load craype-network-ofi cray-dsmml/0.2.2\nmodule load perftools-base/22.09.0 cray-mpich/8.1.21\nmodule load cray-mpich/8.1.21-cce\n\n")
    f.write("\ncd $WORKDIR/ark-2-mhd-last/build\nsource spack_activation.sh\nsource env.sh\n\nset -ex\n\nPREFIX=${SLURM_JOB_NAME}_${SLURM_JOBID}\nCLIENT=in_situ_fft.py\nNNODES=$(($SLURM_NNODES-2))\n")
    f.write("WORKERNODES="+str(num_workers)+"\n")
    f.write("SIMUNODES=$(($NNODES-$WORKERNODES))\nNPROC="+str(nproc_simu)+"\n")                   # Total number of processes\nNWORKER=$(($WORKERNODES))\nSCHEFILE=scheduler.json\n\n")
    f.write("NWORKER=$(($WORKERNODES))\n")
    f.write("SCHEFILE=scheduler.json\n")
    f.write("srun -N 1 -n 1 -c 64 -r 0 dask-scheduler --scheduler-file=${SCHEFILE} 1>> ${PREFIX}_dask-scheduler.o 2>> ${PREFIX}_dask-scheduler.e &\n\n")
    f.write("# Wait for the SCHEFILE to be created\nwhile ! [ -f ${SCHEFILE} ]; do\n        sleep 3\n       echo -n .\ndone\n\n")
    f.write("srun -N 1 -n 1 -c 64 -r 1 `which python` -m trace -l -g ${CLIENT} 1>> ${PREFIX}_client.o 2>> ${PREFIX}_client.e &\nclient_pid=$!\n\nsrun -N ${WORKERNODES} -n ${NWORKER} -c 64 -r 2 dask-worker --local-directory /tmp --scheduler-file=${SCHEFILE} 1>> ${PREFIX}_dask-worker.o 2>> ${PREFIX}_dask-worker.e &\n\nREL=$(($WORKERNODES+2))\nsrun -N ${SIMUNODES} -n ${NPROC} -c 8 -r ${REL} --mpi=cray_shasta --gpu-bind=verbose,closest ./main ./restart_"+str(num_restart)+".ini ./IO_root_deisa.yml --kokkos-map-device-id-by=mpi_rank &\nsimu_pid=$!\nwait $simu_pid")

num_restart=sys.argv[1]
mx=sys.argv[2]
my=sys.argv[3]
mz=sys.argv[4]
Nx=sys.argv[5]
Ny=sys.argv[6]
Nz=sys.argv[7]
tEnd=sys.argv[8]
cfl=sys.argv[9]
muscl_enabled=sys.argv[10]
all_regime_correction=sys.argv[11]
num_workers=sys.argv[12]

nOutput0=0
cfl0=0.9

print("num_restart =", num_restart)
print("mx =", mx)
print("my =", my)
print("mz =", mz)
print("Nx =", Nx)
print("Ny =", Ny)
print("Nz =", Nz)
print("tEnd =", tEnd)
print("muscl_enabled =", muscl_enabled)
print("all_regime_correction =", all_regime_correction)

print("nproc=",int(mx)*int(my)*int(mz))

Nx=int(Nx)
Ny=int(Ny)
Nz=int(Nz)

mx=int(mx)
my=int(my)
mz=int(mz)

if (Nx%mx != 0):
    print("mx does not divide Nx")
    print(Nx//mx)
    exit()
if (Ny%my != 0):
    print("my does not divide Ny")
    exit()
if (Nz%mz != 0):
    print("mz does not divide Nz")
    exit()

nx=int(Nx/int(mx))
ny=int(Ny/int(my))
nz=int(Nz/int(mz))

print("nx, ny, nz=", nx,ny,nz)
print("Nx, Ny, Nz=", Nx,Ny,Nz)


prefix='GC_automated'

write_ini(num_restart, mx, my, mz, tEnd, cfl, muscl_enabled, all_regime_correction)
write_config(num_restart, num_workers, mz, nz, prefix)
write_job_deisa()
