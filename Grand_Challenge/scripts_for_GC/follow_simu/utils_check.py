#Librairies
import h5py
import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt

linestyle=["dashed", "dashdot","dotted","solid"]
colors=["black","red","blue","cyan","grey","yellow","green","pink"]

mmw1=10
mmw2=14
gamma=1.4
kB=1.0
HT=-1.2
RX=-0.6
QA=-0.0
g=1


id=0
ip=1
iu=2
iv=3
iw=4
iBx=5
iBy=6
iBz=7
iX=8


def plot_sdp(file, restart, Nz, mz, nz, iz_middle_gloc, type, icolor):

    time=h5py.File(file,"r+")['time']

    k=1
    while (time[k]>0):
        k=k+1

    global_slice=h5py.File(file,"r+")['global_h_slice']

    mzr=mz[restart]
    nzr=nz[restart]
    Nzr=Nz[restart]
    iz_middle_glocr=iz_middle_gloc[restart]

    for mpi_z_rank in range(mzr):
        contains_middle = ((mpi_z_rank*nzr<=iz_middle_glocr)and(iz_middle_glocr<=(mpi_z_rank+1)*nzr-1))
        if (contains_middle):
            global_slice=global_slice[0:k,mpi_z_rank,:,:,:]

    if(type=='emag'):
        PDI=0.5*(global_slice[:,:,:,iBx]*global_slice[:,:,:,iBx]+global_slice[:,:,:,iBy]*global_slice[:,:,:,iBy]+global_slice[:,:,:,iBz]*global_slice[:,:,:,iBz])/(Nzr*Nzr)
    elif(type=='ekin'):
        PDI=0.5*global_slice[:,:,:,id]*(global_slice[:,:,:,iu]*global_slice[:,:,:,iu]+global_slice[:,:,:,iv]*global_slice[:,:,:,iv]+global_slice[:,:,:,iw]*global_slice[:,:,:,iw])/(Nzr*Nzr)

    istyle=0
    for i in range(k):
        if(i>=k-3):
            print("i=",i)
            kvals, Abins=power_spectrum_ekin(PDI[i,:,:])
            plt.plot(1./kvals, Abins, label="t="+str(time[i])+" N= "+str(Nzr)+" Restart="+str(restart), linestyle=linestyle[istyle], color=colors[icolor])
            istyle+=1

def plot_sdp_deisa(file, restart, Nz, mz, nz, iz_middle_gloc):

    global_slice=h5py.File(file,"r+")['global_h_slice']

    mzr=mz[restart]
    nzr=nz[restart]
    Nzr=Nz[restart]
    iz_middle_glocr=iz_middle_gloc[restart]

    for mpi_z_rank in range(mzr):
        contains_middle = ((mpi_z_rank*nzr<=iz_middle_glocr)and(iz_middle_glocr<=(mpi_z_rank+1)*nzr-1))

        if (contains_middle):
            global_slice_=global_slice[3,mpi_z_rank,:,:,:]

    PDI=0.5*global_slice_[:,:,id]*(global_slice_[:,:,iu]*global_slice_[:,:,iu]+global_slice_[:,:,iv]*global_slice_[:,:,iv]+global_slice_[:,:,iw]*global_slice_[:,:,iw])/(Nzr*Nzr)

    istyle=0
    icolor=0
    kvals, Abins=power_spectrum_ekin(PDI[:,:])
    plt.plot(1./kvals, Abins, label="Post Treatment"+str(restart), linestyle=linestyle[istyle], color=colors[icolor])

def plot_last_profile(file,Nz, mx, my, mz, nz, last_restart):
    Nzlr=Nz[last_restart]
    mxlr=mx[last_restart]
    mylr=my[last_restart]
    mzlr=mz[last_restart]
    nzlr=nz[last_restart]
    time=h5py.File(file,"r+")['time']

    k=1
    while (time[k]>0):
        k=k+1
    print(k," profile reductions have been done during restart ", last_restart)
    print("Last one was at time t=",time[k-1])

    vert_prof = h5py.File(file,"r+")["vert_prof"][k-1,:,:,:]
    nproc = vert_prof.shape[0]
    vert_prof_assembled=np.zeros((7, Nzlr))

    for proc in range(nproc):
        mpi_z_rank=proc%mzlr
        iz0=int(nzlr*mpi_z_rank)
        izf=int(nzlr*(mpi_z_rank+1))
        vert_prof_assembled[:,iz0:izf]+=vert_prof[proc,:,:]/(mx[last_restart]*mylr)

    thermo=vert_prof_assembled[6,:]
    chem=vert_prof_assembled[5,:]
    Bx2=vert_prof_assembled[0,:]
    By2=vert_prof_assembled[1,:]
    Bz2=vert_prof_assembled[2,:]

    B2=np.sum(Bx2)+np.sum(By2)+np.sum(Bz2)
    pctx=100*np.sum(Bx2)/B2
    pcty=100*np.sum(By2)/B2
    pctz=100*np.sum(Bz2)/B2

    dia=thermo -(HT/RX)*chem
    ad=thermo -chem

    #Initial condition
    file="../profile_restart_0_GC_automated.h5"
    vert_prof = h5py.File(file,"r+")["vert_prof"][0,:,:,:]
    nproc = vert_prof.shape[0]
    vert_prof_assembled=np.zeros((7, Nz[0]))
    for proc in range(nproc):
        mpi_z_rank=proc%mz[0]
        iz0=int(nz[0]*mpi_z_rank)
        izf=int(nz[0]*(mpi_z_rank+1))
        vert_prof_assembled[:,iz0:izf]+=vert_prof[proc,:,:]/(mx[0]*my[0])
    thermo=vert_prof_assembled[6,:]
    chem=vert_prof_assembled[5,:]
    dia0=thermo -(HT/RX)*chem
    ad0=thermo -chem
    ###

    plt.plot(ad,np.linspace(0,1, Nzlr), label='log T ad',color="blue")
    plt.plot(ad0,np.linspace(0,1,Nz[0]), label='log T ad at t=0',linestyle="dotted",color="blue")

    plt.plot(dia-np.mean(dia-ad),np.linspace(0,1, Nzlr), label='log T dia',color="red")
    plt.plot(dia0-np.mean(dia-ad),np.linspace(0,1,Nz[0]), label='log T dia at t=0',linestyle="dotted",color="red")

    plt.ylabel("$z$")
    plt.legend(loc="lower right", fontsize=10)
    plt.savefig("suivi_logT.png")
    plt.clf()

    off=int((0.1*Nzlr))
    plt.plot(Bx2[off: Nzlr-off], np.linspace(0,1,Nzlr)[off: Nzlr-off], label='Bx2 '+'{0:.2f}'.format(pctx)+"%",color="blue")
    plt.plot(By2[off: Nzlr-off], np.linspace(0,1,Nzlr)[off: Nzlr-off], label='By2 '+'{0:.2f}'.format(pcty)+"%",color="red")
    plt.plot(Bz2[off: Nzlr-off], np.linspace(0,1,Nzlr)[off: Nzlr-off], label='Bz2 '+'{0:.2f}'.format(pctz)+"%",color="purple")

    plt.ylabel("$z$")
    plt.legend(loc="lower right", fontsize=10)
    plt.savefig("suivi_B2.png")
    plt.clf()




def plot_ekin_emag(file, label,color,axs, nolegend, resolution_change):
    time=[]
    emag=[]
    ekin=[]
    print("file=",file)
    time_r=h5py.File(file,"r+")['time']

    k=1
    while (time_r[k]>0):
        k=k+1

    print(k,"global reductions have been done during restart",label)

    time=time[0:k]
    emag=h5py.File(file,"r+")['emag'][0:k,0]
    ekin=h5py.File(file,"r+")['ekin'][0:k,0]
    time=h5py.File(file,"r+")['time'][0:k]

    if (nolegend):
        add='_'
    else:
        add=''
    axs.plot(time, emag,label=add+r'$e_{mag}$', color=color,linestyle='dotted')
    axs.plot(time, ekin,label=add+r'$e_{kin}$', color=color)

    if(resolution_change):
        plt.axvline(x = time[k-1], color = 'b', label = add+'Upscaling')

def power_spectrum_ekin(ekin_slice):

    npix = ekin_slice.shape[0]

    fourier_image = np.fft.fftn(ekin_slice)
    fourier_amplitudes = np.abs(fourier_image)**2

    kfreq = np.fft.fftfreq(npix) * npix
    kfreq2D = np.meshgrid(kfreq, kfreq)
    knrm = np.sqrt(kfreq2D[0]**2 + kfreq2D[1]**2)

    knrm = knrm.flatten()
    fourier_amplitudes = fourier_amplitudes.flatten()

    kbins = np.arange(0.5, npix//2+1, 1.)
    kvals = 0.5 * (kbins[1:] + kbins[:-1])
    Abins, _, _ = stats.binned_statistic(knrm, fourier_amplitudes,
                                         statistic = "mean",
                                         bins = kbins)
    Abins *= np.pi * (kbins[1:]**2 - kbins[:-1]**2)

    return kvals, Abins

def mean_quantity(f,quantity):

    return np.mean( formula(f,quantity) )

def slice(f, quantity, dir):
    if   dir == 'x':
        return np.mean( formula(f,quantity), axis=(0,1))
    elif dir =='y':
        return np.mean( formula(f,quantity), axis=(0,2))
    elif dir =='z':
        return np.mean( formula(f,quantity), axis=(1,2))
    else:
        print("ERROR WRONG DIRECTION")


Lvar=['d','E','mx','my','mz','Bx','By','Bz','dX']


#Physical functions
def emag(Bx,By,Bz):
    return 0.5*(Bx*Bx+By*By+Bz*Bz)

def ekin(mx,my,mz,d):
    return 0.5*(mx*mx+my*my+mz*mz)/d

def mu0(X):
    return 1.0/(X/mmw1 + (1.-X)/mmw2)

def pressure(d, E, mx, my, mz, Bx, By, Bz):

    decin = 0.5*(mx*mx+my*my+mz*mz)/d
    demag = 0.5*(Bx*Bx+By*By+Bz*Bz)

    deint = E - decin - demag

    return deint*(gamma-1)


def Temp(d, p, X):
    return p*mu0(X)/(d*kB)

def Ltheta(T, p):
    return np.log( T*(1.0/p)**((gamma-1.)/gamma) )

def Lmu0(X):
    return np.log(mu0(X))

def formula(f,quantity):

    for q in Lvar:
        if(quantity==q):
            return f[q][()]

    if quantity=='emag':
        return emag(f['Bx'][()],f['By'][()],f['Bz'][()])

    elif quantity == 'ekin':
        return ekin(f['mx'][()],f['my'][()],f['mz'][()],f['d'][()])

    elif quantity == 'Lmu0':
        X = f['dX'][()]/f['d'][()]
        return Lmu0(X)

    elif quantity == 'Ltheta':
        p = pressure(f['d'][()], f['E'][()], f['mx'][()], f['my'][()], f['mz'][()], f['Bx'][()], f['By'][()], f['Bz'][()])
        X = f['dX'][()]/f['d'][()]
        T = Temp(f['d'][()], p,X)
        return Ltheta(T,p)
    elif quantity=='Bx2':
        return f['Bx'][()]*f['Bx'][()]
    elif quantity=='By2':
        return f['By'][()]*f['By'][()]
    elif quantity=='u':
        return f['mx'][()]/f['d'][()]
    elif quantity=='v':
        return f['my'][()]/f['d'][()]
    elif quantity=='Ma':
        p = pressure(f['d'][()], f['E'][()], f['mx'][()], f['my'][()], f['mz'][()], f['Bx'][()], f['By'][()], f['Bz'][()])
        c=np.sqrt(gamma*p/f['d'][()])
        u=np.sqrt(2*ekin(f['mx'][()],f['my'][()],f['mz'][()],f['d'][()]))
        return u/c
    elif (quantity=='PB'):
        p = pressure(f['d'][()], f['E'][()], f['mx'][()], f['my'][()], f['mz'][()], f['Bx'][()], f['By'][()], f['Bz'][()])
        Emag=emag(f['Bx'][()],f['By'][()],f['Bz'][()])
        return p/Emag

    else:
        print("ERROR - Unknown quantity")

def vertical_profiles(f,L_profiles, Lx, Ly, Lz):


    kx=2*3.14/(Lx)
    ky=2*3.14/(Ly)
    kz=2*3.14/(Lz)

    k2 = kx*kx + ky*ky + kz*kz
    kMy  = k2*ky*ky/(kx*kx+ky*ky)
    kMx  = k2*kx*kx/(kx*kx+ky*ky)
    kSx = kx*kz        /(kx*kx+ky*ky)
    kSy = ky*kz        /(kx*kx+ky*ky)


    timestr=format(f['time'][()],'.1f')


    nz=int(f['d'].shape[0])


    thermo_computed=0
    chem_computed=0
    shear_computed=0
    mag_computed=0

    thermo =np.zeros(nz)
    chem =np.zeros(nz)
    thermo =np.zeros(nz)
    shear =np.zeros(nz)
    mag =np.zeros(nz)

    d = slice(f, 'd', 'z')

    dz=1./nz

    for p in L_profiles:
        for physics in p:

            if ((physics=='thermo') and (thermo_computed==0)):
                thermo = slice(f, 'Ltheta', 'z')
                thermo_computed=1

            elif((physics=='chem') and (chem_computed==0)):
                chem = slice(f, 'Lmu0', 'z')
                chem_computed=1

            elif ((physics=='shear') and (shear_computed==0)):

                u=slice(f, 'u', 'z')
                v=slice(f, 'v', 'z')

                abs_dudz=np.zeros(nz)
                abs_dvdz=np.zeros(nz)

                for i in range(1,nz):
                    abs_dudz[i] = abs((u[i]-u[i-1])/dz)
                    abs_dvdz[i] = abs((v[i]-v[i-1])/dz)

                Sx=np.zeros(nz)
                Sx[0]=0.0
                Sy=np.zeros(nz)
                Sy[0]=0.0

                for i in range(1,nz):

                    Sx[i]  = Sx[i-1] +dz*abs_dudz[i-1]
                    Sy[i]  = Sy[i-1] +dz*abs_dvdz[i-1]

                shear = -Sx*kSx/g -Sy*kSy/g
                shear_computed=1

            elif((physics=='mag') and (mag_computed==0)):
                Bx2 = slice(f, 'Bx2', 'z')
                By2 = slice(f, 'By2', 'z')

                Mx=np.zeros(nz)
                Mx[0]=0.0
                My=np.zeros(nz)
                My[0]=0.0

                for i in range(1,nz):

                     Mx[i]  = Mx[i-1] +dz*Bx2[i-1]/d[i-1]
                     My[i]  = My[i-1] +dz*By2[i-1]/d[i-1]

                mag = Mx*kMx+My*kMy
                mag_computed=1

        type = p[0]
        title = type
        profile = np.zeros(nz)
        k=1

        while (p[k]!='color'):
            title = title +"+"+p[k]

            if (p[k]=='thermo'):
                profile = profile+thermo
            if (p[k]=='chem'):
                if (type=='ad'):
                    profile = profile -chem
                elif (type=='dia'):
                    profile = profile - chem*(QA+HT)/(RX+QA)
            if (p[k]=='shear'):
                if (type=='ad'):
                    profile = profile + shear*(HT+QA+RX)
                elif (type=='dia'):
                    profile = profile + shear*(QA*RX +HT*(QA+RX))/(RX+QA)
            if (p[k]=='mag'):
                if (type=='ad'):
                    profile = profile + mag
                elif (type=='dia'):
                    profile = profile + mag*(HT+RX)/(RX+QA)
            k=k+1


        profile = profile - np.mean(profile-thermo)
        color = p[k+1]
        linestyle=p[k+2]

        return thermo, chem, u,v,Bx2,By2
