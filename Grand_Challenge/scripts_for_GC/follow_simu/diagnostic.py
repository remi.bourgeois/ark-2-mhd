import h5py
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
from utils_check import *
import sys


if (len(sys.argv)<2):
    print('usage: python3 diagnostic.py time')
    exit()

t=int(sys.argv[1])

last_restart=14
mx=[1,1,2,2,2,2,2,4,4,4,4,4,4,4,8]
my=[1,1,2,2,4,4,4,8,8,8,8,8,8,8,16]
mz=[2,2,2,2,4,4,4,8,8,8,8,8,8,8,16]
Nz=[256,256,512,512,1024,1024,1024,2048,2048,2048,2048,2048,2048,2048,4096]
nz=[128,128,256,256,256,256,256,256,256,256,256,256,256,256,256]
Ny=[256,256,512,512,1024,1024,1024,2048,2048,2048,2048,2048,2048,2048,4096]
ny=[256,256,256,256,256,256,256,256,256,256,256,256,256,256,256]

iz_middle_gloc=[]
for r in range(last_restart+1):
    iz_middle_gloc.append(int(Nz[r]/2-1))
    print(iz_middle_gloc)
iy_middle_gloc=[]
for r in range(last_restart+1):
    iy_middle_gloc.append(int(Ny[r]/2-1))

print("****** Automated checks ******\n")

print("*** Suivi energies ***\n")


fig, axs = plt.subplots()

for restart in range(last_restart+1):

    file="../mean_restart_"+str(restart)+"_GC_automated.h5"
    resolution_change = False
    if (restart<last_restart):
        resolution_change = Nz[restart+1]!=Nz[restart]

    plot_ekin_emag(file,str(restart),'black',axs,(restart>0),resolution_change)

axs.set_title(r'Suivi energies')
plt.ylabel(r'$Energies$')
plt.xlabel(r'time (s)')
axs.legend(loc="lower right", fontsize=10)
axs.set_yscale('log')
fig.set_size_inches(8, 6)
plt.savefig("suivi_energies.png")
plt.clf()


print("\n*** Suivi conservation et régimes ***\n")

file0="../GC_automated_time_000000000.h5"
filef="../GC_automated_time_{:09}.h5".format(t)

print("initial file=",file0)
print("last file=",filef)


M0=np.mean(h5py.File(file0,"r+")['d'])
BX0=np.mean(h5py.File(file0,"r+")['Bx'])
BY0=np.mean(h5py.File(file0,"r+")['By'])
BZ0=np.mean(h5py.File(file0,"r+")['Bz'])
DX0=np.mean(h5py.File(file0,"r+")['dX'])

M=np.mean(h5py.File(filef,"r+")['d'])
BX=np.mean(h5py.File(filef,"r+")['Bx'])
BY=np.mean(h5py.File(filef,"r+")['By'])
BZ=np.mean(h5py.File(filef,"r+")['Bz'])
DX=np.mean(h5py.File(filef,"r+")['dX'])


print("Total Mass variation =", M0-M)
print("Total Bx variation =", BX0-BX)
print("Total By variation =", BY0-BY)
print("Total Bz variation =", BZ0-BZ)
print("Total dx variation =", DX0-DX)

Ma=formula(h5py.File(filef,"r+"), 'Ma')

print("Max Ma=",np.max(Ma))
print("Min Ma=",np.min(Ma))
print("Mean Ma=",np.mean(Ma))

Ma_flat = Ma.flatten()
# Plot the histogram
plt.hist(Ma_flat, bins=30, edgecolor='black',weights=100*np.ones_like(Ma_flat) / len(Ma_flat))
plt.xlabel("Ma")
plt.ylabel("Relative fequency %")
fig.set_size_inches(8, 6)
plt.savefig("distribution_Ma.png")
plt.clf()

PB=formula(h5py.File(filef,"r+"), 'PB')
print("Max Plasma Beta=",np.max(PB))
print("Min Plasma Beta=",np.min(PB))

PB_flat = np.log10(PB.flatten())
# Plot the histogram
plt.hist(PB_flat, bins=50, edgecolor='black',weights=100*np.ones_like(PB_flat) / len(PB_flat))
plt.xlabel("log 10 PB")
plt.ylabel("Relative fequency %")
fig.set_size_inches(8, 6)
plt.savefig("distribution_PB.png")
plt.clf()



print("\n*** Suivi profils ***\n")

file="../profile_restart_"+str(last_restart)+"_GC_automated.h5"
plot_last_profile(file, Nz, mx, my, mz, nz, last_restart)

print('\n*** Suivi spd CV ***\n')

icolor = 0
last_resolution=Nz[last_restart]

for restart in range(last_restart+1):

    if (Nz[restart]==last_resolution):
        file="../h_slice_restart_"+str(restart)+"_GC_automated.h5"
        plot_sdp(file, restart, Nz, mz, nz, iz_middle_gloc, 'ekin', icolor)
        icolor += 1

plt.xlabel("$L=1/k$")
plt.ylabel("Kinetic energy E(L)")
plt.yscale('log')
plt.xscale('log')
plt.legend()
plt.tight_layout()
fig.set_size_inches(8, 6)
plt.savefig("power_spectrum_ekin_last_res.png")
plt.clf()

print("\n*** Suivi sdp ***\n")

icolor = 0

for restart in range(last_restart+1):

    resolution_change = False
    if (restart<last_restart):
        resolution_change = Nz[restart+1]!=Nz[restart]

    if (resolution_change)or(restart==last_restart):
        file="../h_slice_restart_"+str(restart)+"_GC_automated.h5"
        plot_sdp(file, restart, Nz, mz, nz, iz_middle_gloc, 'ekin', icolor)
        icolor += 1

plt.xlabel("$L=1/k$")
plt.ylabel("Kinetic energy E(L)")
plt.yscale('log')
plt.xscale('log')
plt.legend()
plt.tight_layout()
fig.set_size_inches(8, 6)
plt.savefig("power_spectrum_ekin.png")
plt.clf()

icolor = 0

for restart in range(last_restart+1):

    resolution_change = False
    if (restart<last_restart):
        resolution_change = Nz[restart+1]!=Nz[restart]

    if (resolution_change)or(restart==last_restart):
        file="../h_slice_restart_"+str(restart)+"_GC_automated.h5"
        plot_sdp(file, restart, Nz, mz, nz, iz_middle_gloc, 'emag', icolor)
        icolor += 1

plt.xlabel("$L=1/k$")
plt.ylabel("Magnetic energy E(L)")
plt.yscale('log')
plt.xscale('log')
plt.legend()
plt.tight_layout()
fig.set_size_inches(8, 6)
plt.savefig("power_spectrum_emag.png")
plt.clf()


print("\n*** Suivi Deisa ***\n")
fig, axs = plt.subplots()

restart=13

file="../h_slice_restart_"+str(restart)+"_GC_automated.h5"
plot_sdp_deisa(file, restart, Nz, mz, nz, iz_middle_gloc)
file="../fft_from_deisa_GC_automated_"+str(restart)+".h5"

knrm= h5py.File(file,"r+")["knrm"]
kbins= h5py.File(file,"r+")["kbins"]
fourier_amplitudes= h5py.File(file,"r+")["fourier_amplitudes"]
kvals= h5py.File(file,"r+")["kvals"]

Abins, _, _ = stats.binned_statistic(knrm, fourier_amplitudes,
                                     statistic = "mean",
                                     bins = kbins)
Abins *= np.pi * (kbins[1:]**2 - kbins[:-1]**2)

plt.plot(1/kvals[:], Abins, label="Deisa", linestyle="dotted", color='pink')
plt.xlabel("$L=1/k$")
plt.ylabel("Kinetic energy E(L)")
plt.yscale('log')
plt.xscale('log')
plt.legend()
plt.tight_layout()
fig.set_size_inches(8, 6)
plt.savefig("check_deisa.png")
plt.clf()

print("\n *** plots slices ***\n")

vmin=999
vmax=-999

for restart in range(last_restart+1):
    file="../v_slice_restart_"+str(restart)+"_GC_automated.h5"
    time=h5py.File(file,"r+")['time']
    k=1
    while (time[k]>0):
        k=k+1
    print(k," slice reductions have been done during restart", restart)
    print("Last one was at time t=",time[k-1])

    global_slice=h5py.File(file,"r+")['global_v_slice']

    myr=my[restart]
    nyr=ny[restart]
    Nyr=Ny[restart]
    iy_middle_glocr=iy_middle_gloc[restart]

    for mpi_y_rank in range(myr):
        contains_middle = ((mpi_y_rank*nyr<=iy_middle_glocr)and(iy_middle_glocr<=(mpi_y_rank+1)*nyr-1))
        if (contains_middle):
            global_slice=global_slice[:,mpi_y_rank,:,:,:]
            d=global_slice[:,:,:,iw]
            for i in range(k):
                if (restart<5):
                    vmin=min(np.min(d[i]),vmin)
                    vmax=max(np.max(d[i]),vmax)

                plt.imshow(d[i,:,:].transpose(),cmap='inferno', vmin=vmin, vmax=vmax)
                plt.xlabel("x")
                plt.ylabel("z")
                fig.set_size_inches(16, 16)
                plt.savefig("v_"+str(restart)+"_"+str(i)+".png")
                plt.clf()
                print(i)

vmin=999
vmax=-999

for restart in range(last_restart+1):
    file="../h_slice_restart_"+str(restart)+"_GC_automated.h5"
    time=h5py.File(file,"r+")['time']
    k=1
    while (time[k]>0):
        k=k+1
    print(k," slice reductions have been done during restart", restart)
    print("Last one was at time t=",time[k-1])

    global_slice=h5py.File(file,"r+")['global_h_slice']

    mzr=mz[restart]
    nzr=nz[restart]
    Nzr=Nz[restart]
    iz_middle_glocr=iz_middle_gloc[restart]

    for mpi_z_rank in range(mzr):
        contains_middle = ((mpi_z_rank*nzr<=iz_middle_glocr)and(iz_middle_glocr<=(mpi_z_rank+1)*nzr-1))
        if (contains_middle):
            global_slice=global_slice[:,mpi_z_rank,:,:,:]
            d=global_slice[:,:,:,id]
            for i in range(k):

                if (restart<5):
                    vmin=min(np.min(d[i]),vmin)
                    vmax=max(np.max(d[i]),vmax)

                plt.imshow(d[i,:,:].transpose(),cmap='inferno',vmin=vmin,vmax=vmax)
                plt.xlabel("x")
                plt.ylabel("y")
                fig.set_size_inches(16, 16)
                plt.savefig("h_"+str(restart)+"_"+str(i)+".png")
                plt.clf()
                print(i)
