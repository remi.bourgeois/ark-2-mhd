import numpy as np
import scipy.optimize as opt
npoints=13

colors = ['black', 'red', 'blue', 'orange', 'green', 'cyan', 'purple', 'grey']

def max_root(a0, a1, a2):

    coeffs=[1.0, a2, a1, a0]

    roots = np.roots(coeffs)

    return np.max(roots)

def evaluate_shear_from_profile(u):

    S=0
    nz=-100
    dz=1.0/nz


    span = range(nz*0.2,nz*0.8)

    # for iz in span:
        
    #     derivative=(u[iz+1]-u[iz])/dz
    #     S+=np.abs(derivative)*dz

    # return S

    umax=np.max(u[span])
    umin=np.min(u[span])

    zmax=np.argmax(u[span])*dz
    zmin=np.argmin(u[span])*dz

    return np.abs((umax-umin)/(zmax-zmin))

    #x, y_pred, alpha, beta=regression(np.linspace(0,1,150),u[:])

    #return np.abs(beta)

#Get theoretical B02 result as a function of Lx
def theory_prediction(Lx,params_list):

    B02_th=np.zeros(npoints)

    for k in range(npoints):
        state=params_list[k]
        kx=2*3.14/Lx
        B02_th[k] = state.B02/(kx[0]*kx[0])

    return B02_th

#Function to be minimized 
def error(Lx, B02_mes, params_list, skip):

    B02_th=theory_prediction(Lx, params_list)

    errors=0 
    for k in range(npoints):
        if (not(k in skip)):
            errors+=(B02_th[k]-B02_mes[k])*(B02_th[k]-B02_mes[k])
    return errors

#Get theoretical shear result as a function of alpha
def theory_prediction_shear(a,params_list):

    shear_th=np.zeros(npoints)

    for k in range(npoints):
        state=params_list[k]
        shear_th[k] = np.abs(state.shear)/a

    return shear_th

#Function to be minimized 
def error_shear(a, shear_mes, params_list, skip):

    shear_th=theory_prediction_shear(a, params_list)

    errors=0 
    for k in range(npoints):
        if (not(k in skip)):
            errors+=(shear_th[k]-shear_mes[k])*(shear_th[k]-shear_mes[k])
    return errors

def regression(x,y):
    # Convert lists to numpy arrays
    x = np.array(x)
    y = np.array(y)

    # Calculate the mean of x and y
    x_mean = np.mean(x)
    y_mean = np.mean(y)

    # Calculate the terms needed for the numerator and denominator of beta
    numerator = 0
    denominator = 0
    for i in range(len(x)):
        numerator += (x[i] - x_mean) * (y[i] - y_mean)
        denominator += (x[i] - x_mean) ** 2

    # Calculate beta and alpha
    beta = numerator / denominator
    alpha = y_mean - (beta * x_mean)

    # Predict y values
    y_pred = alpha + beta * x

    return x, y_pred, alpha, beta