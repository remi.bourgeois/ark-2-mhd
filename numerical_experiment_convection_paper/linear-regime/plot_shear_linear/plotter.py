from utils import *
import matplotlib.pyplot as plt
import h5py
import numpy as np
from matplotlib import rc
from uz_from_vtk import *
rc('text', usetex=True)


def plot_regression_w(x, y, y_th, y_th_noshear, parameter_label, suffix):
    
    # Plotting the original data points
    plt.scatter(x[0:7], y[0:7], color='blue', label='Measurements from simulation')
    #plt.scatter(x[7], y[7], color='black', label='Measurement from simulation - no shear')

    # Plotting the line of linear regression
    plt.plot(x[0:7], y_th[0:7], color='red', label='Analytical $\omega$')
    #plt.plot(x, y_th_noshear, color='red', label='Analytical $\omega$ - no shear')

    plt.xlabel(parameter_label)
    plt.ylabel(r"$\omega$")
    plt.title(r"Variations of the growth rate as a function of $H_T$")

    # Adding legend
    plt.legend(fontsize=10)
    plt.grid()
    # Show the plot
    plt.savefig("w_shear_analytical.png", dpi=200)
    plt.clf()

def plot_time_series(parameter_values, parameter_label, suffix, npoints, params_list, range_avg):
    list_reg=[]
    fig, axs = plt.subplots()

    for k in range(npoints):
        w=add_param_plot(parameter_values[k], colors[k%8], parameter_label, axs, params_list[k], range_avg,k)
        list_reg.append(w)

    plt.ylabel(r'$\max\mid u_z\mid$')
    plt.xlabel('Time')
    plt.legend(bbox_to_anchor=(1.01, 1.0), loc='upper left')
    axs.set_yscale('log')
    plt.tight_layout()
    plt.grid()
    plt.savefig("suivi_energies_"+suffix+".png", dpi=200)
    plt.clf()
    plt.close()

    return list_reg

def add_param_plot(parameter_value, color, parameter_label, axs, Params, range_avg,n):
    
    time, uz, ux= get_series(n)

    if (n==7):
        axs.plot(time, uz,label=r"$H_T=-6\\$no shear", color="black",linestyle='solid')
    else:
        axs.plot(time, uz,label=parameter_label+"="+str(parameter_value)[0:7], color=color,linestyle='dotted')

    wt=np.log(uz)
    wL=np.array([(wt[i+1]-wt[i])/(time[i+1]-time[i]) for i in range(0,39)])


    if (n==0):
        w=0.0#np.mean(wL[3:40])
    if (n==1):
        w=0.0#np.mean(wL[3:45])
    if (n==2):
        w=np.mean(wL[3:40])
    if (n==3):
        w=np.mean(wL[3:40])
    if (n==4):
        w=np.mean(wL[3:35])
    if (n==5):
        w=np.mean(wL[3:15])
    if (n==6):
        w=np.mean(wL[3:5])
    if (n==7):
        w=np.mean(wL[3:5])
    
    print(n)
    return w
