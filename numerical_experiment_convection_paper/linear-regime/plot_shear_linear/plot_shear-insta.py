from params import params
from plotter import *

#suffix of the files to get data from
folder="data/"
suffix="grad_u0"
#Number of parametric datapoints and physical qqt (see params.py)
npoints = 8
rhobot=1.0
QA=0
RX=0
Tbot=70
Xbot=0
mu0=1
mu1=1
gradT = -10
nz=50
grad_u0=0.03

range_avg=0

#Parametric study parameters
parameter_label=r"$H_T$"

def HT(k):
    HT = [-6, -5, -4, -3, -2, -1, 0, -6]
    HT=HT[k]
    return HT

def parameter_value(Params):
    return Params.HT

parameter_values=[]
params_list=[]
for k in range(npoints):

    Params=params(rhobot=rhobot, Tbot=Tbot, gradT=gradT, mu0=mu0, HT=HT(k), grad_u0=grad_u0, type=type, nz=nz)
    parameter_values.append(parameter_value(Params))
    params_list.append(Params)

params_list_noshear=[]
for k in range(npoints):

    Params=params(rhobot=rhobot, Tbot=Tbot, gradT=gradT, mu0=mu0, HT=HT(k), grad_u0=0.0, type=type, nz=nz)
    params_list_noshear.append(Params)


list_reg=plot_time_series(parameter_values, parameter_label, suffix, npoints, params_list, range_avg)

x=np.array([parameter_value(param) for param in params_list])
y_th=np.array([params_list[k].w for k in range(npoints)])
y_th_noshear=np.array([params_list_noshear[k].w for k in range(npoints)])

y=list_reg

plot_regression_w(x, y, y_th, y_th_noshear, parameter_label, suffix)
