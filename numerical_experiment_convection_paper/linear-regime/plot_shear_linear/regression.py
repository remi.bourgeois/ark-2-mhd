import numpy as np

def regression(x,y):
    # Convert lists to numpy arrays
    x = np.array(x)
    y = np.array(y)

    # Calculate the mean of x and y
    x_mean = np.mean(x)
    y_mean = np.mean(y)

    # Calculate the terms needed for the numerator and denominator of beta
    numerator = 0
    denominator = 0
    for i in range(len(x)):
        numerator += (x[i] - x_mean) * (y[i] - y_mean)
        denominator += (x[i] - x_mean) ** 2

    # Calculate beta and alpha
    beta = numerator / denominator
    alpha = y_mean - (beta * x_mean)

    # Predict y values
    y_pred = alpha + beta * x

    return x, y_pred, alpha, beta