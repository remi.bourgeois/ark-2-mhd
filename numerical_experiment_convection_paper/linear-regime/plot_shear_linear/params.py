import numpy as np
from utils import max_root
import matplotlib.pyplot as plt

#Discretisation
ztop, zbot = 1, 0
kx = 3.14
kz  = kx
ky = 0

kH2 = ky**2+kx**2
k2 = (kx**2+ky**2+kz**2)
class params:

    def __init__(self, rhobot=0, QA=0, HT=0, RX=0, Tbot=0, gradT=0, Xbot=0, gradX=0, mu0=0, mu1=0, grad_u0=0, type="", nz=-999):

        #Get values
        self.rhobot=rhobot
        self.QA=QA
        self.HT=HT
        self.RX=RX
        self.Tbot=Tbot
        self.gradT=gradT
        self.mu0=mu0
        self.mu1=mu1
        self.Xbot=Xbot
        self.gradX=gradX
        self.grad_u0=grad_u0
        self.type=type
        self.nz=nz
        
        #This never changes in my study
        self.gamma=1.4
        self.kB=1.
        self.g=1

        #safe default choice 
        if (self.mu1==0):
            self.mu1=mu0

        #Compute gradients 
        self.grad_calc()

    def cs(self,z):
        return np.sqrt(self.gamma*self.kB*self.T(z)/self.mu(z))
    
    def mu(self, z):
        X = self.Xbot +  self.gradX*z
        mum1 = X / self.mu0 + (1.0 - X) / self.mu1
        return 1.0 / mum1
    
    def T(self, z):

        return self.Tbot + z * self.gradT
    
    def P(self,z,rho):
        return rho*self.kB*self.T(z)/self.mu(z)
    
    def theta(self,z,rho):
        P=self.P(z,rho)
        T=self.T(z)

        return T/(P**((self.gamma-1)/self.gamma))

    def grad_calc(self):
        
        nz=self.nz
        dz = (ztop - zbot) / nz
        K = dz * (-self.g) / (2 * self.kB)

        rhom = self.rhobot

        L_rho=np.zeros(nz)
        L_hp=np.zeros(nz)
        L_grad_T=np.zeros(nz)
        L_grad_mu=np.zeros(nz)
        L_ad=np.zeros(nz)
        L_dia=np.zeros(nz)
        L_B02_dia=np.zeros(nz)
        L_B02_ad=np.zeros(nz)

        for iz in range(0, nz):

            #compute next state
            zp = zbot + (iz+1) * dz
            zm = zbot + (iz) * dz

            Tp = self.T(zp)
            Tm = self.T(zm)

            mup = self.mu(zp)
            mum = self.mu(zm)

            rhop = rhom * (K + Tm / mum) / (Tp / mup - K)

            #Get local gradient values
            L_hp[iz], L_grad_T[iz], grad_ad, L_grad_mu[iz] = self.compute_gradients(rhop, rhom, zp, zm)
            L_rho[iz]= rhop
            self.grad_ad=grad_ad
            L_dia[iz] = (L_grad_T[iz]-self.grad_ad)*(self.RX+self.QA) - L_grad_mu[iz]*(self.HT+self.QA)
            L_ad[iz]  = L_grad_T[iz]-self.grad_ad - L_grad_mu[iz]
            L_B02_dia[iz] = (self.g*L_rho[iz]/(L_hp[iz]*(self.HT+self.RX)))*L_dia[iz]
            L_B02_ad[iz] = (self.g*L_rho[iz]/(L_hp[iz]))*L_ad[iz]
        


           # L_shear_ad[iz] = (self.g/(L_hp[iz]*(self.HT+self.RX+self.QA)))*L_ad[iz]

            #Check stability locally
            # if (self.type=="dia"):
            #     if (L_ad[iz]>0):
            #         print("instable ad")

            # if (L_dia[iz]>0):
            #     print("stable dia")
            # elif(self.type=="ad"):
            #     if (L_ad[iz]<0):
            #         print("stable ad")
            #     if (L_dia[iz]<0):
            #         print("instable dia")
            #End of check

            #Prepare for next iteration
            rhom = rhop

        rhotop=rhop
    
        span=range(int(nz*0.2),int(nz*0.8))

        self.grad_T=np.mean(L_grad_T[0])
        self.grad_mu=np.mean(L_grad_mu[0])
        self.hp = np.mean(L_hp[0])
        self.ad=np.mean(L_ad[0])
        self.dia=np.mean(L_dia[0])

        wbot=self.get_growth_rate()

        self.grad_T=np.mean(L_grad_T[nz-1])
        self.grad_mu=np.mean(L_grad_mu[nz-1])
        self.hp = np.mean(L_hp[nz-1])
        self.ad=np.mean(L_ad[nz-1])
        self.dia=np.mean(L_dia[nz-1])

        wtop=self.get_growth_rate()

        self.grad_T=np.mean(L_grad_T[int(nz/2)])
        self.grad_mu=np.mean(L_grad_mu[int(nz/2)])
        self.hp = np.mean(L_hp[int(nz/2)])
        self.ad=np.mean(L_ad[int(nz/2)])
        self.dia=np.mean(L_dia[int(nz/2)])
        self.rho=np.mean(L_rho[int(nz/2)])

        self.w=self.get_growth_rate()
        #self.w=wbot
        self.CS=self.cs(0.5)
        self.MaTH=self.w/self.CS

        self.stratif=self.rhobot/rhotop,wtop/wbot

        #self.hp, self.grad_T, grad_ad, self.grad_mu = self.compute_gradients(rhotop, self.rhobot, 1.0, 0.0)
        #self.w=self.get_growth_rate()

        # print("......")
        # print("......")
        # print("......")

        # grade=self.kB*self.gradT/(self.mu0*(self.gamma-1))
        # beta= -self.g/(grade*(self.gamma-1)) 
        # bm1=beta-1

        # print(rhotop, (self.rhobot*(self.T(ztop)/self.Tbot)**bm1))

        # cv=self.kB/(self.mu0*(self.gamma-1))
        # wloc=self.get_growth_rate()

        # w2thp=(kH2/k2)*(self.gradT*cv*self.gamma/(-self.g)-1)*grad_ad*(-self.g)*beta*self.gradT/self.T(1.0)
        # w2thm=(kH2/k2)*(self.gradT*cv*self.gamma/(-self.g)-1)*grad_ad*(-self.g)*beta*self.gradT/self.T(0.0)

        # print(w2thp, wtop**2)
        # print(w2thm, wbot**2)

        # print(self.rhobot/rhotop, (wtop/wbot)**(2*bm1))

        # print("......")
        # print("......")


    def get_growth_rate(self):

        a2=-self.HT-self.RX+(kx*kz/k2)*self.grad_u0
        a1=self.HT*self.RX - (kH2/k2)*(self.g/self.hp)*self.ad -(kx*kz/k2)*self.grad_u0*(self.HT+self.RX) 
        a0=0.0#(kH2/k2)*(self.g/self.hp)*self.dia + (kx*kz/k2)*self.grad_u0*(self.HT*self.RX) 
        w=max_root(a0,a1,a2)
        
        return w


    def compute_gradients(self, rhop, rhom, zp, zm):
        dz=zp-zm
        dlogPdz = (np.log(self.P(zp, rhop)) - np.log(self.P(zm, rhom))) / dz
        dlogTdz = (np.log(self.T(zp)) - np.log(self.T(zm))) / dz
        dlogmudz = (np.log(self.mu(zp)) - np.log(self.mu(zm))) / dz

        hp = -1.0 / dlogPdz
        grad_T = -hp * dlogTdz
        grad_mu = -hp * dlogmudz
        grad_ad = (self.gamma-1)/self.gamma

        return hp, grad_T, grad_ad, grad_mu

    def show_param(self):

        print("gamma =",self.gamma)
        print("grad_ad =",self.grad_ad)
        print("g =",self.g)
        print("rhobot =",self.rhobot)
        print("mu0 =",self.mu0)
        print("mu1 =",self.mu1)
        print("kB =",self.kB)
        print("Tbot =",self.Tbot)
        print("Xbot =",self.Xbot)
        print("gradT =",self.gradT)
        print("HT =",self.HT)
        print("RX =",self.RX)
        print("QA =",self.QA)
        print("nz =",self.nz)
        print("gradu0 =",self.grad_u0)

