import pyvista
import numpy as np


prefix="/Users/administrateur/Desktop/these/Codes/ARK/ark-2-mhd/build/shear_insta_w_final/zone_param_"
nt=40

def generate_filename(index, nt):
    filenames = []
    for i in range(nt):
        filename = f"{prefix}{index}_time_{i:09d}.vti"
        filenames.append(filename)
    return filenames

def get_max_abs_uz(file):
    reader = pyvista.get_reader(file)
    time=reader.read()["time"]
    d=reader.read()["d"]
    mz=reader.read()["mz"]
    mx=reader.read()["mx"]
   #return time[0], np.mean(np.abs((mz/d))), np.max(np.abs(mx/d))
    return time[0], np.max(np.abs(mz/d)), np.max(np.abs(mx/d))


def get_series(index):

    filenames=generate_filename(index, nt)

    t=np.zeros(nt)
    uz=np.zeros(nt)
    ux=np.zeros(nt)
    shear=np.zeros(nt)

    for k,file in enumerate(filenames):
        t[k], uz[k], ux[k]= get_max_abs_uz(file)

    return t,uz,ux
