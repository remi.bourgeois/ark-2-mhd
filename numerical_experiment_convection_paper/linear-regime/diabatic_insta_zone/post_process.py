import numpy as np
import vtk
import pyvista

f = open("grid.txt", "w")

phi_min = 0.05
phi_max = 1

h_min = 0.05
h_max = 1

N=20

nx=50
ny=2
nz=25

def ix_(i):

    a = i%(nx*ny)
    ix = a%nx
    iy = (a-ix)/nx
    iz = (i-ix - iy*nx) / (nx*ny)

    return ix


print(nx*ny*nz)
for phi in range(20):
    for h in range(20):

        L=[]

        instable = 1

        for k in range(25):

            if(k<10):
                string = "0"+str(k)
            else:
                string=str(k)

            reader = pyvista.get_reader("9_june_ddia_chem_driven_big_H/output_convection_phi_"+str(phi)+"_h_"+str(h)+"_time_0000000"+string+".vti")

            L.append( np.mean( abs(reader.read()['mz'])/reader.read()['d'] ) )

            if( (L[k]-L[k-1] < 0)and(k>12) ):
                instable=0

        #    if( ix_(np.argmax(reader.read()['mz']))  > 24):
        #        instable=0

        if (L[len(L)-1]<0.4*L[0]) :
            instable =0

        if (L[len(L)-1]>L[0]):
            instable = 1


        print("last = ",L[len(L)-1]/L[0])
        print("phi=",phi,"h=",h,instable)

        f.write(str(phi_min+phi*(phi_max-phi_min)/(N-1))+"\n")
        f.write(str(h_min+h*(h_max-h_min)/(N-1))+"\n")
        f.write(str(instable)+"\n")
f.close()
