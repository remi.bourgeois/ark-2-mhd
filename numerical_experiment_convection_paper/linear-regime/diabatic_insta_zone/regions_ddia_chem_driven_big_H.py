import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc

def f_ad(h):
    return 1


def f_dia(h):
    return h

N=1000
h=np.linspace(0,2,N)



fig=plt.figure()

ax=fig.add_subplot(111, label="1")
ax.plot(h, np.ones(N), color="black",label=r"$\phi = 1$")
ax.plot(h, f_dia(h), color="blue", label=r"$\phi = h$")

ax.fill_between(h,f_dia(h),np.ones(N)*2,color="blue",label="Double Diabatic unstable" )
ax.fill_between(h,np.ones(N)*1,np.ones(N)*2,color="grey",label="Adiabatic and Diabatic unstable" )

f = open("grid.txt", "r")
# first_stable = True
# first_unstable = True
# for phi in range(20):
#     for h in range(20):
#
#         phi_ = float(f.readline())
#         h_ = float(f.readline())
#         instable = int(f.readline())
#
#         if (instable):
#             color = "green"
#             if first_unstable:
#                 label = "Unstable in simulation"
#                 first_unstable=False
#             else:
#                 label=None
#
#         else:
#             color = 'red'
#             if first_stable:
#                 label = "Stable in simulation"
#                 first_stable=False
#             else:
#                 label=None
#
#
#         ax.scatter(h_, phi_,s=4, color=color, zorder=10, label=label)



ax.set_xlabel(r"$r= R_X/Q_A$", color="black")
ax.set_ylabel(r"$\phi = -\nabla_{\mu}/C_kB_0^2$", color="black")
ax.legend(loc='lower right',prop={'size': 8})
ax.set_ylim([0, 2])
ax.set_xlim([0, 2])

plt.savefig('ddia_chem_driven_big_H_no_simu.png',figsize=(16, 12), dpi=200)
