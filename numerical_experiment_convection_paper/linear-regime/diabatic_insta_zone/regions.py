import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc

def f_ad(h):
    return 1

def f_dia(h):
    return 0.5*(h+1)


def f_ddia(h):
    return h

N=1000
h=np.linspace(0,2,N)



fig=plt.figure()

ax=fig.add_subplot(111, label="1")
ax.plot(h, np.ones(N), color="black",label=r"$\phi = 1$")
ax.plot(h, f_dia(h), color="red", label=r"$\phi = \frac{1}{2}(h+1)$")
ax.plot(h, f_ddia(h), color="blue", label=r"$\phi = h$")

ax.fill_between(h,f_ddia(h),np.ones(N)*2,color="blue",label="Double diabatic unstable" )
ax.fill_between(h,f_dia(h),np.ones(N)*2,color="red",label="Diabatic unstable" )
ax.fill_between(h,np.ones(N)*1,np.ones(N)*2,color="grey",label="Adiabatic unstable" )


ax.set_xlabel(r"$h=H_T/Q_A = H_T/R_X$", color="black")
ax.set_ylabel(r"$\phi = (\nabla_T - \nabla_{ad})/(\nabla_\mu + C_kB_0^2)$", color="black")
ax.legend(loc='lower right',prop={'size': 8})
ax.set_ylim([0, 2])
ax.set_xlim([0, 2])

plt.savefig('double_diabatic_diagram.png',figsize=(16, 12), dpi=200)
