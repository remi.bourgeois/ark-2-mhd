import numpy as np

phi_min = 0.05
phi_max = 1.0

r_min = 0.05
r_max = 1.0

tEnd=200.0
cfl=0.9

nx=50
ny=2
nz=25

nOutput=4000


List_B=np.zeros(20)

List_B[0]=(0.692719)
List_B[1]=(0.489826)
List_B[2]=(0.399941)
List_B[3]=(0.346359)
List_B[4]=(0.309793)
List_B[5]=(0.282801)
List_B[6]=(0.261823)
List_B[7]=(0.261823)
List_B[8]=(0.230906)
List_B[9]=(0.219057)
List_B[10]=(0.208863)
List_B[11]=(0.199971)
List_B[12]=(0.192126)
List_B[13]=(0.185137)
List_B[14]=(0.178859)
List_B[15]=(0.173180)
List_B[16]=(0.168009)
List_B[17]=(0.163275)
List_B[18]=(0.158921)
List_B[19]=(0.154897)

List_B*=np.sqrt(1)

Nphi=20
Nh=20
k=0
for i_phi in range(Nphi):

    directory="."

    phi_target=str(phi_min + i_phi*(phi_max-phi_min)/(Nphi-1))
    Bx_bottom=str(List_B[i_phi])


    for i_h in range(Nh):

        RX = str(-(r_min + i_h*(r_max-r_min)/(Nh-1)))

        prefix = "output_convection_phi_"+str(i_phi)+"_h_"+str(i_h)

        f = open("test_convection_phi_"+str(k)+".ini", "x")
        f.write("\n[run]\nsolver=godunov\nriemann=mhd_all_regime_3w\n")
        f.write("all_regime_correction=true"+"\n")
        f.write("muscl_enabled=false"+"\n")
        f.write("tEnd="+str(tEnd)+"\n")

        f.write("nStepmax=100000\ninfo=1000\n")

        f.write("cfl="+str(cfl)+'\n')

        f.write("\n[mesh]\n")

        f.write("nx="+str(nx)+"\n")
        f.write("ny="+str(ny)+"\n")
        f.write("nz="+str(nz)+"\n")

        f.write("boundary_type_xmin=3\nboundary_type_xmax=3\nboundary_type_ymin=3\nboundary_type_ymax=3\nboundary_type_zmin=0\nboundary_type_zmax=0\nxmin=0.0\nxmax=2.0\nymin=0.0\nymax=1.0\nzmin=0.0\nzmax=1\n\n[hydro]\ng_z=-1\nconvection_source_term_enabled=true\nQ_source_term=true\nR_source_term=true\nH_source_term=true\n\n[thermo]\ngamma=1.4\nmmw1=1.0\nmmw2=1.1\n\n[output]\n")

        f.write("directory="+directory+"\n")
        f.write("prefix="+prefix+"\n")

        f.write("nOutput="+str(nOutput)+"\nformat=appended\ntype=vtk\n\n[problem]\nname=convection\ndimension=3\namplitude_seed = 0.00000001\nkB = 1.0\nQA = -1.0\nHT = -1000\ndensity_bottom=10\nT_bottom = 100\nu0_bottom = 0\nBy_bottom = 0\nBz_bottom = 0\nX_bottom = 1.0\ngrad_T  = 0\ngrad_u0 = 0\ngrad_X  = -0.5193\n")
        f.write("\n")
        f.write("phi_target="+phi_target+"\n")
        f.write("Bx_bottom="+Bx_bottom+"\n")
        f.write("RX="+RX+"\n")

        k+=1
