import h5py
import numpy as np
import matplotlib.pyplot as plt
nz=75
t=36
ilogtheta=5

file0="profile_restart_0_diatabic_saturation_HQ_ratio_0.5_HT_02.ini.h5"
file1="profile_restart_0_diatabic_saturation_HQ_ratio_0.33_HT_02.ini.h5"
file2="profile_restart_0_diatabic_saturation_HQ_ratio_0.25_HT_02.ini.h5"

Lthetainit=h5py.File(file0,"r+")['vert_prof'][0,0,ilogtheta,:]

Ltheta0=h5py.File(file0,"r+")['vert_prof'][t,0,ilogtheta,:]
Ltheta1=h5py.File(file1,"r+")['vert_prof'][t,0,ilogtheta,:]
Ltheta2=h5py.File(file2,"r+")['vert_prof'][t,0,ilogtheta,:]


off=15
top =75-off
bot =off
th_half=Lthetainit*1/2 - np.mean(Lthetainit[bot:top]*1/2-Ltheta0[bot:top])
th_third=Lthetainit*1/3 - np.mean(Lthetainit[bot:top]*1/3-Ltheta1[bot:top])
th_quarter=Lthetainit*1/4 - np.mean(Lthetainit[bot:top]*1/4-Ltheta2[bot:top])

fig, axs = plt.subplots()
#axs.set_title(r'$\nabla_T-\nabla_{ad}=B_0^2\frac{H_T}{Q_A}=B_0^2\ r $')
axs.set_title('Potential temperature as a function of depth in 2D MHD simulations')

axs.plot(Ltheta0, np.linspace(0,1,nz),label=r'$r = 1/2\  Simu$', color='black')
axs.plot(th_half, np.linspace(0,1,nz),label=r'$r = 1/2\  MLT$', color='black', linestyle='dotted')
axs.plot(Ltheta1, np.linspace(0,1,nz),label=r'$r = 1/3\  Simu$', color='red')
axs.plot(th_third, np.linspace(0,1,nz),label=r'$r = 1/3\  MLT$', color='red', linestyle='dotted')
axs.plot(Ltheta2, np.linspace(0,1,nz),label=r'$r = 1/4 \ Simu$', color='blue')
axs.plot(th_quarter, np.linspace(0,1,nz),label=r'$r = 1/4\  MLT$', color='blue', linestyle='dotted')

#axs.plot(Lthetainit, np.linspace(0,1,nz),label=r'Initial profile', color='green')
plt.xlabel(r'$log\ \theta$')
plt.ylabel(r'z')


axs.legend(loc="lower left", fontsize=10)
fig.set_size_inches(8, 6)
plt.savefig('log_theta_th.png')
