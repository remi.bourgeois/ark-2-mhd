import h5py
import numpy as np
import matplotlib.pyplot as plt
nz=75
t=36
ilogtheta=5

file0="profile_restart_0_diatabic_saturation_HQ_ratio_0.5_HT_02.ini.h5"
file1="profile_restart_0_diatabic_saturation_HQ_ratio_0.5_HT_04.ini.h5"
file2="profile_restart_0_diatabic_saturation_HQ_ratio_0.25_HT_02.ini.h5"
file3="profile_restart_0_diatabic_saturation_HQ_ratio_0.25_HT_04.ini.h5"
file6="profile_restart_0_diatabic_saturation_HQ_ratio_0.33_HT_02.ini.h5"
file7="profile_restart_0_diatabic_saturation_HQ_ratio_0.33_HT_04.ini.h5"

Lthetainit=h5py.File(file0,"r+")['vert_prof'][0,0,ilogtheta,:]

Ltheta0=h5py.File(file0,"r+")['vert_prof'][t,0,ilogtheta,:]
Ltheta1=h5py.File(file1,"r+")['vert_prof'][t,0,ilogtheta,:]
Ltheta2=h5py.File(file2,"r+")['vert_prof'][t,0,ilogtheta,:]
Ltheta3=h5py.File(file3,"r+")['vert_prof'][t,0,ilogtheta,:]
Ltheta6=h5py.File(file6,"r+")['vert_prof'][t,0,ilogtheta,:]
Ltheta7=h5py.File(file7,"r+")['vert_prof'][t,0,ilogtheta,:]

off=10
top =75-off
bot =off
grad0=Ltheta0[ top ]-Ltheta0[ bot ]
grad1=Ltheta1[ top ]-Ltheta1[ bot ]
grad2=Ltheta2[ top ]-Ltheta2[ bot ]
grad3=Ltheta3[ top ]-Ltheta3[ bot ]
grad6=Ltheta6[ top ]-Ltheta6[ bot ]
grad7=Ltheta7[ top ]-Ltheta7[ bot ]

print(100*(grad0-grad1)/grad1)
print(100*(grad2-grad3)/grad3)
print(100*(grad6-grad7)/grad7)


print("1/2 / 1/4",grad0/grad2)
print("1/2 / 1/4",grad1/grad3)
print("1/2 / 1/3",grad0/grad6)
print("1/2 / 1/3",grad1/grad7)

print("r = 1/2; HT=0.02", grad0)
print("r = 1/2; HT=0.04", grad1)

print("r = 1/3; HT=0.02", grad6)
print("r = 1/3; HT=0.04", grad7)

print("r = 1/4; HT=0.02", grad2)
print("r = 1/4; HT=0.04", grad3)


fig, axs = plt.subplots()

#axs.set_title(r'$\nabla_T-\nabla_{ad}=B_0^2\frac{H_T}{Q_A}=B_0^2\ r $')
axs.plot(Ltheta0, np.linspace(0,1,nz),label=r'$r = 1/2; HT=0.02$', color='black')
axs.plot(Ltheta1, np.linspace(0,1,nz),label=r'$r = 1/2; HT=0.04$', color='black',linestyle='dotted')
axs.plot(Ltheta6, np.linspace(0,1,nz),label=r'$r = 1/3; HT=0.02$', color='red')
axs.plot(Ltheta7, np.linspace(0,1,nz),label=r'$r = 1/3; HT=0.04$', color='red',linestyle='dotted')
axs.plot(Ltheta2, np.linspace(0,1,nz),label=r'$r = 1/4; HT=0.02$', color='blue')
axs.plot(Ltheta3, np.linspace(0,1,nz),label=r'$r = 1/4; HT=0.04$', color='blue',linestyle='dotted')
#axs.plot(Ltheta4, np.linspace(0,1,nz),label=r'$r = 1/8; HT=0.02$', color='red')
#axs.plot(Ltheta5, np.linspace(0,1,nz),label=r'$r = 1/8; HT=0.04$', color='red',linestyle='dotted')
axs.plot(Lthetainit, np.linspace(0,1,nz),label=r'Initial profile', color='green')
plt.xlabel(r'$log\ \theta$')
plt.ylabel(r'z')

axs.legend(loc="lower left", fontsize=10)
fig.set_size_inches(8, 6)
plt.savefig('log_theta.png')
