import matplotlib.pyplot as plt
import h5py
import numpy as np
from matplotlib import rc
from uz_from_vtk import *
rc('text', usetex=True)

npoints=1
parameter_label=r"$k_x/k_z$"
parameter_values=[1]

#names=["L21K11"] #Stable conv 
names=["L21K11_lowHT"] #Stable sheared
#names=["L11K21"]#stable shear
#parameter_values=[2,3, 4,6,8,9]

#names=["L11K21", "L1.51K31", "L11K41", "L1.51K61","L11K81","L1.51K91"]
colors=["blue", "cyan", "red", "purple","black","pink"]


def plot_time_series(parameter_values, parameter_label, names, npoints):
    
    fig, axs = plt.subplots()

    for k in range(npoints):
        add_param_plot(parameter_values[k], names[k], parameter_label, axs, colors[k], k)

    plt.ylabel(r'Energies')
    plt.xlabel('Time')
    plt.legend(loc="lower right", fontsize=12)
    axs.set_yscale('log')
    plt.grid()
    plt.savefig("suivi_energies_shear2D.pdf", dpi=200)
    plt.clf()
    plt.close()

def add_param_plot(parameter_value, name, parameter_label, axs, color, n):
    
    time, Ekinz, Ekinx= get_series(n, name)
    #stable conv L21K11
    #axs.plot(time[0:50], Ekinz[0:50],label=r"$e_{kin}^z$", color=color,linestyle='solid')
    #axs.plot(time[0:50], Ekinx[0:50],label=r"$e_{kin}^x$", color=color,linestyle='dotted')
    #stable sheared conv L21K11
    axs.plot(time[0:400], Ekinz[0:400],label=r"$e_{kin}^z$ ", color=color,linestyle='solid')
    axs.plot(time[0:400], Ekinx[0:400],label=r"$e_{kin}^x$ ", color=color,linestyle='dotted')
    #stable sheared conv L21K11 zoomed
    #axs.plot(time[16:400], Ekinz[16:400],label=r"$e_{kin}^z$", color=color,linestyle='solid')
    #axs.plot(time[16:400], Ekinx[16:400],label=r"$e_{kin}^x$", color=color,linestyle='dotted')

    #plt.axvline(x=220, color="black", linestyle="dashed") #stable conv L21K11


    #plt.axvline(x=160, color="black", linestyle="dashed") #stable sheared conv L21K11
    #plt.axvline(x=1500, color="black", linestyle="dashed") #stable sheared conv L21K11

    #stable conv zoom
    #axs.plot(time[25:400], Ekinz[25:400],label=r"$e_{kin}^z$", color=color,linestyle='solid')
    #axs.plot(time[25:400], Ekinx[25:400],label=r"$e_{kin}^x$", color=color,linestyle='dotted')
    
    #stable conv zoom
    #axs.plot(time[0:400], Ekinz[0:400],label=r"$e_{kin}^z$", color=color,linestyle='solid')
    #axs.plot(time[0:400], Ekinx[0:400],label=r"$e_{kin}^x$", color=color,linestyle='dotted')
    #plt.axvline(x=259, color="black", linestyle="dashed") #

plot_time_series(parameter_values, parameter_label, names, npoints)
