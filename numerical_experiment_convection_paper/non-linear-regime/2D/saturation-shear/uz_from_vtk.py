import pyvista
import numpy as np


prefix="/Users/administrateur/Desktop/these/Codes/ARK/ark-2-mhd/build/"
nt=200

def generate_filename(index, nt, name):
    filenames = []
    for i in range(nt):
        filename = f"{prefix+name}_time_{i:09d}.vti"
        filenames.append(filename)
    return filenames

def get_max_abs_uz(file):
    reader = pyvista.get_reader(file)
    time=reader.read()["time"]
    d=reader.read()["d"]
    mz=reader.read()["mz"]
    mx=reader.read()["mx"]

    Ekinz=np.mean(mz*mz/d) 
    Ekinx=np.mean(mx*mx/d) + 1e-16

    return time[0], Ekinz, Ekinx


def get_series(index, name):

    filenames=generate_filename(index, nt, name)

    t=np.zeros(nt)
    uz=np.zeros(nt)
    ux=np.zeros(nt)
    shear=np.zeros(nt)

    for k,file in enumerate(filenames):
        t[k], uz[k], ux[k]= get_max_abs_uz(file)

    return t,uz,ux
