import matplotlib.pyplot as plt
import h5py
import numpy as np
from matplotlib import rc

rc('text', usetex=True)

npoints=2
parameter_label=r"$k_x/k_z$"
parameter_values=["cube","Elongated box"]
 
names=["L111K221","L211K121"] 
#names=["L211K121"] 

colors=["blue", "red", "red", "purple","black","pink"]

# def evaluate_shear_from_profile(u):

#     S=0
#     nz=150
#     dz=1.0/nz


#     span = range(30,120)

#     # for iz in span:
        
#     #     derivative=(u[iz+1]-u[iz])/dz
#     #     S+=np.abs(derivative)*dz

#     # return S

#     umax=np.max(u[span])
#     umin=np.min(u[span])

#     zmax=np.argmax(u[span])*dz
#     zmin=np.argmin(u[span])*dz

#     return np.abs((umax-umin)/(zmax-zmin))

#     #x, y_pred, alpha, beta=regression(np.linspace(0,1,150),u[:])

#     #return np.abs(beta)

def plot_time_series(parameter_values, parameter_label, names, npoints):
    
    fig, axs = plt.subplots()

    for k in range(npoints):
        add_param_plot(parameter_values[k], names[k], parameter_label, axs, colors[k], k)

    plt.ylabel(r'Energies')
    plt.xlabel('Time')
    plt.legend(loc="center right", fontsize=10)
    axs.set_yscale('log')
    plt.grid()
    plt.savefig("rectangle_zoom.pdf", dpi=200)
    plt.clf()
    plt.close()

def add_param_plot(parameter_value, name, parameter_label, axs, color, n):
    
    file_mean="shear3D/" + "mean_restart_0_"+name+ '.h5'

    time_r = h5py.File(file_mean, "r+")['time'][()]

    k=1
    while (time_r[k]>0):
        k=k+1

    time = h5py.File(file_mean, "r+")['time'][0:k]
    ekinx = h5py.File(file_mean, "r+")['ekinx'][0:k]
    ekiny = h5py.File(file_mean, "r+")['ekiny'][0:k]
    ekinz = h5py.File(file_mean, "r+")['ekinz'][0:k]
    ekin = h5py.File(file_mean, "r+")['ekin'][0:k]

    #print(np.max(ekinx+ekiny+ekinz - 2*ekin))
    #print(ekinz)

    avg_ekinz=np.mean(ekinz[20:])
    avg_ekiny=np.mean(ekiny[20:])
    avg_ekinx=np.mean(ekinx[20:])

    avg_ekin=avg_ekinx+avg_ekiny+avg_ekinz 

    pctx=str(100*avg_ekinx/avg_ekin)[0:4]
    pcty=str(100*avg_ekiny/avg_ekin)[0:4]
    pctz=str(100*avg_ekinz/avg_ekin)[0:4]

    axs.plot(time[20:], ekinz[20:],label=r"$e_{kin}^z\ \approx$"+pctz+" \%", color="black", linestyle='solid')
    axs.plot(time[20:], ekinx[20:],label=r"$e_{kin}^x\ \approx$"+pctx+" \%", color="red",linestyle='dashed')
    axs.plot(time[20:], ekiny[20:],label=r"$e_{kin}^y\ \approx$"+pcty+" \%", color="blue",linestyle='dotted')



    #plt.axvline(x=160, color="black", linestyle="dashed") #stable sheared conv L21K11
    #plt.axvline(x=1500, color="black", linestyle="dashed") #stable sheared conv L21K11



def plot_avg_velocity_profile(names, npoints):

    for n in range(npoints):
            file_mean="shear3D/" + "mean_restart_0_"+names[n]+ '.h5'
            file_profile="shear3D/" + "profile_restart_0_"+names[n]+ '.h5'
            vert_prof = h5py.File(file_profile, "r+")['vert_prof'][()]
            time_r = h5py.File(file_mean, "r+")['time'][()]

            k=1
            while (time_r[k]>0):
                k=k+1
            u_series = np.array(vert_prof[0:k-1, 0, 3, :])
            v_series = np.array(vert_prof[0:k-1, 0, 4, :])

            nz=np.shape(u_series)[-1]

            u_mean=np.mean(u_series[10:,:], axis=0)
            v_mean=np.mean(v_series[10:,:], axis=0)

            if (names[n]=="L111K221"):
                 type="Cube"
            else:
                 type="Elongated Box"

            plt.plot(u_mean[10:40], np.linspace(0.2,0.8,int(nz*0.6)), label="u "+type, color=colors[n])
            plt.plot(v_mean[10:40], np.linspace(0.2,0.8,int(nz*0.6)), label="v "+type, color=colors[n], linestyle="dotted")

    plt.legend()
    plt.ylabel(r'Altitude z')
    plt.xlabel('Horizontal velocities')
    plt.legend(loc="center right", fontsize=10)
    plt.grid()
    plt.savefig("shear_profiles_3D.pdf", dpi=200)
    plt.clf()
    plt.close()

plot_time_series(parameter_values, parameter_label, names, npoints)
plot_avg_velocity_profile(names, npoints)
