import h5py
import numpy as np
import matplotlib.pyplot as plt
nz=75
t=48
ilogtheta=5

file0="profile_restart_0_dynamo_HT_1.2.h5"
file1="profile_restart_0_dynamo_HT_.12.h5"
file2="profile_restart_0_dynamo_HT_.012.h5"

Ltheta0=h5py.File(file0,"r+")['vert_prof'][t,0,4,:]
Ltheta1=h5py.File(file1,"r+")['vert_prof'][t,0,4,:]
Ltheta2=h5py.File(file2,"r+")['vert_prof'][t,0,4,:]

fig, axs = plt.subplots()

axs.set_title(r'dynamo')
axs.plot(Ltheta0, np.linspace(0,1,nz),label=r'$HT=1.2$', color='black')
axs.plot(Ltheta1, np.linspace(0,1,nz),label=r'$HT=.12$', color='blue')
axs.plot(Ltheta2, np.linspace(0,1,nz),label=r'$HT=.012$', color='red')

plt.xlabel(r'$log\ \theta$')
plt.ylabel(r'z')

axs.legend(loc="lower left", fontsize=10)
plt.savefig('log_theta.png')
