import numpy as np
import matplotlib.pyplot as plt
H=1.2
R=0.6 

S=R+H 

print(1/S)

L1s=np.linspace(0.25, 1.5, 8)

for invS in L1s:
    print( "S=",1/invS, "HT=", -2/(3*invS), "RX=", -1.0/(3*invS) )


grad_T, grad_ad, grad_mu = -0.21499391766704026, 0.28571428571428564, -0.31098508432179234


def CB02(invS):
    HT= -2/(3*invS)
    RX= -1.0/(3*invS)
    QA = -0.03

    dia=(1.0/(RX+HT))*(RX*(grad_T-grad_ad)-grad_mu*HT)
    ad=(QA/(RX+HT))*(     grad_T-grad_ad -grad_mu  )

    print(dia,ad)
    res=dia+ad

    
    print(res)
    return res


plt.plot(L1s[:], CB02(L1s[:]))

print("ratio",CB02(L1s[-1])/CB02(L1s[0]))
plt.show()