import h5py
import numpy as np
import matplotlib.pyplot as plt

BC=""

file_3="dynamo/mean_restart_0_dynamo_HT_1.2_5"+BC+".h5"
file_25="dynamo/mean_restart_0_dynamo_HT_1.2_4"+BC+".h5"
file_2="dynamo/mean_restart_0_dynamo_HT_1.2"+BC+".h5"
file_15="dynamo/mean_restart_0_dynamo_HT_1.2_1"+BC+".h5"
file_1="dynamo/mean_restart_0_dynamo_HT_1.2_2"+BC+".h5"


emag3=h5py.File(file_3,"r+")['emag']
ekin3=h5py.File(file_3,"r+")['ekin']
ad_3=0.1045
ad__3=np.mean(emag3[40:])

emag25=h5py.File(file_25,"r+")['emag']
ekin25=h5py.File(file_25,"r+")['ekin']
ad_25=0.2197
ad__25=np.mean(emag25[40:])

emag2=h5py.File(file_2,"r+")['emag']
ekin2=h5py.File(file_2,"r+")['ekin']
ad_2=0.3344
ad__2=np.mean(emag2[40:])

emag15=h5py.File(file_15,"r+")['emag']
ekin15=h5py.File(file_15,"r+")['ekin']
ad_15=0.4487
ad__15=np.mean(emag15[40:])

emag1=h5py.File(file_1,"r+")['emag']
ekin1=h5py.File(file_1,"r+")['ekin']
ad_1=0.5625 #ad aussi
ad__1=np.mean(emag1[40:])

fig, axs = plt.subplots()

axs.set_title(r'dynamo')

axs.plot(emag3,label=r'$e_{mag}\ \nabla T= 3$', color='black',linestyle='dotted')
axs.plot(ekin3,label=r'$e_{kin}\ \nabla T= 3$', color='black')

axs.plot(emag25,label=r'$e_{mag}\ \nabla T= 2.5$', color='red',linestyle='dotted')
axs.plot(ekin25,label=r'$e_{kin}\ \nabla T= 2.5$', color='red')

axs.plot(emag2,label=r'$e_{mag}\ \nabla T= 2$', color='blue',linestyle='dotted')
axs.plot(ekin2,label=r'$e_{kin}\ \nabla T= 2$', color='blue')

axs.plot(emag15,label=r'$e_{mag}\ \nabla T= 1.5$', color='green',linestyle='dotted')
axs.plot(ekin15,label=r'$e_{kin}\ \nabla T= 1.5$', color='green')

axs.plot(emag1,label=r'$e_{mag}\ \nabla T= 1$', color='cyan',linestyle='dotted')
axs.plot(ekin1,label=r'$e_{kin}\ \nabla T= 1$', color='cyan')

plt.ylabel(r'$energies$')
plt.xlabel(r'time')

print(ad_3/ad_2, ad_25/ad_2, ad_15/ad_2,ad_1/ad_2)
print(ad__3/ad__2, ad__25/ad__2, ad__15/ad__2,ad_1/ad__2)

axs.legend(loc="lower right", fontsize=10)
axs.set_yscale('log')
plt.savefig("dynamo non conservatif B"+BC+".png")
