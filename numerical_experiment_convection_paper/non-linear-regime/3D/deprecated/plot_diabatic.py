from params import params
from plotter import *
from regression import *

#suffix of the files to get data from
suffix="diabatic_gradmu4"
folder="data_1mode/"

#Number of parametric datapoints and physical qqt (see params.py)
npoints = 1

rhobot=10
QA=-0.01
HT=-1.2
RX=-0.6
Tbot=10
gradT=2.5
Xbot=0.98
mu0=10
mu1=14

Lx=0.5

#Parametric study parameters
parameter_label=r"$\nabla_{\mu}$"

def gradX(k):
    gradX=-0.96 - k*0.02
    return gradX

def parameter_value(Params):
    return Params.grad_mu

parameter_values=[]
params_list=[]
for k in range(npoints):
   
    Params=params(rhobot=rhobot, QA=QA, HT=HT, RX=RX, Tbot=Tbot, gradT=gradT, Xbot=Xbot, gradX=gradX(k), mu0=mu0, mu1=mu1)
    parameter_values.append(parameter_value(Params))
    params_list.append(Params)

files_means    = [folder + "mean_restart_0_"+suffix+"_" + str(k) + '.h5' for k in range(npoints)]
files_profiles = [folder + "profile_restart_0_"+suffix+"_" + str(k) + '.h5' for k in range(npoints)]

list_B02=plot_time_series(files_means, files_profiles, parameter_values, parameter_label, suffix, npoints, params_list)

#theoretical plot
kx=2*3.14/Lx
C_k= kx*kx*(Params.hp/(Params.g))
beta_th=1.0/C_k
y_th =  beta_th * np.array(parameter_values)
#Regression

x,y_pred,_,_= regression(parameter_values, list_B02)
plot_regression(parameter_values, list_B02, y_pred, y_th, parameter_label, suffix)