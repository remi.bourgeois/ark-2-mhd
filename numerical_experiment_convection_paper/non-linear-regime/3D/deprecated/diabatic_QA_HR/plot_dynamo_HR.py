import h5py
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rc

max=8

gamma=1.4
grad_ad=(gamma-1.0)/gamma
g=-1
rhobot=10
mu0=10
mu1=14
kB=1.
Tbot=10
Xbot=0.98
grad_X=-0.96
gradT=2.5
HT=-1.2
RX=-0.6

Lx=(1.0/3)/(2*3.14)



rc('text', usetex=True)

def mu(z):
    
    X=Xbot+z*grad_X
    mum1 = X/mu0 + (1.0-X)/mu1

    return 1./mum1

def T(gradT,z):
    return Tbot + z*gradT

def grad_calc(gradT):
    ztop=1
    zbot=0

    Ttop=T(gradT,ztop)
    Tbot=T(gradT,zbot)

    mubot=mu(zbot)
    mutop=mu(ztop)

    pbot=rhobot*kB*Tbot/mubot

    nz=150
    dz=(ztop-zbot)/nz
    K=dz*g/(2*kB)

    rhom=rhobot

    for iz in range(0,nz):
        zp = zbot + iz*dz
        zm = zbot + (iz-1)*dz

        Tp = T(gradT,zp)
        Tm = T(gradT,zm)

        mup = mu(zp)
        mum = mu(zm)

        rhop = rhom*(K+Tm/mum)/(Tp/mup-K)
        rhom=rhop

    rhotop=rhop
    ptop=rhotop*kB*Ttop/mutop

    dlogPdz =(np.log(ptop)-np.log(pbot))/(ztop-zbot)
    dlogTdz =(np.log(Ttop)-np.log(Tbot))/(ztop-zbot)
    dlogmudz=(np.log(mutop)-np.log(mubot))/(ztop-zbot)

    hp = -1.0/(dlogPdz) 

    grad_T= -hp*dlogTdz
    grad_mu=-hp*dlogmudz

    return hp, grad_T, grad_ad, grad_mu

def plot_ekin_emag(file, label, color,axs):
    time=[]
    emag=[]
    ekin=[]
    time_r=h5py.File(file,"r+")['time']

    k=1
    while (time_r[k]>0):
        k=k+1

    print(k,"global reductions have been done during restart",label)

    time=time[0:k]
    emag=h5py.File(file,"r+")['emag'][0:k,0]
    ekin=h5py.File(file,"r+")['ekin'][0:k,0]
    time=h5py.File(file,"r+")['time'][0:k]

    axs.plot(time, emag,label=r'$e_{mag},\ Q_A=$'+str(label)[0:6], color=color,linestyle='dotted')
    axs.plot(time, ekin,label='_nolegend_', color=color)

    return np.mean(emag[200:])

folder='mean/'
prefix = 'mean_restart_0_diabatic_QA_HR_'

param=[-0.01-k*0.001 for k in range(0,max)]

files = [folder+prefix+str(k)+'.h5' for k in range(0,max)]
colors=['black','red','blue','orange','green', 'cyan','purple','grey']

LB02=[]
fig, axs = plt.subplots()

for k,file in enumerate(files):
    print(file,k)
    LB02.append(plot_ekin_emag(file, param[k], colors[k],axs)*2*2/3)

axs.set_title(r'Energy time series')
plt.ylabel(r'$Energies$')
plt.xlabel(r'time (s)')
plt.legend(fontsize=9)
axs.set_yscale('log')
plt.savefig("suivi_energies_diabatic_QA_HR.png", dpi=200)
plt.clf()


# Convert lists to numpy arrays
x = np.array(param[:])
y = np.array(LB02[:])

# Calculate the mean of x and y
x_mean = np.mean(x)
y_mean = np.mean(y)

# Calculate the terms needed for the numerator and denominator of beta
numerator = 0
denominator = 0
for i in range(len(x)):
    numerator += (x[i] - x_mean) * (y[i] - y_mean)
    denominator += (x[i] - x_mean) ** 2

# Calculate beta and alpha
beta = numerator / denominator
alpha = y_mean - (beta * x_mean)

hp, grad_T, grad_ad, grad_mu=grad_calc(gradT)

C_k= (hp/(-g*Lx*Lx*rhobot))

print("hp=",hp, "grad_T- grad_ad, grad_mu=", grad_T-grad_ad, grad_mu)
alpha_th=(1.0/(RX+HT))*(RX*(grad_T-grad_ad)-grad_mu*HT)/C_k
beta_th =(1.0/(RX+HT))*(     grad_T-grad_ad -grad_mu  )/C_k

#print(hp, grad_T-grad_ad, grad_mu)
print("HR",alpha, beta)
#print(alpha_th, beta_th)
#print(alpha/alpha_th, beta/beta_th)
# Predict y values
y_pred = alpha + beta * x
y_th = alpha_th + beta_th * x

# Plotting the original data points
plt.scatter(x, y, color='blue', label='Measurement from simulation')
print(y)
# Plotting the line of linear regression
plt.plot(x, y_pred, color='red', label='Linear regression')
#plt.plot(x, y_th, color='green', label='Theory prediction')

plt.xlabel(r"$Q_A$")
plt.ylabel(r"$B_0^2=\frac{4}{3}e_{mag}$")
plt.title(r"Linear regression of $B_0^2$ as a function of $Q_A$")

# Adding legend
plt.legend(fontsize=10)

# Show the plot
plt.savefig("regression_diabatic_QA_HR", dpi=200)
