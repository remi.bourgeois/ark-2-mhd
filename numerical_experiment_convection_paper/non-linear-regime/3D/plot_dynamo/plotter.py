from utils import *
import matplotlib.pyplot as plt
import h5py
import numpy as np
from matplotlib import rc
rc('text', usetex=True)


def plot_time_series(files_mean, files_profiles, parameter_values, parameter_label, suffix, npoints, params_list, range_avg):
    list_reg=[[],[]]
    fig, axs = plt.subplots()

    for k in range(npoints):
        B02, shear=add_param_plot(files_mean[k], files_profiles[k], parameter_values[k], colors[k], parameter_label, axs, params_list[k], range_avg)
        list_reg[0].append(B02)
        list_reg[1].append(shear)

    axs.set_title('Energy time series')
    plt.ylabel('Energies')
    plt.xlabel('Time (s)')
    plt.legend(fontsize=10)
    axs.set_yscale('log')
    plt.savefig("suivi_energies_"+suffix+".png", dpi=200)
    plt.clf()

    return list_reg

def add_param_plot(file_mean, file_profile, parameter_value, color, parameter_label, axs, Params, range_avg):
    time_r = h5py.File(file_mean, "r+")['time'][()]
    vert_prof = h5py.File(file_profile, "r+")['vert_prof'][()]

    k=1
    while (time_r[k]>0):
        k=k+1
    
    time, B02, ekin = h5py.File(file_mean, "r+")['time'][0:k], h5py.File(file_mean, "r+")['B02_middle'][0:k, 0], h5py.File(file_mean, "r+")['ekin'][0:k, 0]

    u_series = np.array(vert_prof[:, 0, 3, :])
    v_series = np.array(vert_prof[:, 0, 4, :])

    range_avg_shear=range_avg
    mean_u = evaluate_shear_from_profile(np.mean(u_series[range_avg_shear,:], axis=0))
    mean_v = evaluate_shear_from_profile(np.mean(v_series[range_avg_shear,:], axis=0))

    shear_series_u = np.array([evaluate_shear_from_profile(u_series[t,:]) for t in range(k)])
    shear_series_v = np.array([evaluate_shear_from_profile(v_series[t,:]) for t in range(k)])

    shear_series=(shear_series_u+shear_series_v)

    axs.plot(time, B02,label=parameter_label+"="+str(parameter_value)[0:7], color=color,linestyle='dotted')
    axs.plot(time, ekin, label='_nolegend_', color=color)
    #axs.plot(time, shear_series, label='_nolegend_', linestyle="dashed",color=color)

    avg_then_evaluate = mean_u+mean_v
    evaluate_then_avg = np.mean(shear_series[range_avg_shear])

    print("oe",avg_then_evaluate/evaluate_then_avg)
    return np.mean(B02[range_avg]), avg_then_evaluate

def plot_regression_B02(x, y, y_th, parameter_label, suffix, Lx):
    # Plotting the original data points
    plt.scatter(x, y, color='blue', label='Measurement from simulation')

    # Plotting the line of linear regression
    plt.plot(x, y_th, color='red', label='Analytical estimate '+r'$L=$'+str(Lx)[1:5])
    plt.xlabel(parameter_label)
    plt.ylabel(r"$B_0^2=B_x^2+B_y^2$")
    plt.title(r"Regression of the total magnetic energy as a function of "+parameter_label)

    # Adding legend
    plt.legend(fontsize=10)
    plt.grid()
    # Show the plot
    plt.savefig("regression_B02_"+suffix+".png", dpi=200)
    plt.clf()

def plot_regression_shear(x, y, y_th, parameter_label, suffix, a):
    # Plotting the original data points
    plt.scatter(x, y, color='blue', label='Measurement from simulation')

    # Plotting the line of linear regression
    plt.plot(x, y_th, color='red', label='Analytical estimate '+r'$\alpha=$'+str(a)[1:5])
    plt.xlabel(parameter_label)
    plt.ylabel(r"$\mid \partial_z u_0 \mid $")
    plt.title(r"Linear regression of the shear as a function of "+parameter_label)

    # Adding legend
    plt.legend(fontsize=10)
    plt.grid()
    # Show the plot
    plt.savefig("regression_shear_"+suffix+".png", dpi=200)
    plt.clf()



def minimise_error_and_plot(list_reg, params_list, parameter_values, parameter_label, suffix, skip):
    
    #Get values from simulation 
    shear=-np.array(list_reg[1])
    B02=np.array(list_reg[0])
    print("\n")
    print("\n ratio valeurs brutes",shear/B02)
    
    #Get parameter values
    HT=params_list[0].HT 
    QA=params_list[0].QA 
    hp=np.array([param.hp for param in params_list])
    rho=np.array([param.rho for param in params_list])
    g=np.array([param.g for param in params_list])
    ad=np.array([param.ad for param in params_list])

    #Fit B02 to saturate without shear
    result = opt.minimize(fun=error, x0=100, args=(list_reg[0], params_list, skip), tol=1e-15)
    Lx=result.x

    Ck=Lx
    L=2*3.14/np.sqrt(Ck*(2./3))
    print("C_k=",Ck,"L=",L)
    y_th=theory_prediction(Lx, params_list)
    plot_regression_B02(parameter_values, list_reg[0], y_th, parameter_label, suffix, L)
    # #Random value for a
    # a=1.0

    # contrib_shear= a*(hp/g)*shear*(HT+QA)
    # contrib_B02= (2*3.14/Lx)**2*(hp/(rho*g))*B02

    # print("ratio contrib si B02 sature", contrib_shear/contrib_B02)
    # print("B02 sature seul ?", (ad-contrib_B02)/ad)
    # print("saturation si B02 sature", (ad-contrib_B02-contrib_shear)/ad)

    #Fit shear to saturate without shear
    # result = opt.minimize(fun=error_shear, x0=100, args=(list_reg[1], params_list, skip), tol=1e-15)
    # a=result.x
    # print("a=",a)
    # y_th=theory_prediction_shear(a, params_list)
    # plot_regression_shear(parameter_values, list_reg[1], y_th, parameter_label, suffix, a)

    # #Random value for Lx
    # Lx=1.0
    # contrib_shear= a*(hp/g)*shear*(HT+QA)
    # kx=2*3.14/Lx
    # contrib_B02= (2*3.14/Lx)**2*(hp/(rho*g))*B02

    # print("ratio contrib si shear sature", contrib_shear/contrib_B02)
    # print("shear sature seul ?", (ad-contrib_shear)/ad)
    # print("saturation si shear sature", (ad-contrib_B02-contrib_shear)/ad)

    # print("shear=",shear)