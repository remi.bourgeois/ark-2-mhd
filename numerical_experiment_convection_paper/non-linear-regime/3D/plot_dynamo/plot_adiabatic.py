from params import params
from plotter import *
from regression import *

#suffix of the files to get data from
suffix="gradT"
folder="data_1mode/"
type="ad"

#Number of parametric datapoints and physical qqt (see params.py)
npoints = 8
mu0 = 15
Tbot = 20
rhobot=1.0
HT=-1.5

range_avg=range(30,100)

#Parametric study parameters
parameter_label=r"$\nabla_T-\nabla_{ad}$"

def gradT(k):
    gradT=-10+0.5*k
    return gradT

def parameter_value(Params):
    return Params.grad_T-Params.grad_ad

parameter_values=[]
params_list=[]
for k in range(npoints):

    Params=params(rhobot=rhobot, Tbot=Tbot, gradT=gradT(k), mu0=mu0, HT=HT, type=type)
    parameter_values.append(parameter_value(Params))
    params_list.append(Params)
    print("gradT=", gradT(k), "MaTH=",Params.MaTH, "w=", Params.w, "stratif=", Params.stratif)

print("Var B02=", params_list[0].B02/params_list[-1].B02)

files_means    = [folder + "mean_restart_0_"+suffix+"_" + str(k) + '.h5' for k in range(npoints)]
files_profiles = [folder + "profile_restart_0_"+suffix+"_" + str(k) + '.h5' for k in range(npoints)]

list_reg=plot_time_series(files_means, files_profiles, parameter_values, parameter_label, suffix, npoints, params_list, range_avg)

minimise_error_and_plot(list_reg, params_list, parameter_values, parameter_label, suffix, skip=[-100])
