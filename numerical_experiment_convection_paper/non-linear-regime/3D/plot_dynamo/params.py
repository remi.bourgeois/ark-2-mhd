import numpy as np
from utils import max_root
import matplotlib.pyplot as plt

#Discretisation
ztop, zbot = 1, 0
nz = 150
kxy = 2*3.14
kz  = kxy #170

kH = kxy**2+kxy**2
k2 = kH+kz**2
class params:

    def __init__(self, rhobot=0, QA=0, HT=0, RX=0, Tbot=0, gradT=0, Xbot=0, gradX=0, mu0=0, mu1=0, type=""):

        #Get values
        self.rhobot=rhobot
        self.QA=QA
        self.HT=HT
        self.RX=RX
        self.Tbot=Tbot
        self.gradT=gradT
        self.mu0=mu0
        self.mu1=mu1
        self.Xbot=Xbot
        self.gradX=gradX
        self.type=type
        
        #This never changes in my study
        self.gamma=1.4
        self.kB=1.
        self.g=1.

        #safe default choice 
        if (self.mu1==0):
            self.mu1=mu0

        #Compute gradients 
        self.grad_calc()
    
    def cs(self,z):
        return np.sqrt(self.gamma*self.kB*self.T(z)/self.mu(z))
    
    def mu(self, z):
        X = self.Xbot +  self.gradX*z
        mum1 = X / self.mu0 + (1.0 - X) / self.mu1
        return 1.0 / mum1
    
    def T(self, z):
        return self.Tbot + z * self.gradT
    
    def P(self,z,rho):
        return rho*self.kB*self.T(z)/self.mu(z)
    
    def theta(self,z,rho):
        P=self.P(z,rho)
        T=self.T(z)

        return T/(P**((self.gamma-1)/self.gamma))

    def grad_calc(self):
        
        dz = (ztop - zbot) / nz
        K = dz * (-self.g) / (2 * self.kB)

        rhom = self.rhobot

        L_rho=np.zeros(nz)
        L_hp=np.zeros(nz)
        L_grad_T=np.zeros(nz)
        L_grad_mu=np.zeros(nz)
        L_ad=np.zeros(nz)
        L_dia=np.zeros(nz)
        L_B02_dia=np.zeros(nz)
        L_B02_ad=np.zeros(nz)
        L_shear_ad=np.zeros(nz)

        for iz in range(0, nz):

            #compute next state
            zp = zbot + iz * dz
            zm = zbot + (iz - 1) * dz

            Tp = self.T(zp)
            Tm = self.T(zm)

            mup = self.mu(zp)
            mum = self.mu(zm)

            rhop = rhom * (K + Tm / mum) / (Tp / mup - K)

            #Get local gradient values
            L_hp[iz], L_grad_T[iz], grad_ad, L_grad_mu[iz] = self.compute_gradients(rhop, rhom, zp, zm)
            L_rho[iz]= rhop
            self.grad_ad=grad_ad
            L_dia[iz] = (L_grad_T[iz]-self.grad_ad)*(self.RX+self.QA) - L_grad_mu[iz]*(self.HT+self.QA)
            L_ad[iz]  = L_grad_T[iz]-self.grad_ad - L_grad_mu[iz]
            L_B02_dia[iz] = (self.g*L_rho[iz]/(L_hp[iz]*(self.HT+self.RX)))*L_dia[iz]
            L_B02_ad[iz] = (self.g*L_rho[iz]/(L_hp[iz]))*L_ad[iz]
            L_shear_ad[iz] = (self.g/(L_hp[iz]*(self.HT+self.RX+self.QA)))*L_ad[iz]

            #Check stability locally
            if (self.type=="dia"):
                if (L_ad[iz]>0):
                    print("instable ad")

                if (L_dia[iz]>0):
                    print("stable dia")
            elif(self.type=="ad"):
                if (L_ad[iz]<0):
                    print("stable ad")

                if (L_dia[iz]<0):
                    print("instable dia")
            #End of check

            #Prepare for next iteration
            rhom = rhop

        rhotop=rhop
    
        span=range(30,120)

        if (self.type=="dia"):
            self.B02 = np.mean(L_B02_dia[span])
        elif(self.type=="ad"):
            self.B02 = np.mean(L_B02_ad[span])
            self.shear = np.mean(L_shear_ad[span])

        #fac=0.28*0.28*2
        #print("ratio TH", self.shear/self.B02, 1.0/(self.rhobot*(self.HT+self.QA)))

        self.grad_T=np.mean(L_grad_T[0])
        self.grad_mu=np.mean(L_grad_mu[0])
        self.hp = np.mean(L_hp[0])
        self.ad=np.mean(L_ad[0])
        self.dia=np.mean(L_dia[0])

        wbot=self.get_growth_rate()

        self.grad_T=np.mean(L_grad_T[nz-1])
        self.grad_mu=np.mean(L_grad_mu[nz-1])
        self.hp = np.mean(L_hp[nz-1])
        self.ad=np.mean(L_ad[nz-1])
        self.dia=np.mean(L_dia[nz-1])

        wtop=self.get_growth_rate()

        self.grad_T=np.mean(L_grad_T[span])
        self.grad_mu=np.mean(L_grad_mu[span])
        self.hp = np.mean(L_hp[span])
        self.ad=np.mean(L_ad[span])
        self.dia=np.mean(L_dia[span])
        self.rho=np.mean(L_rho[span])

        self.w=self.get_growth_rate()

        self.CS=self.cs(0.5)
        self.MaTH=self.w/self.CS

        self.stratif=rhotop/self.rhobot, wtop/wbot

    def get_growth_rate(self):

        a2=-self.HT-self.RX
        a1=self.HT*self.RX - (kH/k2)*(self.g/self.hp)*self.ad
        a0=(kH/k2)*(self.g/self.hp)*self.dia
        w=max_root(a0,a1,a2)
        return w


    def compute_gradients(self, rhop, rhom, zp, zm):
        dz=zp-zm
        dlogPdz = (np.log(self.P(zp, rhop)) - np.log(self.P(zm, rhom))) / dz
        dlogTdz = (np.log(self.T(zp)) - np.log(self.T(zm))) / dz
        dlogmudz = (np.log(self.mu(zp)) - np.log(self.mu(zm))) / dz

        hp = -1.0 / dlogPdz
        grad_T = -hp * dlogTdz
        grad_mu = -hp * dlogmudz
        grad_ad = (self.gamma-1)/self.gamma

        return hp, grad_T, grad_ad, grad_mu

    def show_param(self):

        print("gamma =",self.gamma)
        print("grad_ad =",self.grad_ad)
        print("g =",self.g)
        print("rhobot =",self.rhobot)
        print("mu0 =",self.mu0)
        print("mu1 =",self.mu1)
        print("kB =",self.kB)
        print("Tbot =",self.Tbot)
        print("Xbot =",self.Xbot)
        print("gradT =",self.gradT)
        print("HT =",self.HT)
        print("RX =",self.RX)
        print("QA =",self.QA)

