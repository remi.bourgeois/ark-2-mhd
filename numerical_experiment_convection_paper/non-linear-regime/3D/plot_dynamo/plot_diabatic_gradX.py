from params import params
from plotter import *
from regression import *
import scipy.optimize as opt

#suffix of the files to get data from
suffix="diabatic_gradmu_highmach"
folder="data_1mode/"
type="dia"

#Number of parametric datapoints and physical qqt (see params.py)
npoints = 8
rhobot=1.0
QA=-0.01
HT=-4.8
RX=-0.4
Tbot=20
gradT=1.5
Xbot=0
mu0=30
mu1=15

range_avg=range(60,120)

#Parametric study parameters
parameter_label=r"$\nabla_{\mu}$"

def gradX(k):
    gradX=0.3+k*0.025
    return gradX

def parameter_value(Params):
    return Params.grad_mu

parameter_values=[]
params_list=[]
for k in range(npoints):
   
    Params=params(rhobot=rhobot, QA=QA, HT=HT, RX=RX, Tbot=Tbot, gradT=gradT, Xbot=Xbot, gradX=gradX(k), mu0=mu0, mu1=mu1, type=type)
    parameter_values.append(parameter_value(Params))
    params_list.append(Params)
    print("gradX=", gradX(k), "MaTH=",Params.MaTH, "w=", Params.w, "stratif=", Params.stratif)

print("Var B02=", params_list[0].B02/params_list[-1].B02)

files_means    = [folder + "mean_restart_0_"+suffix+"_" + str(k) + '.h5' for k in range(npoints)]
files_profiles = [folder + "profile_restart_0_"+suffix+"_" + str(k) + '.h5' for k in range(npoints)]

list_reg=plot_time_series(files_means, files_profiles, parameter_values, parameter_label, suffix, npoints, params_list, range_avg)

minimise_error_and_plot(list_reg, params_list, parameter_values, parameter_label, suffix, skip=[-100])
