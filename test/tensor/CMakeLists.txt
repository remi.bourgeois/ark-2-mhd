add_executable(test_Tensor test_Tensor.cpp)
target_include_directories(test_Tensor PUBLIC ${CMAKE_SOURCE_DIR}/src/experimental)
target_link_libraries(test_Tensor PUBLIC Euler)
add_test(NAME test_Tensor COMMAND test_Tensor)
