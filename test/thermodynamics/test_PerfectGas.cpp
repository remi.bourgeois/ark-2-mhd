#include <iostream>
#include <thermodynamics/PerfectGas.hpp>
#include "HydroUnits.hpp"

int main()
{
    double gamma = {1.4};
    double mu = {28.74};
    double e0 = {124864.56};
    hydro::thermodynamics::PerfectGas air = {gamma, mu, mu, mu, e0, hydro::code_units::SI_Constants::k_b};
    double d = {1.293};
    double p = {101325.0};
    std::cout << "Gamma = " << air.computeAdiabaticIndex() << std::endl;
    std::cout << "Mu    = " << air.computeMeanMolecularWeight() << std::endl;
    std::cout << "Eint  = " << air.computeInternalEnergy(d, p)/d << " J/kg" << std::endl;
    std::cout << "Cs    = " << air.computeSpeedOfSound(d, p) << " m/s" << std::endl;
    std::cout << "T     = " << air.computeTemperature(d, p, 0.0) << " K" << std::endl;
    return 0;
}
