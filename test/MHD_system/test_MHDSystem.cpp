#include <iostream>
#include "MHDSystem.hpp"
#include "thermodynamics/ThermoParams.hpp"

int main()
{
    hydro::thermodynamics::ThermoParams thermo;
    thermo.gamma = 1.4;
    thermo.mmw = 1.0;
    thermo.p0 = 0.0;

    std::cout << "Dimension 3:" << std::endl;
    using EoS       = hydro::MHDSystem::EquationOfState;
    using PrimState = hydro::MHDSystem::PrimState;
    using ConsState = hydro::MHDSystem::ConsState;

    EoS eos(thermo);

    PrimState q1;
    q1.d = 2.0;
    q1.p = 0.0;
    q1.v(0) = 3.0;
    q1.v(1) = 4.0;
    q1.v(2) = 5.0;
    ConsState u1 = hydro::MHDSystem::primitiveToConservative(q1, eos);
    PrimState q2 = hydro::MHDSystem::conservativeToPrimitive(u1, eos);

    std::cout << q1.d << " " << q2.d << " " << u1.d << std::endl;
    std::cout << q1.p << " " << q2.p << " " << u1.e << std::endl;
    std::cout << q1.v(0) << " " << q2.v(0) << " " << u1.m(0) << std::endl;
    std::cout << q1.v(1) << " " << q2.v(1) << " " << u1.m(1) << std::endl;
    std::cout << q1.v(2) << " " << q2.v(2) << " " << u1.m(2) << std::endl;

    std::cout << q1.v.size() << " " << q1.v(0) << " " << q1.v(1) << " " << q1.v(2) << std::endl;
    std::cout << hydro::MHDSystem::computeKineticEnergy(q1) << " ";
    std::cout << hydro::MHDSystem::computeKineticEnergy(u1) << std::endl;

    return 0;
}
