#include <DistributedMemorySession.hpp>
#include <HydroUniformGrid.hpp>
#include <HydroTypes.hpp>

int main(int argc, char** argv)
{
    Session::initialize(argc, argv);

    std::cout << "Dimension 3:" << std::endl;
    hydro::UniformGrid grid {{-0.5, -0.5, -0.5},
                             { 0.5,  0.5,  0.5},
                             {10, 10, 10},
                             {1, 1, 1},
                             0};
    std::cout << grid.lo(0) << " " << grid.lo(1) << " " << grid.lo(2) << std::endl;
    std::cout << grid.hi(0) << " " << grid.hi(1) << " " << grid.hi(2) << std::endl;
    std::cout << grid.dl(0) << " " << grid.dl(1) << " " << grid.dl(2) << std::endl;
    std::cout << grid.nbCells() << std::endl;
    std::cout << grid.coordToIndex({0, 0, 7}) << std::endl;
    std::cout << (grid.coordToIndex(grid.indexToCoord(7)) == 7 ? "true" : "false") << std::endl;

    Session::finalize();

    return 0;
}
