//#pragma once

#include "HydroBaseKernel.hpp"
#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"
#include "ars/HydroRiemannAllRegime.hpp"
#include "ars/MHDRiemann_AllRegime_3W.hpp"
#include "ars/MHDRiemann_AllRegime_5W.hpp"

#include <Kokkos_Core.hpp>

namespace hydro
{

template < class RiemannSolver >
class FluxesAndUpdateKernel : public BaseKernel
{
public:
    using Super = BaseKernel;

    using MHD = MHDSystem;
    using EquationOfState = typename MHD::EquationOfState;
    using ConsState = typename MHD::ConsState;
    using PrimState = typename MHD::PrimState;
    using RealVector = RealVector3d;

    using IntVector = IntVectorNd< three_d >;
    using TeamPolicy = Kokkos::TeamPolicy< Kokkos::IndexType< Int > >;
    using Team = typename TeamPolicy::member_type;
    static constexpr int ghostDepth = 0;

    FluxesAndUpdateKernel( const Params& params, const UniformGrid& grid,
                           const Array3d& u, const Array3d& q,   const Kokkos::Array<Array3d, 2*three_d>& qr,  Real dt )
        : m_u( u )
        , m_cu( u )
        , m_q( q )
        , m_qr( qr )
        , m_grid( grid )
        , m_eos( params.thermo )
        , m_riemann_solver( params, m_eos, m_grid )
        , m_dt( dt )
        , m_powell_st (params.run.powell_st)
        , m_beta_threshold(params.run.beta_threshold)
        , m_alfven_threshold(params.run.alfven_threshold)
        , m_muscl_enabled(params.run.muscl_enabled)
# if defined(MPI_SESSION)
, m_mpi_z_rank(grid.comm.getCoords(grid.comm.rank())[IZ])
#else
, m_mpi_z_rank(0)
#endif
    {
    }

    KOKKOS_INLINE_FUNCTION
    void operator()( const Int ix, const Int iy, const Int iz ) const
    {
    const Int j = m_grid.coordToIndex({ix,iy,iz});

    ConsState u_j = Super::getCons( m_cu, j );

    PrimState q_j = Super::getPrim( m_q, j );

    bool powell_st_local = false;

    Real beta=1.0;
    Real alfven=1.0;

    if (m_powell_st)
    {
        beta = q_j.p/MHD::computeMagneticEnergy(q_j);
        alfven = sqrt(dot(q_j.v, q_j.v)*q_j.d/dot(q_j.B, q_j.B));
        if (beta   < m_beta_threshold  ) powell_st_local = true;
        if (alfven > m_alfven_threshold) powell_st_local = true;
    }

    // Compute divergence of B
    //Real divB = computeDivergenceB(ix, iy, iz);
    //auto B = u_j.B;
    //auto v = q_j.v;

    updateAlongFace( Tags::DirX, Tags::SideL, j, u_j, powell_st_local );
    updateAlongFace( Tags::DirX, Tags::SideR, j, u_j, powell_st_local );
    updateAlongFace( Tags::DirY, Tags::SideL, j, u_j, powell_st_local );
    updateAlongFace( Tags::DirY, Tags::SideR, j, u_j, powell_st_local );
    updateAlongFace( Tags::DirZ, Tags::SideL, j, u_j, powell_st_local );
    updateAlongFace( Tags::DirZ, Tags::SideR, j, u_j, powell_st_local );

    //u_j.m -= divB*B;
    //u_j.e -=(dot(B,v))*divB;
    Super::set( m_u, j, u_j );

     } 
    


    template < int idir, int iside >
    KOKKOS_FORCEINLINE_FUNCTION void
    updateAlongFace( std::integral_constant< int, idir > idir_tag,
                     std::integral_constant< int, iside > iside_tag, Int j, ConsState& u_j, bool powell_st_local ) const
    {
        using namespace constants;

        /* const IntVector coord = m_grid.indexToCoord(j);
        const bool BCbot = (m_mpi_z_rank==0)&&(idir==IZ)&&(coord[IZ]==m_grid.m_ghostWidths[ IZ ])&&(iside==0);
        const bool BCtop = (m_mpi_z_rank==m_grid.m_dom[IZ]-1)&&(idir==IZ)&&(coord[IZ]==m_grid.m_nbCells[ IZ ]+m_grid.m_ghostWidths[ IZ ]-1)&&(iside==1); */
    //if (m_muscl_enabled && (BCtop==false) && (BCbot==false))

        const Int j_L = iside == 0 ? j - m_grid.m_strides[ idir ] : j;
        const Int j_R = iside == 0 ? j : j + m_grid.m_strides[ idir ];

        PrimState q_L, q_R;

        if (m_muscl_enabled)
        {
          q_L = Super::getPrim( m_qr[1+2*idir], j_L );
          q_R = Super::getPrim( m_qr[0+2*idir], j_R );
        }
        else
        {
          q_L = Super::getPrim( m_q, j_L );
          q_R = Super::getPrim( m_q, j_R );
        }

        const Real side = iside == 0 ? -one : +one;
        const Real B_powell = iside == 0 ? Super::getPrim( m_q, j_R ).B(idir) : Super::getPrim( m_q, j_L ).B(idir);

        const ConsState flux = m_riemann_solver( q_L, q_R, side, idir, powell_st_local, B_powell) ;

        const Real sdtdSdV = side * m_dt * m_grid.getdSdV( j, idir_tag, iside_tag );
        u_j.d -= sdtdSdV * flux.d;
        u_j.m -= sdtdSdV * flux.m;
        u_j.e -= sdtdSdV * flux.e;
        u_j.B -= sdtdSdV * flux.B;
        u_j.dX -= sdtdSdV * flux.dX;

    }

    KOKKOS_INLINE_FUNCTION
    Real computeDivergenceB(const Int ix, const Int iy, const Int iz) const
    {
        const Real Bx_plus = Super::getPrim(m_q, m_grid.coordToIndex({ ix + 1, iy, iz })).B(0);
        const Real Bx_minus = Super::getPrim(m_q, m_grid.coordToIndex({ ix - 1, iy, iz })).B(0);
        const Real By_plus = Super::getPrim(m_q, m_grid.coordToIndex({ ix, iy + 1, iz })).B(1);
        const Real By_minus = Super::getPrim(m_q, m_grid.coordToIndex({ ix, iy - 1, iz })).B(1);
        const Real Bz_plus = Super::getPrim(m_q, m_grid.coordToIndex({ ix, iy, iz + 1 })).B(2);
        const Real Bz_minus = Super::getPrim(m_q, m_grid.coordToIndex({ ix, iy, iz - 1 })).B(2);

        const Real divB = (Bx_plus - Bx_minus) / (2.0 * m_grid.dl(0)) +
                          (By_plus - By_minus) / (2.0 * m_grid.dl(1)) +
                          (Bz_plus - Bz_minus) / (2.0 * m_grid.dl(2));

        return divB;
    }

    const Array3d m_u;
    const ConstArray3d m_cu;
    const ConstArray3d m_q;
    const Kokkos::Array<Array3d, 2*three_d> m_qr;
    const UniformGrid m_grid;
    const EquationOfState m_eos;
    const RiemannSolver m_riemann_solver;
    Real m_dt;

    bool m_powell_st;
    Real m_beta_threshold;
    Real m_alfven_threshold;
    bool m_muscl_enabled;
    int m_mpi_z_rank;
};

} // namespace hydro
