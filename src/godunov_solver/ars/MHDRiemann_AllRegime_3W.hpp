#pragma once

#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"

#include <cmath>

namespace hydro { namespace riemann
{

class MHD_AllRegime_3W
{
public:
    using MHD           = MHDSystem;
    using EquationOfState = typename MHD::EquationOfState;
    using ConsState       = typename MHD::ConsState;
    using PrimState       = typename MHD::PrimState;

    MHD_AllRegime_3W(const Params& params, const EquationOfState& eos, const UniformGrid& grid);
    //MHD_AllRegime_3W(const MHD_AllRegime_3W& x) = default;
    //MHD_AllRegime_3W(MHD_AllRegime_3W&& x) = default;
    //~MHD_AllRegime_3W() = default;
    //MHD_AllRegime_3W& operator=(const MHD_AllRegime_3W& x) = default;
    //MHD_AllRegime_3W& operator=(MHD_AllRegime_3W&& x) = default;

    KOKKOS_FORCEINLINE_FUNCTION ConsState
    operator()(const PrimState &q_L, const PrimState &q_R, const int& side, const int& idir, bool st_powell, Real B_powell) const;

  private:
    const EquationOfState m_eos;
    const Real m_K;
    const RealVector3d m_g;
    const RealVector3d m_dl;
    const bool m_all_regime_correction;

};
inline
MHD_AllRegime_3W::MHD_AllRegime_3W(const Params& params, const EquationOfState& eos, const UniformGrid& grid)
    : m_eos {eos}
    , m_K   {params.hydro.K}
    , m_g   {params.hydro.g}
    , m_dl  {grid.m_dl}
    , m_all_regime_correction {params.run.all_regime_correction}

{
}

KOKKOS_FORCEINLINE_FUNCTION
typename MHD_AllRegime_3W::ConsState
MHD_AllRegime_3W::operator()(const PrimState &q_L, const PrimState &q_R, const int& side,const int& idir, bool st_powell, Real B_powell) const
{
    using namespace constants;

    const Real rho_L = q_L.d;
    const Real p_L   = q_L.p;
          Real emag  = MHD::computeMagneticEnergy(q_L);
    const Real c_L   = rho_L*MHD::computeFastMagnetoAcousticSpeed(q_L, m_eos, idir);
    const Real c0_L   = MHD::computeSpeedOfSound(q_L, m_eos);

    Vector<three_d, Real> pL;
    pL          = -q_L.B( idir )*q_L.B;
    pL(idir) += p_L + emag;

    const Real rho_R = q_R.d;
    const Real p_R   = q_R.p;
               emag  = MHD::computeMagneticEnergy(q_R);
    const Real c_R   = rho_R*MHD::computeFastMagnetoAcousticSpeed(q_R, m_eos, idir);
    const Real c0_R   = MHD::computeSpeedOfSound(q_R, m_eos);

    Vector<three_d, Real> pR;
    pR           = -q_R.B( idir )*q_R.B;
    pR(idir) += p_R + emag;

    Vector<three_d, Real> ustar, pstar, gdl, M;

    gdl(IX) = m_g[IX]*m_dl[IX];
    gdl(IY) = m_g[IY]*m_dl[IY];
    gdl(IZ) = m_g[IZ]*m_dl[IZ];

    M = 0.5*(q_L.d + q_R.d)*gdl;

    const Real a    = m_K * std::fmax(c_R, c_L);
    const Real a_m1 = 1.0/a;

    ustar = 0.5*(q_R.v+q_L.v)-0.5*a_m1*(pR-pL-M);

    const Real Ma_norm = std::fabs(ustar(idir))/std::fmin(c0_L, c0_R);

    const Real theta_norm = m_all_regime_correction ? std::fmin(Ma_norm, 1.0) : 1.0;

    pstar = 0.5*(pR+pL)-0.5*a*theta_norm*(q_R.v-q_L.v);

    PrimState q;
    Real B_next;

    if ( ustar(idir) > zero )
     {
       q = q_L;
       B_next = q_R.B(idir);
     }
     else
     {
       q = q_R;
       B_next = q_L.B(idir);
     }

     //Powell source term

     if (st_powell) // si powell, on fait ce truc non conservatif
     {
      B_next=B_powell;
     }

     ConsState u = MHD::primitiveToConservative(q, m_eos);

     ConsState flux;

      flux.d = ustar(idir)*u.d;
      flux.m = ustar(idir)*u.m +pstar ;
      flux.m ( idir) += -side*M(idir)*0.5;
      flux.e = ustar(idir)*u.e + dot(pstar,ustar);
      flux.e += - 0.5*side*M(idir)*ustar(idir);
      flux.B = ustar(idir)*u.B - B_next*ustar;
      flux.dX = ustar(idir)*u.dX;

    return flux;
}

}}
