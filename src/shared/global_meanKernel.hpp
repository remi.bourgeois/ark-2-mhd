#pragma once

#include "HydroBaseKernel.hpp"
#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"
#include "global_mean.hpp"

#include <Kokkos_Core.hpp>
#include <cmath>

namespace hydro
{

class global_meanKernel : public BaseKernel
{
    using Super = BaseKernel;

    using EquationOfState = MHDSystem::EquationOfState;
    using ConsState = MHDSystem::ConsState;
    using PrimState = MHDSystem::PrimState;
    using VC = MHDSystem::VarCons;
    using VP = MHDSystem::VarPrim;

public:
    global_meanKernel( const Params& params, const UniformGrid& grid, Array3d q )
        : m_grid( grid )
        , m_q( q )
        , m_params( params)
    {
    }

    KOKKOS_INLINE_FUNCTION
    void init( global_mean& dst ) const
    {
        using constants::zero;
        dst.m_emag = zero;
        dst.m_ekin = zero;
        dst.m_B02 = zero;
        dst.m_B02_middle = zero;
        dst.m_ekinx = zero;
        dst.m_ekiny = zero;
        dst.m_ekinz = zero;
    }

    KOKKOS_INLINE_FUNCTION
    void operator()( Int i, Int j, Int k, global_mean& sum ) const
    {
        const PrimState q_j = Super::getPrim( m_q, m_grid.coordToIndex({i,j,k}) );
        // get primitive variables in current cell

        sum.m_emag += MHDSystem::computeMagneticEnergy(q_j);
        sum.m_ekin += MHDSystem::computeKineticEnergy(q_j);
        sum.m_B02 += q_j.B(IX)*q_j.B(IX) + q_j.B(IY)*q_j.B(IY);
        const int nz = m_params.mesh.nbCells[IZ];

        if ((k>=nz*0.2)&&(k<nz*0.8))
        {
            sum.m_B02_middle += q_j.B(IX)*q_j.B(IX) + q_j.B(IY)*q_j.B(IY);
        }

        sum.m_ekinx += q_j.v(IX)*q_j.v(IX)*q_j.d;
        sum.m_ekiny += q_j.v(IY)*q_j.v(IY)*q_j.d;
        sum.m_ekinz += q_j.v(IZ)*q_j.v(IZ)*q_j.d;

    }

    KOKKOS_INLINE_FUNCTION
    void join( global_mean& dst, const global_mean& src ) const
    {
        // sum reduce
        dst.m_emag += src.m_emag;
        dst.m_ekin += src.m_ekin;
        dst.m_B02 += src.m_B02;
        dst.m_B02_middle += src.m_B02_middle;
        dst.m_ekinx += src.m_ekinx;
        dst.m_ekiny += src.m_ekiny;
        dst.m_ekinz += src.m_ekinz;
    }

    KOKKOS_INLINE_FUNCTION
    void join( volatile global_mean& dst, const volatile global_mean& src ) const
    {
        // sum reduce
        dst.m_emag += src.m_emag;
        dst.m_ekin += src.m_ekin;
        dst.m_B02 += src.m_B02;
        dst.m_B02_middle += src.m_B02_middle;
        dst.m_ekinx += src.m_ekinx;
        dst.m_ekiny += src.m_ekiny;
        dst.m_ekinz += src.m_ekinz;
    }

    UniformGrid m_grid;
    ConstArray3d m_q;
    Params m_params;
};

} // namespace hydro
