#pragma once

#include "HydroTypes.hpp"

#include <cmath>
#include <string>

namespace hydro
{

class global_mean
{
public:
    global_mean() noexcept = default;

    explicit global_mean(Real value) noexcept
        : m_emag(value)
        , m_ekin(value)
        , m_B02(value)
        , m_B02_middle(value)
        , m_ekinx(value)
        , m_ekiny(value)
        , m_ekinz(value)
    {
    }

    Real get_emag() const
    {
        return m_emag;
    }

    Real get_ekin() const
    {
        return m_ekin;
    }

    Real get_B02() const
    {
        return m_B02;
    }

    Real get_B02_middle() const
    {
        return m_B02_middle;
    }

    Real get_ekinx() const
    {
        return m_ekinx;
    }

    Real get_ekiny() const
    {
        return m_ekiny;
    }

    Real get_ekinz() const
    {
        return m_ekinz;
    }

    Real m_emag;
    Real m_ekin;
    Real m_B02;
    Real m_B02_middle;
    Real m_ekinx;
    Real m_ekiny;
    Real m_ekinz;

};

}  // hydro
