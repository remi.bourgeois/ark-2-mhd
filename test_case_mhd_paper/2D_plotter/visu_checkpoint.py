import numpy as np
import matplotlib.pyplot as plt
import h5py
from matplotlib import rc
rc('text', usetex=False)

import glob

test="Orszag-Tang_Vortex"
time = 0.5
colmap='inferno'


# all_h5_files = glob.glob("PDI_*.h5")
all_h5_files = glob.glob("PDI_output_checkpoint_*.h5")
# print(all_h5_files)
var_name_list = ["density", "energy", "mx", "my", "mz", "Bx", "By", "Bz", "dX"]

for h5_file in all_h5_files:
    # print(h5_file)
    time_step = "TS"+h5_file.split("_")[-1].split(".")[-2]
    # print(time_step) 
    
    # print(var_name)
  
    f_ar = h5py.File(h5_file)
    for var_name in var_name_list:

        # var_name = var_name_list[i]

        ar   = f_ar['full_field'][var_name_list.index(var_name)][0]
        ar1 = np.squeeze(ar)

        ny= ar1.shape[0]
        nx= ar1.shape[1]
    
        fig, (axAR) = plt.subplots(1, 1)
        img = axAR.imshow(ar1, interpolation='none',cmap=colmap, extent=[0.5, -0.5, -0.5, 0.5])
        axAR.set_title(test+', ' + r'$t='+time_step+'$,'+' '+r'$n_x=n_y='+str(nx)+'$')
        ticks = np.linspace(ar1.min(), ar1.max(), 5, endpoint=True)
        axAR.invert_xaxis()
        plt.colorbar(img,ticks=ticks, label="var_name")
        # plt.show()
        plt.savefig(str(test)+"_"+var_name+"_"+time_step+'.pdf',dpi=200)
        # fig.clf()
        plt.close()
    
# ar   = f_ar['full_field'][1][0]

# ar1 = np.squeeze(ar)

# ny= ar1.shape[0]
# nx= ar1.shape[1]
# print(nx)
# print(ny)

# fig, (axAR) = plt.subplots(1, 1)
# img = axAR.imshow(ar1, interpolation='none',cmap=colmap, extent=[0.5, -0.5, -0.5, 0.5])
# axAR.set_title(test+', ' + r'$t='+str(time)+'$,'+' '+r'$n_x=n_y='+str(nx)+'$')
# ticks = np.linspace(ar1.min(), ar1.max(), 5, endpoint=True)
# axAR.invert_xaxis()
# plt.colorbar(img,ticks=ticks, label="energy")
# plt.show()
# # plt.savefig(str(test)+'.pdf',figsize=(16, 12), dpi=200)
# # fig.clf()
# # plt.close()

