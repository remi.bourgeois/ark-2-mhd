import numpy as np
import matplotlib.pyplot as plt
import h5py
from matplotlib import rc
rc('text', usetex=True)


#Par5ameters
#data file
f_ar5 = h5py.File('output_SS_2nd_order_256_256_time_000000001.h5')
f_ar3 = h5py.File('output_SS_2nd_order_256_256_3w_time_000000001.h5')

#Test case
test="Skewed shock"
#Resolution time
time = 0.03

ar5Bx   = f_ar5['Bx'][0]
ar5By   = f_ar5['By'][0]

ar3Bx   = f_ar3['Bx'][0]
ar3By   = f_ar3['By'][0]

ar5Bx1 = np.squeeze(ar5Bx)
ar5By1 = np.squeeze(ar5By)

ar3Bx1 = np.squeeze(ar3Bx)
ar3By1 = np.squeeze(ar3By)

nx= ar5Bx1.shape[1]
x=np.linspace(0,1,nx)

B_parallel5 = np.zeros(nx)
B_parallel3 = np.zeros(nx)

o=-1.1071487


for i in range(nx):
    B_parallel5[i]=np.cos(o)*ar5Bx1[128][i]-np.sin(o)*ar5By1[128][i]
    B_parallel3[i]=np.cos(o)*ar3Bx1[128][i]-np.sin(o)*ar3By1[128][i]

fig, (axAR) = plt.subplots(1, 1)
axAR.set_title(test+', ' + r'$t='+str(time)+'$,'+' '+r'$n_x=n_y='+str(nx)+'$'+'\n Horizontal slice of '+r'$ B_{//}$')#'=\mathbf{B}\cdot\mathbf{d_{//}}$'+' at '+r'y=0')
plt.plot(x ,B_parallel5,label=r'$B_{//}$'+' '+r'$5+1$'+' waves solver ',color='blue', linewidth=1.25)
plt.plot(x ,B_parallel3,label=r'$B_{//}$'+' '+r'$3+1$'+' waves solver ',color='red', linewidth=1.25)
plt.plot(x ,np.zeros((nx))+1.41047393,label=r'$B_{//}$'+'   Reference',color='black', linewidth=0.5)


plt.xlabel(r'$x$')
plt.ylabel(r'$B_{//}$')
plt.legend(loc='lower left')

plt.savefig(str(test)+'zoomed_out.pdf',figsize=(16, 12), dpi=200)
plt.ylim([1.1,1.7])
plt.legend(loc='upper right')
plt.savefig(str(test)+'usual_scale.pdf',figsize=(16, 12), dpi=200)

fig.clf()
plt.close()
