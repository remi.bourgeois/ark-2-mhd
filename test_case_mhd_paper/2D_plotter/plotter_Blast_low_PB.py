import numpy as np
import matplotlib.pyplot as plt
import h5py
from matplotlib import rc
rc('text', usetex=True)

#Parameters
#data file
f_ar = h5py.File('output_Blast_low_PB_2nd_order_256_256_time_000000001.h5')
#Test case
test="MHD Blast low "+r"$\beta$"
#Resolution time
time = 0.02
#colormap
colmap='inferno'

ar   = f_ar['d'][0]

ar1 = np.squeeze(ar)

ny= ar1.shape[0]
nx= ar1.shape[1]


fig, (axAR) = plt.subplots(1, 1)
img = axAR.imshow(ar1, interpolation='none',cmap=colmap, extent=[0.5, -0.5, -0.5, 0.5])
axAR.set_title(test+', ' + r'$t='+str(time)+'$,'+' '+r'$n_x=n_y='+str(nx)+'$')
ticks = np.linspace(ar1.min(), ar1.max(), 5, endpoint=True)
axAR.invert_xaxis()
plt.colorbar(img,ticks=ticks, label="density")

plt.savefig(str(test)+'.pdf',figsize=(16, 12), dpi=200)
fig.clf()
plt.close()
