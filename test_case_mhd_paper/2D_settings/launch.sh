SCHEFILE=scheduler.json
rm -f scheduler.json
rm -f simulation.yml
rm -f reduction.py
ln -s ../simulation.yml
ln -s ../reduction.py
dask-scheduler --scheduler-file=$SCHEFILE &
dask-worker --local-directory /tmp --scheduler-file=$SCHEFILE &
while ! [ -f $SCHEFILE ]; do
    sleep 3
done
python3 reduction.py
rm -f scheduler.json
rm -f simulation.yml
rm -f reduction.py
