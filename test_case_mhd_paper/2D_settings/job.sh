#!/bin/bash
#SBATCH --job-name=ark-pdi
#SBATCH --output=%x.o%j
#SBATCH --ntasks=4
##SBATCH --ntasks=80
#SBATCH --exclusive
#SBATCH --time=00:10:00
#SBATCH --partition=cpu_short

module purge

module load gcc/11.2.0/gcc-4.8.5 
module load cmake/3.21.4/gcc-11.2.0 
module load openmpi/4.1.1/gcc-11.2.0 
module load cuda/10.2.89/intel-19.0.3.199

source $WORKDIR/project_pdi/pdi_install/share/pdi/env.bash


# module load gcc/9.2.0/gcc-4.8.5 
# module load cuda/10.2.89/intel-19.0.3.199 
# module load intel-mpi/2019.3.199/intel-19.0.3.199
# module load cmake/3.16.2/gcc-9.2.0 
# module load hdf5/1.10.8/intel-20.0.4.304-intel-mpi

# source $WORKDIR/project_pdi/pdi-1.5.2/install/share/pdi/env.bash

srun path_to_main ../test_case_mhd_paper/2D_settings/test_OTang_mpi.ini
