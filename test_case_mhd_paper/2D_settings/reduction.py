###################################################################################################
# Copyright (c) 2020-2022 Centre national de la recherche scientifique (CNRS)
# Copyright (c) 2020-2022 Commissariat a l'énergie atomique et aux énergies alternatives (CEA)
# Copyright (c) 2020-2022 Institut national de recherche en informatique et en automatique (Inria)
# Copyright (c) 2020-2022 Université Paris-Saclay
# Copyright (c) 2020-2022 Université de Versailles Saint-Quentin-en-Yvelines
#
# SPDX-License-Identifier: MIT
#
###################################################################################################

from deisa import Deisa
from dask.distributed import performance_report, wait
import os

os.environ["DASK_DISTRIBUTED__COMM__UCX__INFINIBAND"] = "True"

# Initialize Deisa
Deisa = Deisa('scheduler.json', 'config.yml')

# Get client
client = Deisa.get_client()

# either: Get data descriptor as a list of Deisa arrays object
arrays = Deisa.get_deisa_arrays()
# or: Get data descriptor as a dict of Dask array
# arrays = Deisa.get_dask_arrays()

# Select data
gt = arrays["global_full_field"][:, :, :, :, :]
print(gt, flush=True)

# Check contract
arrays.check_contract()

# Construct a lazy task graph
cpt = gt[:, 0, :, :, :].mean()

# Submit the task graph to the scheduler
s = client.persist(cpt, release=True)

# Sign contract
arrays.validate_contract()
del gt
# Print the result, note that "s" is a future object, to get the result of the computation,
# we call `s.result()` to retreive it.
print(client.compute(s).result(), flush=True)

print("Done", flush=True)
client.shutdown()
