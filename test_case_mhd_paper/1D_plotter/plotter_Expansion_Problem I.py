import numpy as np
import matplotlib.pyplot as plt
import h5py
from matplotlib import rc
rc('text', usetex=True)


#Par5ameters
#data file
f_ar5 = h5py.File('output_Expansion_Problem_I_time_000000001.h5')
f_ref = h5py.File('output_Expansion_Problem_I_ref_time_000000001.h5')

#Test case
test="Expansion problem I"
#Resolution time
time = 0.15
xmax=1.4


ar5d   =  f_ar5['d'][0]
ar5By   = f_ar5['By'][0]

refd   =  f_ref['d'][0]
refBy   = f_ref['By'][0]


ar5d1 = np.squeeze(ar5d )
ar5By1 = np.squeeze(ar5By)

refd1 = np.squeeze(refd)
refBy1 = np.squeeze(refBy)

nx= ar5d1.shape[1]
x=np.linspace(0,xmax,nx)

nxref= refd1.shape[1]
xref=np.linspace(0,xmax,nxref)

fig, (axAR) = plt.subplots(1, 1)
axAR.set_title(test+', ' + r'$t='+str(time)+'$,'+' '+r'$n_x=n_y='+str(nx)+'$,'+' '+r'$\rho$'+' (log-scale)')
plt.plot(x ,ar5d[0][:],label=r'$\rho$'+' '+r'$5+1$'+' waves solver ',color='blue', linewidth=1.75)
plt.plot(xref ,refd[0][:],label=r'$\rho$'+'   Reference',color='black', linewidth=0.75)

plt.xlabel(r'$x$')
plt.ylabel(r'$\rho$'+' (log-scale)')
plt.legend()
plt.xlim([0,xmax])
plt.yscale('log')
plt.savefig(str(test)+'rho.pdf',figsize=(16, 12), dpi=200)
fig.clf()
plt.close()

fig, (axAR) = plt.subplots(1, 1)
axAR.set_title(test+', ' + r'$t='+str(time)+'$,'+' '+r'$n_x=n_y='+str(nx)+'$,'+' '+r'$B_y$'+' (log-scale)')
plt.plot(x ,ar5By[0][:],label=r'$B_y$'+' '+r'$5+1$'+' waves solver ',color='blue', linewidth=1.75)
plt.plot(xref ,refBy[0][:],label=r'$B_y$'+'   Reference',color='black', linewidth=0.75)
plt.xlabel(r'$x$')
plt.ylabel(r'$B_y$'+' (log-scale)')
plt.legend()
plt.xlim([0,xmax])
plt.yscale('log')
plt.savefig(str(test)+'By.pdf',figsize=(16, 12), dpi=200)


fig.clf()
plt.close()
