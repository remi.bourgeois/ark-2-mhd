#pragma once

#include "ViscousStepKernel.hpp"

#include "HydroBaseKernel.hpp"
#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"
#include "linalg/Tensor.hpp"
#include "linalg/Vector.hpp"

#include <Kokkos_Core.hpp>
#include <cmath>

namespace hydro
{

void ExecuteViscousStep( const Params& params, const UniformGrid& grid,
                         const Array3d& q, const Array3d& u, Real dt );

#define ETI_ExecuteViscousStep(  )                                                              \
    void ExecuteViscousStep( const Params& params, const UniformGrid& grid,          \
                             const Array3d& q, const Array3d& u, Real dt )

ETI_ExecuteViscousStep( );

} // namespace hydro
