#pragma once

#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"

#include <cmath>

namespace hydro { namespace riemann
{

class Rusanov
{
public:
    using MHD           = MHDSystem;
    using EquationOfState = typename MHD::EquationOfState;
    using ConsState       = typename MHD::ConsState;
    using PrimState       = typename MHD::PrimState;

    Rusanov(const Params& params, const EquationOfState& eos);

    //Rusanov(const Rusanov& x) = default;

    //Rusanov(Rusanov&& x) = default;

    //~Rusanov() = default;

    //Rusanov& operator=(const Rusanov& x) = default;

    //Rusanov& operator=(Rusanov&& x) = default;

    template<int idir>
    KOKKOS_FORCEINLINE_FUNCTION
    ConsState operator()(const PrimState& q_L, const PrimState& q_R, std::integral_constant<int, idir> idir_tag, const int& side, bool st_powell) const;

private:
    const EquationOfState m_eos;
};
inline
Rusanov::Rusanov(const Params&, const EquationOfState& eos)
    : m_eos ( eos )
{
}

template<int idir>
KOKKOS_FORCEINLINE_FUNCTION
typename Rusanov::ConsState
Rusanov::operator()(const PrimState& q_L, const PrimState& q_R, std::integral_constant<int, idir> idir_tag, const int& side, bool st_powell) const
{
    constexpr Real half = constants::half;

    const ConsState flux_L = MHD::template Flux<idir>(q_L, m_eos);
    const ConsState flux_R = MHD::template Flux<idir>(q_R, m_eos);

    const Real cA_L   = MHD::computeAlfvenSpeed(q_L, idir);
    const Real cFMA_L = MHD::computeFastMagnetoAcousticSpeed(q_L, m_eos, idir);
    const Real c_L = std::fmax(cFMA_L, cA_L);

    const Real cA_R   = MHD::computeAlfvenSpeed(q_R, idir);
    const Real cFMA_R = MHD::computeFastMagnetoAcousticSpeed(q_R, m_eos, idir);
    const Real c_R = std::fmax(cFMA_R, cA_R);

    const Real un_L = q_L.v( idir_tag );
    const Real un_R = q_R.v( idir_tag );

    const Real S = std::fmax(std::fabs(un_L)+c_L, std::fabs(un_R)+c_R);

    const ConsState u_L = MHD::primitiveToConservative(q_L, m_eos);
    const ConsState u_R = MHD::primitiveToConservative(q_R, m_eos);

    // Compute the Godunov flux
    ConsState flux;
    flux.d = half * (flux_L.d + flux_R.d) - half * S * (u_R.d - u_L.d);
    flux.m = half * (flux_L.m + flux_R.m) - half * S * (u_R.m - u_L.m);
    flux.B = half * (flux_L.B + flux_R.B) - half * S * (u_R.B - u_L.B);
    flux.e = half * (flux_L.e + flux_R.e) - half * S * (u_R.e - u_L.e);

    return flux;
}

}}
