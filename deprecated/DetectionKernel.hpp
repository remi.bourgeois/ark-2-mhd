#pragma once

#include "HydroBaseKernel.hpp"
#include "HydroUniformGrid.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"

namespace hydro
{

class DetectionKernel : public BaseKernel
{
    using Super           = BaseKernel;

    using MHD           = MHDSystem;
    using EquationOfState = typename MHD::EquationOfState;
    using ConsState           = typename MHD::ConsState;
    using Grid       = UniformGrid;
    using Array      = Array3d;
    using ConstArray = ConstArray3d;
    using BoolArray = BoolArrayDyn;

    using TeamPolicy = Kokkos::TeamPolicy<Kokkos::IndexType<Int>>;
    using Team       = typename TeamPolicy::member_type;
    static constexpr int ghostDepth = 0;

public:
    DetectionKernel(const Params& params, const Grid& grid, const Array3d& u, BoolArray& to_be_recomputed);

    static void apply(const Params& params, const Grid& grid, const Array3d& u, BoolArray& to_be_recomputed);

    KOKKOS_INLINE_FUNCTION
    void operator()(const Team& team) const;


    const Params m_params;
    const Array3d m_u;
    const Array3d m_q;
    const Grid m_grid;
    BoolArray  m_to_be_recomputed;
    const EquationOfState m_eos;


};

}
