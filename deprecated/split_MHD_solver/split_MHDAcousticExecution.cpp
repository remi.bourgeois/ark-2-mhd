#include "split_MHDAcousticExecution.hpp"

#include "split_MHDAcousticKernel.hpp"

#include "HydroBaseKernel.hpp"
#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"
#include "linalg/Vector.hpp"

#include <Kokkos_Core.hpp>
#include <cmath>

namespace hydro
{

void
ExecuteMHD_AcousticStep( const Params& params, const UniformGrid& grid,
                     const Array3d& u, const Array3d& uAc, const Array3d& q,
                     Real dt )
{
    using Kernel = MHD_AcousticStepKernel;
    using TeamPolicy = typename Kernel::TeamPolicy;
    Kernel kernel( params, grid, u, uAc, q, dt );
    const int league_size = Kernel::computeLeagueSize( grid.m_nbCells, Kernel::ghostDepth );
    const int vector_length = TeamPolicy::vector_length_max();
    TeamPolicy policy { league_size, Kokkos::AUTO, vector_length };
    Kokkos::parallel_for( "Acoustic kernel - TeamPolicy", policy, kernel );
}

ETI_ExecuteMHD_AcousticStep( );

} // namespace hydro
