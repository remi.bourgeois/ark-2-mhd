#pragma once

#include "cxxtimer.hpp"
#include "HydroUniformGrid.hpp"
#include "HydroConstants.hpp"
#include "HydroProblem.hpp"
#include "HydroSolver.hpp"
#include "HydroTypes.hpp"
#include "io/Writer.hpp"
#include "Utils.hpp"
#include "TimeStep.hpp"

#include <memory>
#include <vector>

namespace hydro
{
class split_MHD : public Solver
{
    using Super           = Solver;

    using MHD           = MHDSystem;
    using EquationOfState = typename MHD::EquationOfState;
    using VC              = typename MHD::VarCons;
    using VP              = typename MHD::VarPrim;
    static constexpr int nbvar = MHD::nbvar;

    static constexpr Int ghostWidth {2};

    using Array       = Array3d;
    using HostArray   = HostArray3d;

public:
    split_MHD(std::shared_ptr<Problem> problem);
    split_MHD(const split_MHD& x) = default;
    split_MHD(split_MHD&& x) = default;
#if defined(__INTEL_COMPILER) && __INTEL_COMPILER <= 1900
    ~split_MHD() override {};
#else
    ~split_MHD() override = default;
#endif
    //split_MHD& operator=(const split_MHD& x) = default;
    //split_MHD& operator=(split_MHD&& x) = default;

    Real computeTimeStep() final;
    void nextIteration(Real dt) final;
    void saveOutput() final;
    void prepareNextOutput(Real& dt) final;
    bool finished() const final;
    void printMonitoring(double t_tot) const final;
    bool shouldPrintInformation() const final;
    void printInformation() const final;
    double memoryUsage() const final;

private:
    std::shared_ptr<Problem> m_problem;
    std::shared_ptr<Params> m_params;
    const UniformGrid m_grid;
    std::shared_ptr<io::WriterBase> m_writer;
    bool m_should_save;

    const Array m_u;
    const Array m_uAc;
    const Array m_q;
    const HostArray m_u_host;

    Int m_nStepmax;
    Real m_tEnd;
    TimeStep m_dt;

    std::vector<cxxtimer::Timer> timers;
};

class split_MHD;

}
