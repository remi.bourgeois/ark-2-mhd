#pragma once

#include "HydroBaseKernel.hpp"
#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"
#include "linalg/Vector.hpp"

#include <Kokkos_Core.hpp>
#include <cmath>

namespace hydro
{

class MHD_AcousticStepKernel : public BaseKernel
{
public:
    using Super = BaseKernel;

    using MHD = MHDSystem;
    using EquationOfState = typename MHD::EquationOfState;
    using ConsState = typename MHD::ConsState;
    using PrimState = typename MHD::PrimState;

    using IntVector = IntVectorNd< three_d >;
    using TeamPolicy = Kokkos::TeamPolicy< Kokkos::IndexType< Int > >;
    using Team = typename TeamPolicy::member_type;

    static constexpr int ghostDepth = 1;

    MHD_AcousticStepKernel( const Params& params, const UniformGrid& grid,
                        const Array3d& u, const Array3d& uAc, const Array3d& q,
                        Real dt )
        : m_K( params.hydro.K )
        , m_eos( params.thermo )
        , m_grid( grid )
        , m_u( u )
        , m_uAc( uAc )
        , m_q( q )
        , m_dt( dt )
        , m_dphi {}
    {
        for ( int idir = 0; idir < three_d; ++idir )
        {
            m_dphi( idir ) = -params.hydro.g[ idir ] * grid.dl( idir );
        }
    }

    template < int idir, int iside, std::enable_if_t< ( idir >= three_d ), int > = idir >
    KOKKOS_FORCEINLINE_FUNCTION void updateAlongFace( std::integral_constant< int, idir >,
                                                      std::integral_constant< int, iside >, Int,
                                                      ConsState&, Real& ) const noexcept
    {
    }

    template < int idir, int iside, std::enable_if_t< ( idir < three_d ), int > = idir >
    KOKKOS_FORCEINLINE_FUNCTION void
    updateAlongFace( std::integral_constant< int, idir > idir_tag,
                     std::integral_constant< int, iside > iside_tag, Int j, ConsState& u_j,
                     Real& L ) const noexcept
    {
        using namespace constants;

        const Int j_L = iside == 0 ? j - m_grid.m_strides[ idir ] : j;
        const Int j_R = iside == 0 ? j : j + m_grid.m_strides[ idir ];

        const PrimState q_L = Super::getPrim( m_q, j_L );
        const PrimState q_R = Super::getPrim( m_q, j_R );


        const Real rho_L = q_L.d;
        const Real p_L   = q_L.p;
        Real emag  = MHD::computeMagneticEnergy(q_L);
        const Real c_L   = rho_L*MHD::computeFastMagnetoAcousticSpeed(q_L, m_eos, idir);
        Vector<three_d, Real> pL;
        pL          = -q_L.B( idir )*q_L.B;
        pL(idir) += p_L + emag;

        const Real rho_R = q_R.d;
        const Real p_R   = q_R.p;
        emag  = MHD::computeMagneticEnergy(q_R);
        const Real c_R   = rho_R*MHD::computeFastMagnetoAcousticSpeed(q_R, m_eos, idir);
        Vector<three_d, Real> pR;
        pR           = -q_R.B( idir )*q_R.B;
        pR(idir) += p_R + emag;

        Vector<three_d, Real> ustar, pstar;

        const Real a    = 1.01 * std::fmax(c_R, c_L);
        const Real a_m1 = 1.0/a;

        ustar = 0.5*(q_R.v+q_L.v)-0.5*a_m1*(pR-pL);
        pstar = 0.5*(pR+pL)-0.5*a*1.0*(q_R.v-q_L.v);

        Real B_next;

        if ( ustar(idir) > zero )
         {
           B_next = q_R.B(idir);
         }
         else
         {
           B_next = q_L.B(idir);
         }

        const Real side = iside == 0 ? -one : +one;
        const Real dtdSdV = m_dt * m_grid.getdSdV( j, idir_tag, iside_tag );
        L += dtdSdV * side * ustar(idir);
        u_j.m -= dtdSdV * side * pstar;
        u_j.e -= dtdSdV * side * (dot(ustar,pstar));
        u_j.B -= dtdSdV * side * (- B_next*ustar);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()( const Team& team ) const
    {
        using namespace constants;

        // From given team compute coordinates of cell (0, jy, jz).
        const IntVector coord0 = Super::teamToCoord( team, m_grid, ghostDepth );
        // Get linear index from cell (0, jy, jz).
        const Int j0 = m_grid.coordToIndex( coord0 );

        Int start = m_grid.m_ghostWidths[ IX ] - ghostDepth;
        Int end = m_grid.m_ghostWidths[ IX ] + ( m_grid.m_nbCells[ IX ] + ghostDepth );
        Kokkos::parallel_for( Super::ArkTeamVectorRange( team, start, end ), [ & ]( const Int jx ) {
            const Int j = j0 + jx;
            ConsState u_j = Super::getCons( m_u, j );
            Real L = one;

            updateAlongFace( Tags::DirX, Tags::SideL, j, u_j, L );
            updateAlongFace( Tags::DirX, Tags::SideR, j, u_j, L );
            updateAlongFace( Tags::DirY, Tags::SideL, j, u_j, L );
            updateAlongFace( Tags::DirY, Tags::SideR, j, u_j, L );
            updateAlongFace( Tags::DirZ, Tags::SideL, j, u_j, L );
            updateAlongFace( Tags::DirZ, Tags::SideR, j, u_j, L );

            const Real invL = one / L;
            u_j.d *= invL;
            u_j.e *= invL;
            u_j.m *= invL;
            u_j.B *= invL;


            Super::set( m_uAc, j, u_j );
        } );
    }

private:
    const Real m_K;
    const EquationOfState m_eos;
    const UniformGrid m_grid;
    const ConstArray3d m_u;
    const Array3d m_uAc;
    const ConstArray3d m_q;
    const Real m_dt;
    Vector< three_d, Real > m_dphi;
};

} // namespace hydro
