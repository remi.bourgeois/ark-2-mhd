#include "GravityStepExecution.hpp"

#include "GravityStepKernel.hpp"
#include "HydroBaseKernel.hpp"
#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroProblem.hpp"
#include "HydroSolver.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"

namespace hydro
{

void
ExecuteGravityStep( const Params& params, const UniformGrid& grid,
                    const Array3d& u, const Array3d& q, Real dt )
{
    using Kernel = GravityStepKernel;
    using TeamPolicy = typename Kernel::TeamPolicy;
    GravityStepKernel kernel( params, grid, u, q, dt );
    const int league_size = Kernel::computeLeagueSize( grid.m_nbCells, Kernel::ghostDepth );
    const int vector_length = TeamPolicy::vector_length_max();
    TeamPolicy policy( league_size, Kokkos::AUTO, vector_length );
    Kokkos::parallel_for( "Gravity kernel - TeamPolicy", policy, kernel );
}

ETI_ExecuteGravityStep();

} // namespace hydro
