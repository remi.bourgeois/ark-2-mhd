#include "DetectionKernel.hpp"

#include "HydroBaseKernel.hpp"
#include "HydroUniformGrid.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"

namespace hydro
{

DetectionKernel::DetectionKernel(const Params& params, const Grid& grid, const Array3d& u, BoolArray& to_be_recomputed)
    : m_params {params}
    , m_u      {u}
    , m_grid   {grid}
    , m_to_be_recomputed {to_be_recomputed}
    , m_eos( params.thermo )

{
}

void DetectionKernel::apply(const Params& params, const Grid& grid, const Array3d& u, BoolArray& to_be_recomputed)
{
    DetectionKernel kernel(params, grid, u, to_be_recomputed);

    const int league_size = Super::computeLeagueSize(grid.m_nbCells, ghostDepth);
    const int vector_length = TeamPolicy::vector_length_max();
    TeamPolicy policy {league_size, Kokkos::AUTO, vector_length};
    Kokkos::parallel_for("Detection kernel - TeamPolicy", policy, kernel);

}

KOKKOS_INLINE_FUNCTION
void DetectionKernel::operator()(const Team& team) const
{
    using namespace constants;

    // From given team compute coordinates of cell (0, jy, jz).
    const IntVector coord0 {Super::teamToCoord(team, m_grid, ghostDepth)};
    // Get linear index from cell (0, jy, jz).
    const int j0 {m_grid.coordToIndex(coord0)};

    const auto vec_loop = Super::ArkTeamVectorRange(team, m_grid.m_ghostWidths[IX]-ghostDepth, m_grid.m_nbCells[IX] + m_grid.m_ghostWidths[IX]+ghostDepth);
    Kokkos::parallel_for(Kokkos::ThreadVectorRange(team, vec_loop.start, vec_loop.end),
                         KOKKOS_LAMBDA(const int jx)
                         {
                             const int j{j0 + jx};

                             const ConsState u_j {Super::getCons(m_u, j)};

                             bool is_to_be_recomputed = false;

                             if ( (u_j.d <= 0.0) )//||isnan(u_j.d) )
                             {
                               is_to_be_recomputed=true;
                             }

                             const Real emag = MHD::computeMagneticEnergy(u_j);
                             const Real ecin = MHD::computeKineticEnergy(u_j);

                             const Real eint = u_j.e - emag - ecin;

                             if ( (eint <= 0.0) )//||isnan(eint) )
                             {
                               is_to_be_recomputed=true;
                             }

                             if (is_to_be_recomputed) //Flag and reset all neighbors
                             {

                               m_to_be_recomputed(j) = 1;

                               int j_R, j_L;

                               j_L = j - m_grid.m_strides[IX];
                               j_R = j + m_grid.m_strides[IX];
                               m_to_be_recomputed(j_L) = 1;
                               m_to_be_recomputed(j_R) = 1;

                               j_L = j - m_grid.m_strides[IY];
                               j_R = j + m_grid.m_strides[IY];
                               m_to_be_recomputed(j_L) = 1;
                               m_to_be_recomputed(j_R) = 1;

                               j_L = j - m_grid.m_strides[IZ];
                               j_R = j + m_grid.m_strides[IZ];
                               m_to_be_recomputed(j_L) = 1;
                               m_to_be_recomputed(j_R) = 1;
                             }
                         });
}

}  // hydro
