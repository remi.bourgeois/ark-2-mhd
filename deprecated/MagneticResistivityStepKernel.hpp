#pragma once

#include "HydroBaseKernel.hpp"
#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"
#include "linalg/Tensor.hpp"
#include "linalg/Vector.hpp"

#include <Kokkos_Core.hpp>
#include <cmath>

namespace hydro
{

template < int IN >
struct getTransverse2
{
    static constexpr int IT1 = ( IN + 1 ) % three_d;
    static constexpr int IT2 = ( IN + 2 ) % three_d;
};

class MagneticResistivityStepKernel : public BaseKernel
{
public:
    using Super = BaseKernel;

    using Euler = MHDSystem;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;

    using IntVector = IntVectorNd< three_d >;
    using TeamPolicy = Kokkos::TeamPolicy< Kokkos::IndexType< Int > >;
    using Team = typename TeamPolicy::member_type;

    static constexpr int ghostDepth = 0;

    using RealVector = Vector< three_d, Real >;
    using RealTensor = Tensor< three_d, Real >;

    MagneticResistivityStepKernel( const Params& params, const UniformGrid& grid,
                       const Array3d& q, const Array3d& u, Real dt )
        : m_grid( grid )
        , m_q( q )
        , m_u( u )
        , m_u_const( u )
        , m_mu( params.hydro.mu )
        , m_eta( -constants::two * constants::third * m_mu )
        , m_invdl {}
        , m_dt( dt )
    {
        for ( int idim = 0; idim < three_d; ++idim )
        {
            m_invdl[ idim ] = constants::one / grid.m_dl[ idim ];
        }
    }

    template < int idir >
    KOKKOS_FORCEINLINE_FUNCTION RealTensor
    computeGradB( Tags::Dim1_t, std::integral_constant< int, idir > idir_tag, Int, Int,
                    const RealVector& BL, const RealVector& BR ) const noexcept
    {
        using namespace constants;

        RealTensor gradB;

        // Normal gradient
        gradB( idir_tag, Tags::DirX ) =
            ( BR( Tags::DirX ) - BL( Tags::DirX ) ) * m_invdl[ idir ];

        return gradB;
    }

    template < int idir >
    KOKKOS_FORCEINLINE_FUNCTION RealTensor
    computeGradB( Tags::Dim2_t, std::integral_constant< int, idir > idir_tag, Int j_L, Int j_R,
                    const RealVector& BL, const RealVector& BR ) const noexcept
    {
        using namespace constants;

        RealTensor gradB;

        // Normal gradient
        gradB( idir_tag, Tags::DirX ) =
            ( BR( Tags::DirX ) - BL( Tags::DirX ) ) * m_invdl[ idir ];
        gradB( idir_tag, Tags::DirY ) =
            ( BR( Tags::DirY ) - BL( Tags::DirY ) ) * m_invdl[ idir ];

        {
            constexpr int it = getTransverse2< idir >::IT1;
            std::integral_constant< int, it > it_tag;
            const RealVector BLL = Super::getMagneticField( m_q, j_L - m_grid.m_strides[ it ] );
            const RealVector BRL = Super::getMagneticField( m_q, j_R - m_grid.m_strides[ it ] );
            const RealVector BLR = Super::getMagneticField( m_q, j_L + m_grid.m_strides[ it ] );
            const RealVector BRR = Super::getMagneticField( m_q, j_R + m_grid.m_strides[ it ] );

            const RealVector meanBL = mean( BLL, BRL );
            const RealVector meanBR = mean( BLR, BRR );

            gradB( it_tag, Tags::DirX ) =
                half * ( meanBR( Tags::DirX ) - meanBL( Tags::DirX ) ) * m_invdl[ it ];
            gradB( it_tag, Tags::DirY ) =
                half * ( meanBR( Tags::DirY ) - meanBL( Tags::DirY ) ) * m_invdl[ it ];
        }

        return gradB;
    }

    template < int idir >
    KOKKOS_FORCEINLINE_FUNCTION RealTensor
    computeGradB( Tags::Dim3_t, std::integral_constant< int, idir > idir_tag, const Int j_L,
                    const Int j_R, const RealVector& BL, const RealVector& BR ) const noexcept
    {
        using namespace constants;

        RealTensor gradB;

        // Normal gradient
        gradB( idir_tag, Tags::DirX ) =
            ( BR( Tags::DirX ) - BL( Tags::DirX ) ) * m_invdl[ idir ];
        gradB( idir_tag, Tags::DirY ) =
            ( BR( Tags::DirY ) - BL( Tags::DirY ) ) * m_invdl[ idir ];
        gradB( idir_tag, Tags::DirZ ) =
            ( BR( Tags::DirZ ) - BL( Tags::DirZ ) ) * m_invdl[ idir ];

        {
            constexpr int it = getTransverse2< idir >::IT1;
            std::integral_constant< int, it > it_tag;
            const RealVector BLL = Super::getMagneticField( m_q, j_L - m_grid.m_strides[ it ] );
            const RealVector BRL = Super::getMagneticField( m_q, j_R - m_grid.m_strides[ it ] );
            const RealVector BLR = Super::getMagneticField( m_q, j_L + m_grid.m_strides[ it ] );
            const RealVector BRR = Super::getMagneticField( m_q, j_R + m_grid.m_strides[ it ] );

            const RealVector meanBL = mean( BLL, BRL );
            const RealVector meanBR = mean( BLR, BRR );

            gradB( it_tag, Tags::DirX ) =
                half * ( meanBR( Tags::DirX ) - meanBL( Tags::DirX ) ) * m_invdl[ it ];
            gradB( it_tag, Tags::DirY ) =
                half * ( meanBR( Tags::DirY ) - meanBL( Tags::DirY ) ) * m_invdl[ it ];
            gradB( it_tag, Tags::DirZ ) =
                half * ( meanBR( Tags::DirZ ) - meanBL( Tags::DirZ ) ) * m_invdl[ it ];
        }

        {
            constexpr int it = getTransverse2<idir >::IT2;
            std::integral_constant< int, it > it_tag;
            const RealVector BLL = Super::getMagneticField( m_q, j_L - m_grid.m_strides[ it ] );
            const RealVector BRL = Super::getMagneticField( m_q, j_R - m_grid.m_strides[ it ] );
            const RealVector BLR = Super::getMagneticField( m_q, j_L + m_grid.m_strides[ it ] );
            const RealVector BRR = Super::getMagneticField( m_q, j_R + m_grid.m_strides[ it ] );

            const RealVector meanBL = mean( BLL, BRL );
            const RealVector meanBR = mean( BLR, BRR );

            gradB( it_tag, Tags::DirX ) =
                half * ( meanBR( Tags::DirX ) - meanBL( Tags::DirX ) ) * m_invdl[ it ];
            gradB( it_tag, Tags::DirY ) =
                half * ( meanBR( Tags::DirY ) - meanBL( Tags::DirY ) ) * m_invdl[ it ];
            gradB( it_tag, Tags::DirZ ) =
                half * ( meanBR( Tags::DirZ ) - meanBL( Tags::DirZ ) ) * m_invdl[ it ];
        }

        return gradB;
    }

    template < int idir, int iside, std::enable_if_t< ( idir >= three_d ), int > = idir >
    KOKKOS_FORCEINLINE_FUNCTION void updateAlongFace( std::integral_constant< int, idir >,
                                                      std::integral_constant< int, iside >,
                                                      const Int, ConsState& ) const noexcept
    {
    }

    template < int idir, int iside, std::enable_if_t< ( idir < three_d ), int > = idir >
    KOKKOS_FORCEINLINE_FUNCTION void
    updateAlongFace( std::integral_constant< int, idir > idir_tag,
                     std::integral_constant< int, iside > iside_tag, const Int j,
                     ConsState& u_j ) const noexcept
    {
        using namespace constants;

        constexpr Tags::Tag_t< three_d > dim_tag;

        const Int j_L = iside == 0 ? j - m_grid.m_strides[ idir ] : j;
        const Int j_R = iside == 0 ? j : j + m_grid.m_strides[ idir ];

        const RealVector BL = Super::getMagneticField( m_q, j_L );
        const RealVector BR = Super::getMagneticField( m_q, j_R );

        // Compute Magnetic Field gradient at face (idir, iside)
        // gradB_{i, j} = d(B_i) / dx_j
        const RealTensor gradB = computeGradB( dim_tag, idir_tag, j_L, j_R, BL, BR );

        // Compute divergence by summing diagonal elements
        // div(B) = Sum_j d(B_j) / dx_j
        const Real divu = trace( gradB );

        // Compute normal to face (idir, iside) projection of symetric part of gradB
        // tau_i = 2*mu*Sym(gradB)_{i, idir} + kronecker_{i, idir}*eta*div(B)
        RealVector tau = sym( idir_tag, gradB );
        tau *= two * m_mu;
        tau( idir_tag ) += m_eta * divu;

        const RealVector meanB = mean( BL, BR );
        const Real tau_dot_u = dot( meanB, tau );

        const Real side = iside == 0 ? -one : +one;
        const Real dtdSdV = side * m_dt * m_grid.getdSdV( j, idir_tag, iside_tag );
        u_j.B += dtdSdV * tau;
    }

    KOKKOS_INLINE_FUNCTION
    void operator()( const Team& team ) const
    {
        // From given team compute coordinates of cell (0, jy, jz).
        const IntVector coord0 = Super::teamToCoord( team, m_grid, ghostDepth );
        // Get linear index from cell (0, jy, jz).
        const int j0 = m_grid.coordToIndex( coord0 );

        Int start = m_grid.m_ghostWidths[ IX ] - ghostDepth;
        Int end = m_grid.m_ghostWidths[ IX ] + ( m_grid.m_nbCells[ IX ] + ghostDepth );
        Kokkos::parallel_for( Super::ArkTeamVectorRange( team, j0 + start, j0 + end ),
                              [ & ]( const Int j ) {
                                  ConsState u_j = Super::getCons( m_u_const, j );

                                  updateAlongFace( Tags::DirX, Tags::SideL, j, u_j );
                                  updateAlongFace( Tags::DirX, Tags::SideR, j, u_j );
                                  updateAlongFace( Tags::DirY, Tags::SideL, j, u_j );
                                  updateAlongFace( Tags::DirY, Tags::SideR, j, u_j );
                                  updateAlongFace( Tags::DirZ, Tags::SideL, j, u_j );
                                  updateAlongFace( Tags::DirZ, Tags::SideR, j, u_j );

                                  Super::set( m_u, j, u_j );
                              } );
    }

private:
    UniformGrid m_grid;
    const ConstArray3d m_q;
    Array3d m_u;
    ConstArray3d m_u_const;
    Real m_mu;
    Real m_eta;
    RealVector3d m_invdl;
    Real m_dt;
};

} // namespace hydro
