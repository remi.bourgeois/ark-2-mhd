#pragma once

#include "HydroBaseKernel.hpp"
#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"
#include "linalg/Tensor.hpp"
#include "linalg/Vector.hpp"

#include <Kokkos_Core.hpp>
#include <cmath>

namespace hydro
{

template < int IN >
struct getTransverse
{
    static constexpr int IT1 = ( IN + 1 ) % three_d;
    static constexpr int IT2 = ( IN + 2 ) % three_d;
};

class ViscousStepKernel : public BaseKernel
{
public:
    using Super = BaseKernel;

    using Euler = MHDSystem;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;

    using IntVector = IntVectorNd< three_d >;
    using TeamPolicy = Kokkos::TeamPolicy< Kokkos::IndexType< Int > >;
    using Team = typename TeamPolicy::member_type;

    static constexpr int ghostDepth = 0;

    using RealVector = Vector< three_d, Real >;
    using RealTensor = Tensor< three_d, Real >;

    ViscousStepKernel( const Params& params, const UniformGrid& grid,
                       const Array3d& q, const Array3d& u, Real dt )
        : m_grid( grid )
        , m_q( q )
        , m_u( u )
        , m_u_const( u )
        , m_mu( params.hydro.mu )
        , m_eta( -constants::two * constants::third * m_mu )
        , m_invdl {}
        , m_dt( dt )
    {
        for ( int idim = 0; idim < three_d; ++idim )
        {
            m_invdl[ idim ] = constants::one / grid.m_dl[ idim ];
        }
    }

    template < int idir >
    KOKKOS_FORCEINLINE_FUNCTION RealTensor
    computeGradVel( Tags::Dim1_t, std::integral_constant< int, idir > idir_tag, Int, Int,
                    const RealVector& velL, const RealVector& velR ) const noexcept
    {
        using namespace constants;

        RealTensor gradVel;

        // Normal gradient
        gradVel( idir_tag, Tags::DirX ) =
            ( velR( Tags::DirX ) - velL( Tags::DirX ) ) * m_invdl[ idir ];

        return gradVel;
    }

    template < int idir >
    KOKKOS_FORCEINLINE_FUNCTION RealTensor
    computeGradVel( Tags::Dim2_t, std::integral_constant< int, idir > idir_tag, Int j_L, Int j_R,
                    const RealVector& velL, const RealVector& velR ) const noexcept
    {
        using namespace constants;

        RealTensor gradVel;

        // Normal gradient
        gradVel( idir_tag, Tags::DirX ) =
            ( velR( Tags::DirX ) - velL( Tags::DirX ) ) * m_invdl[ idir ];
        gradVel( idir_tag, Tags::DirY ) =
            ( velR( Tags::DirY ) - velL( Tags::DirY ) ) * m_invdl[ idir ];

        {
            constexpr int it = getTransverse< idir >::IT1;
            std::integral_constant< int, it > it_tag;
            const RealVector velLL = Super::getVelocity( m_q, j_L - m_grid.m_strides[ it ] );
            const RealVector velRL = Super::getVelocity( m_q, j_R - m_grid.m_strides[ it ] );
            const RealVector velLR = Super::getVelocity( m_q, j_L + m_grid.m_strides[ it ] );
            const RealVector velRR = Super::getVelocity( m_q, j_R + m_grid.m_strides[ it ] );

            const RealVector meanVelL = mean( velLL, velRL );
            const RealVector meanVelR = mean( velLR, velRR );

            gradVel( it_tag, Tags::DirX ) =
                half * ( meanVelR( Tags::DirX ) - meanVelL( Tags::DirX ) ) * m_invdl[ it ];
            gradVel( it_tag, Tags::DirY ) =
                half * ( meanVelR( Tags::DirY ) - meanVelL( Tags::DirY ) ) * m_invdl[ it ];
        }

        return gradVel;
    }

    template < int idir >
    KOKKOS_FORCEINLINE_FUNCTION RealTensor
    computeGradVel( Tags::Dim3_t, std::integral_constant< int, idir > idir_tag, const Int j_L,
                    const Int j_R, const RealVector& velL, const RealVector& velR ) const noexcept
    {
        using namespace constants;

        RealTensor gradVel;

        // Normal gradient
        gradVel( idir_tag, Tags::DirX ) =
            ( velR( Tags::DirX ) - velL( Tags::DirX ) ) * m_invdl[ idir ];
        gradVel( idir_tag, Tags::DirY ) =
            ( velR( Tags::DirY ) - velL( Tags::DirY ) ) * m_invdl[ idir ];
        gradVel( idir_tag, Tags::DirZ ) =
            ( velR( Tags::DirZ ) - velL( Tags::DirZ ) ) * m_invdl[ idir ];

        {
            constexpr int it = getTransverse< idir >::IT1;
            std::integral_constant< int, it > it_tag;
            const RealVector velLL = Super::getVelocity( m_q, j_L - m_grid.m_strides[ it ] );
            const RealVector velRL = Super::getVelocity( m_q, j_R - m_grid.m_strides[ it ] );
            const RealVector velLR = Super::getVelocity( m_q, j_L + m_grid.m_strides[ it ] );
            const RealVector velRR = Super::getVelocity( m_q, j_R + m_grid.m_strides[ it ] );

            const RealVector meanVelL = mean( velLL, velRL );
            const RealVector meanVelR = mean( velLR, velRR );

            gradVel( it_tag, Tags::DirX ) =
                half * ( meanVelR( Tags::DirX ) - meanVelL( Tags::DirX ) ) * m_invdl[ it ];
            gradVel( it_tag, Tags::DirY ) =
                half * ( meanVelR( Tags::DirY ) - meanVelL( Tags::DirY ) ) * m_invdl[ it ];
            gradVel( it_tag, Tags::DirZ ) =
                half * ( meanVelR( Tags::DirZ ) - meanVelL( Tags::DirZ ) ) * m_invdl[ it ];
        }

        {
            constexpr int it = getTransverse<idir >::IT2;
            std::integral_constant< int, it > it_tag;
            const RealVector velLL = Super::getVelocity( m_q, j_L - m_grid.m_strides[ it ] );
            const RealVector velRL = Super::getVelocity( m_q, j_R - m_grid.m_strides[ it ] );
            const RealVector velLR = Super::getVelocity( m_q, j_L + m_grid.m_strides[ it ] );
            const RealVector velRR = Super::getVelocity( m_q, j_R + m_grid.m_strides[ it ] );

            const RealVector meanVelL = mean( velLL, velRL );
            const RealVector meanVelR = mean( velLR, velRR );

            gradVel( it_tag, Tags::DirX ) =
                half * ( meanVelR( Tags::DirX ) - meanVelL( Tags::DirX ) ) * m_invdl[ it ];
            gradVel( it_tag, Tags::DirY ) =
                half * ( meanVelR( Tags::DirY ) - meanVelL( Tags::DirY ) ) * m_invdl[ it ];
            gradVel( it_tag, Tags::DirZ ) =
                half * ( meanVelR( Tags::DirZ ) - meanVelL( Tags::DirZ ) ) * m_invdl[ it ];
        }

        return gradVel;
    }

    template < int idir, int iside, std::enable_if_t< ( idir >= three_d ), int > = idir >
    KOKKOS_FORCEINLINE_FUNCTION void updateAlongFace( std::integral_constant< int, idir >,
                                                      std::integral_constant< int, iside >,
                                                      const Int, ConsState& ) const noexcept
    {
    }

    template < int idir, int iside, std::enable_if_t< ( idir < three_d ), int > = idir >
    KOKKOS_FORCEINLINE_FUNCTION void
    updateAlongFace( std::integral_constant< int, idir > idir_tag,
                     std::integral_constant< int, iside > iside_tag, const Int j,
                     ConsState& u_j ) const noexcept
    {
        using namespace constants;

        constexpr Tags::Tag_t< three_d > dim_tag;

        const Int j_L = iside == 0 ? j - m_grid.m_strides[ idir ] : j;
        const Int j_R = iside == 0 ? j : j + m_grid.m_strides[ idir ];

        const RealVector velL = Super::getVelocity( m_q, j_L );
        const RealVector velR = Super::getVelocity( m_q, j_R );

        // Compute velocity gradient at face (idir, iside)
        // gradVel_{i, j} = d(Vel_i) / dx_j
        const RealTensor gradVel = computeGradVel( dim_tag, idir_tag, j_L, j_R, velL, velR );

        // Compute divergence by summing diagonal elements
        // div(Vel) = Sum_j d(Vel_j) / dx_j
        const Real divu = trace( gradVel );

        // Compute normal to face (idir, iside) projection of symetric part of gradVel
        // tau_i = 2*mu*Sym(gradVel)_{i, idir} + kronecker_{i, idir}*eta*div(Vel)
        RealVector tau = sym( idir_tag, gradVel );
        tau *= two * m_mu;
        tau( idir_tag ) += m_eta * divu;

        const RealVector meanVel = mean( velL, velR );
        const Real tau_dot_u = dot( meanVel, tau );

        const Real side = iside == 0 ? -one : +one;
        const Real dtdSdV = side * m_dt * m_grid.getdSdV( j, idir_tag, iside_tag );
        u_j.m += dtdSdV * tau;
        u_j.e += dtdSdV * tau_dot_u;
    }

    KOKKOS_INLINE_FUNCTION
    void operator()( const Team& team ) const
    {
        // From given team compute coordinates of cell (0, jy, jz).
        const IntVector coord0 = Super::teamToCoord( team, m_grid, ghostDepth );
        // Get linear index from cell (0, jy, jz).
        const int j0 = m_grid.coordToIndex( coord0 );

        Int start = m_grid.m_ghostWidths[ IX ] - ghostDepth;
        Int end = m_grid.m_ghostWidths[ IX ] + ( m_grid.m_nbCells[ IX ] + ghostDepth );
        Kokkos::parallel_for( Super::ArkTeamVectorRange( team, j0 + start, j0 + end ),
                              [ & ]( const Int j ) {
                                  ConsState u_j = Super::getCons( m_u_const, j );

                                  updateAlongFace( Tags::DirX, Tags::SideL, j, u_j );
                                  updateAlongFace( Tags::DirX, Tags::SideR, j, u_j );
                                  updateAlongFace( Tags::DirY, Tags::SideL, j, u_j );
                                  updateAlongFace( Tags::DirY, Tags::SideR, j, u_j );
                                  updateAlongFace( Tags::DirZ, Tags::SideL, j, u_j );
                                  updateAlongFace( Tags::DirZ, Tags::SideR, j, u_j );

                                  Super::set( m_u, j, u_j );
                              } );
    }

private:
    UniformGrid m_grid;
    const ConstArray3d m_q;
    Array3d m_u;
    ConstArray3d m_u_const;
    Real m_mu;
    Real m_eta;
    RealVector3d m_invdl;
    Real m_dt;
};

} // namespace hydro
