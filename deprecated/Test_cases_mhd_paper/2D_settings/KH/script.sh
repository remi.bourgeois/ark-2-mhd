#!/bin/bash

cd ../../../build_cuda
./main ../Test_cases_mhd_paper/2D_settings/KH/test_KH_1st_order_64.ini
./main ../Test_cases_mhd_paper/2D_settings/KH/test_KH_1st_order_128.ini
./main ../Test_cases_mhd_paper/2D_settings/KH/test_KH_1st_order_256.ini
./main ../Test_cases_mhd_paper/2D_settings/KH/test_KH_1st_order_512.ini
./main ../Test_cases_mhd_paper/2D_settings/KH/test_KH_1st_order_1024.ini
./main ../Test_cases_mhd_paper/2D_settings/KH/test_KH_2nd_order_64.ini
./main ../Test_cases_mhd_paper/2D_settings/KH/test_KH_2nd_order_128.ini
./main ../Test_cases_mhd_paper/2D_settings/KH/test_KH_2nd_order_256.ini
./main ../Test_cases_mhd_paper/2D_settings/KH/test_KH_2nd_order_512.ini
./main ../Test_cases_mhd_paper/2D_settings/KH/test_KH_2nd_order_1024.ini
