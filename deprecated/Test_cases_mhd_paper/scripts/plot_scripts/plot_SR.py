import numpy as np
import matplotlib.pyplot as plt
import h5py

n_simu=100
n_ref =10000

f_ar3 = h5py.File('../../outputs/test_SR_AR3/output_SR_AR3_time_000000002.h5')
f_ar5 = h5py.File('../../outputs/test_SR_AR5/output_SR_AR5_time_000000002.h5')
f_hlld = h5py.File('../../outputs/test_SR_HLLD/output_SR_HLLD_time_000000002.h5')
f_ref = h5py.File('../../reference_solutions/SR_ref.h5')

ar3_d = f_ar3['d'][:]
ar5_d = f_ar5['d'][:]
hlld_d = f_hlld['d'][:]
ref_d  = f_ref['d'][:]

ar3_By = f_ar3['By'][:]
ar5_By = f_ar5['By'][:]
hlld_By = f_hlld['By'][:]
ref_By  = f_ref['By'][:]

x=np.linspace(0,1.,n_simu)
x_ref=np.linspace(0,1.,n_ref)
plt.figure()

plt.xlabel('x')
plt.ylabel('density')
plt.title(r" Slow rarefaction")


plt.plot(x    ,ar3_d[0][0][:],label='Density, AR 3 waves n='+str(n_simu),color='blue')
plt.plot(x    ,ar5_d[0][0][:],label='Density, AR 5 waves n='+str(n_simu),color='red')
plt.plot(x    ,hlld_d[0][0][:],label='Density, HLLD  n='+str(n_simu),color='purple')
plt.plot(x_ref   ,ref_d[0][0][:],label='Density,reference' ,color='black')


plt.legend(prop={"size":6})
plt.savefig('../../SR_density.png')

plt.figure()

plt.xlabel('x')
plt.ylabel('density')
plt.title(r" Slow rarefaction")


plt.plot(x    ,ar3_By[0][0][:],label='By, AR 3 waves n='+str(n_simu),color='blue')
plt.plot(x    ,ar5_By[0][0][:],label='By, AR 5 waves n='+str(n_simu),color='red')
plt.plot(x    ,hlld_By[0][0][:],label='By, HLLD  n='+str(n_simu),color='purple')
plt.plot(x_ref   ,ref_By[0][0][:],label='By,reference' ,color='black')


plt.legend(prop={"size":6})
plt.savefig('../../SR_By.png')

plt.clf()
