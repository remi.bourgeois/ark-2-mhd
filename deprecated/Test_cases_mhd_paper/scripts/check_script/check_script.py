import numpy as np
import matplotlib.pyplot as plt
import h5py
import math
import sys

mystring = sys.argv[1]

f_data = h5py.File(mystring)

data = f_data['d'][:]

(nx, ny, nz)=data.shape

NanFound = False

for ix in range(nx):
    for iy in range(ny):
        for iz in range ( nz):
             if (math.isnan(data[ix][iy][iz])):
                NanFound = True

if (NanFound):
    print('\033[0;31m Admissibility Test has failed (Nan value(s) found) \033[0;0m')
else:
    print('\033[0;32m Admissibility Test Passed (Finite final solution)\033[0;0m')
