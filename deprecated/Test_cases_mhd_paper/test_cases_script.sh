#!/bin/bash
#Test of the code with several test cases
#Compile the code with HDF5_C on and execute this script from this folder
#You need wget, python3, hdf5 for python, numpy and matplotlib

#import the reference solutions


#Create the output file
mkdir outputs
mkdir outputs/test_DW_AR3
mkdir outputs/test_DW_AR5
mkdir outputs/test_DW_HLLD

mkdir outputs/test_BWI_AR3
mkdir outputs/test_BWI_AR5
mkdir outputs/test_BWI_HLLD

mkdir outputs/test_BWII_AR3
mkdir outputs/test_BWII_AR5
mkdir outputs/test_BWII_HLLD

mkdir outputs/test_SR_AR3
mkdir outputs/test_SR_AR5
mkdir outputs/test_SR_HLLD

mkdir outputs/test_ETI_AR3
mkdir outputs/test_ETI_AR5
mkdir outputs/test_ETI_HLLD

mkdir outputs/test_ETII_AR3
mkdir outputs/test_ETII_AR5



#Exectute the simulations
cd ./../build/
echo " "
echo "=================================== 1D Problem runs ==================================="
echo "--Test: Dai wookdward Schock tube--"
echo "Riemann solver: All Regime 3 waves"
./main ../Test_cases_mhd_paper/settings/test_DW_AR3.ini>>'out.txt'
python3 ../Test_cases_mhd_paper/scripts/check_script/check_script.py  ../Test_cases_mhd_paper/outputs/test_DW_AR3/output_DW_AR3_time_000000002.h5
echo "Riemann solver: All Regime 5 waves"
./main ../Test_cases_mhd_paper/settings/test_DW_AR5.ini>>'out.txt'
python3 ../Test_cases_mhd_paper/scripts/check_script/check_script.py  ../Test_cases_mhd_paper/outputs/test_DW_AR5/output_DW_AR5_time_000000002.h5
echo "Riemann solver: HLLD"
./main ../Test_cases_mhd_paper/settings/test_DW_HLLD.ini>>'out.txt'
python3 ../Test_cases_mhd_paper/scripts/check_script/check_script.py  ../Test_cases_mhd_paper/outputs/test_DW_HLLD/output_DW_HLLD_time_000000002.h5

echo "--Test: Brio Wu I Schock tube--"
echo "Riemann solver: All Regime 3 waves"
./main ../Test_cases_mhd_paper/settings/test_BWI_AR3.ini>>'out.txt'
python3 ../Test_cases_mhd_paper/scripts/check_script/check_script.py  ../Test_cases_mhd_paper/outputs/test_BWI_AR3/output_BWI_AR3_time_000000002.h5
echo "Riemann solver: All Regime 5 waves"
./main ../Test_cases_mhd_paper/settings/test_BWI_AR5.ini>>'out.txt'
python3 ../Test_cases_mhd_paper/scripts/check_script/check_script.py  ../Test_cases_mhd_paper/outputs/test_BWI_AR5/output_BWI_AR5_time_000000002.h5
echo "Riemann solver: HLLD"
./main ../Test_cases_mhd_paper/settings/test_BWI_HLLD.ini>>'out.txt'
python3 ../Test_cases_mhd_paper/scripts/check_script/check_script.py  ../Test_cases_mhd_paper/outputs/test_BWI_HLLD/output_BWI_HLLD_time_000000002.h5

echo "--Test: Brio Wu II Schock tube--"
echo "Riemann solver: All Regime 3 waves"
./main ../Test_cases_mhd_paper/settings/test_BWII_AR3.ini>>'out.txt'
python3 ../Test_cases_mhd_paper/scripts/check_script/check_script.py  ../Test_cases_mhd_paper/outputs/test_BWII_AR3/output_BWII_AR3_time_000000002.h5
echo "Riemann solver: All Regime 5 waves"
./main ../Test_cases_mhd_paper/settings/test_BWII_AR5.ini>>'out.txt'
python3 ../Test_cases_mhd_paper/scripts/check_script/check_script.py  ../Test_cases_mhd_paper/outputs/test_BWII_AR5/output_BWII_AR5_time_000000002.h5
echo "Riemann solver: HLLD"
./main ../Test_cases_mhd_paper/settings/test_BWII_HLLD.ini>>'out.txt'
python3 ../Test_cases_mhd_paper/scripts/check_script/check_script.py  ../Test_cases_mhd_paper/outputs/test_BWII_HLLD/output_BWII_HLLD_time_000000002.h5

echo "--Test: Slow rarefaction --"
echo "Riemann solver: All Regime 3 waves"
./main ../Test_cases_mhd_paper/settings/test_SR_AR3.ini>>'out.txt'
python3 ../Test_cases_mhd_paper/scripts/check_script/check_script.py  ../Test_cases_mhd_paper/outputs/test_SR_AR3/output_SR_AR3_time_000000002.h5
echo "Riemann solver: All Regime 5 waves"
./main ../Test_cases_mhd_paper/settings/test_SR_AR5.ini>>'out.txt'
python3 ../Test_cases_mhd_paper/scripts/check_script/check_script.py  ../Test_cases_mhd_paper/outputs/test_SR_AR5/output_SR_AR5_time_000000002.h5
echo "Riemann solver: HLLD"
./main ../Test_cases_mhd_paper/settings/test_SR_HLLD.ini>>'out.txt'
python3 ../Test_cases_mhd_paper/scripts/check_script/check_script.py  ../Test_cases_mhd_paper/outputs/test_SR_HLLD/output_SR_HLLD_time_000000002.h5

echo "--Test: Expansion problem I --"
echo "Riemann solver: All Regime 3 waves"
./main ../Test_cases_mhd_paper/settings/test_ETI_AR3.ini>>'out.txt'
python3 ../Test_cases_mhd_paper/scripts/check_script/check_script.py  ../Test_cases_mhd_paper/outputs/test_ETI_AR3/output_ETI_AR3_time_000000002.h5
echo "Riemann solver: All Regime 5 waves"
./main ../Test_cases_mhd_paper/settings/test_ETI_AR5.ini>>'out.txt'
python3 ../Test_cases_mhd_paper/scripts/check_script/check_script.py  ../Test_cases_mhd_paper/outputs/test_ETI_AR5/output_ETI_AR5_time_000000002.h5
echo "Riemann solver: HLLD"
./main ../Test_cases_mhd_paper/settings/test_ETI_HLLD.ini>>'out.txt'
python3 ../Test_cases_mhd_paper/scripts/check_script/check_script.py  ../Test_cases_mhd_paper/outputs/test_ETI_HLLD/output_ETI_HLLD_time_000000002.h5

echo "=============================== Generating output plots: =============================="
echo " "
#Treat the ouputs and produce plots
cd ../Test_cases_mhd_paper/scripts/plot_scripts
python3 plot_DW.py
python3 plot_BWI.py
python3 plot_BWII.py
python3 plot_SR.py
python3 plot_ETI.py






#echo "Press enter when you are done"
cd ../../
#Delete the outputs
rm -r outputs
#rm *.pdf *.png

echo "========================================= END ========================================="
