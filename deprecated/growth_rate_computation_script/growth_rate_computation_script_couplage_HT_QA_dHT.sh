#!/bin/bash

#Create the output file
mkdir outputs

#Exectute the simulations

for i in 0 1 2 3 4 5
do
  cd ./../build/
  ./main ../growth_rate_computation_script/settings/couplage_HT_QA_dHT/test_convection$i.ini>>'out.txt'
  rm out.txt
  echo 'Simulation' $i ' done '
  cd ../growth_rate_computation_script/scripts/
  python3 plot_speed_evolution.py $i
  echo 'Plot and growth rate ' $i 'done'

  cd ../
  #Delete the outputs
  rm outputs/*.h5
done
python3 scripts/linear_regression_couplage_HT_QA_dHT.py

rm -r outputs
