import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression

np.loadtxt("growth_rates_list.txt")
tab = np.loadtxt('growth_rates_list.txt')

les_growthrates=tab[:]*tab[:]
les_g=np.array([5.0, 5.05, 5.1, 5.15, 5.20, 5.25]).reshape((-1, 1))

les_g=1.0/les_g
model = LinearRegression()

model.fit(les_g, les_growthrates)

r_sq = model.score(les_g, les_growthrates)
print('coefficient of determination:', r_sq)
print('intercept:', model.intercept_)
print('slope:', model.coef_)

g=np.linspace(les_g[0],les_g[les_g.size-1],100)

plt.xlabel('1/tau')
plt.ylabel('omega^2')
plt.title(r"growth rate in function of parameter, R = "+str("{:.4f}".format(r_sq)))

plt.scatter(les_g ,les_growthrates, label=' data ',color='red')
plt.plot(g ,model.intercept_ + model.coef_*g, label=' Linear regression',color='blue')


plt.legend()
plt.savefig('regression_grad_tau.png')
