import numpy as np
import matplotlib.pyplot as plt
import h5py
import os
import sys
import statistics


import os

import sys
#sys.stdout.write('\a')
#sys.stdout.flush()

time_list=[]
speed_list=[]
for root, dirs, files in os.walk("../outputs", topdown=False):
   for name in files:
      if name.endswith((".h5")):

          f = h5py.File("../outputs/"+name)

          mz = f["mz"][:]
          d  = f["d"][:]
          uz = mz/d

          if (f["time"][()]> 0.):
              time_list.append(f["time"][()])
              speed_list.append(np.sum(abs(uz)))

#sort the speed list with respect to time
zipped_lists = zip(time_list, speed_list)
sorted_zipped_lists = sorted(zipped_lists)
sorted_speed_list = [element for _, element in sorted_zipped_lists]
time_list.sort()

#compute the log of speeds
log_sorted_speed_list=np.log10(sorted_speed_list)
#amount of data
N=len(time_list)
time_index = range(N)

## Find the interval of growth rate computation

# #Find the maximal value and the corresponding index
# maxval = max(log_sorted_speed_list)
# maxvalue = max(sorted_speed_list)
# max_index = sorted_speed_list.index(maxvalue)
#
# # #Compute the indexes
# derr=[]
# index1=0
# for i in range(max_index):
#     derr.append(log_sorted_speed_list[i]-log_sorted_speed_list[i-1])
#
#     #If the speed is going down and we are far away from the max value, it's the original oscillations
#     if((derr[i] < 0)and(log_sorted_speed_list[i]<0.5*maxval)):
#         index1=i+1
#     #We start when the speed stop decreasing
#
# index2 = index1
#
# i = index1
#
# #We want to find the end, while it's increasing and hasnt reached the max, let's go
# while (derr[min(i,max_index-1)]>0)and(i<max_index):
#     index2=i
#     i=i+1
#
# #We get rid of the non linear damping
# i = index2
# while((derr[i-1]>derr[i]*1.001)):
#     i=i-1
#
#
# maxderr = statisitics.median(derr)
#
# while (derr[i] < 0.9*maxderr):
#     i=i-1
#
# index2=i
#index1= index1 + 3


plt.xlabel('Time')
plt.ylabel('total abs Vertical speed')
plt.title(r"Growth of the instability")

x_ticks = np.arange(0, len(time_index), 5)
plt.xticks(x_ticks)

plt.plot(time_index ,log_sorted_speed_list, label=' ',color='red')

plt.legend()
plt.grid()
plt.savefig('../growth'+str(sys.argv[1])+'.png')
# while True:
#     try:
#         index1= int(input("enter beginning"))
#     except ValueError:
#         print("Sorry, I didn't understand that.")
#         continue
#     else:
#         break
index1=15
plt.axvline(x=time_index[index1])
plt.savefig('../growth'+str(sys.argv[1])+'.png')

# while True:
#     try:
#         index2= int(input("enter end"))
#     except ValueError:
#         print("Sorry, I didn't understand that.")
#         continue
#     else:
#         break
index2=40
plt.axvline(x=time_index[index2])
plt.savefig('../growth'+str(sys.argv[1])+'.png')


print(sorted_speed_list[int(index2)], sorted_speed_list[int(index1)])

#Calcul du growth rate
growth_rate=(np.log(sorted_speed_list[int(index2)]) - np.log(sorted_speed_list[int(index1)]))/(time_list[int(index2)]-time_list[int(index1)])

file_object = open('../growth_rates_list.txt', 'a')
file_object.write(str(growth_rate)+'\n')
file_object.close()
