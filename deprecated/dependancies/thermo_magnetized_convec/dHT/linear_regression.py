import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from matplotlib import rc

rc('text', usetex=True)

np.loadtxt("growth_rates_list.txt")
tab = np.loadtxt('growth_rates_list.txt')

les_growthrates=tab[:]*tab[:]
les_g=np.array([0.2, 0.25, 0.3, 0.35, 0.4, 0.45]).reshape((-1, 1))

model = LinearRegression()

model.fit(les_g, les_growthrates)

r_sq = model.score(les_g, les_growthrates)
print('coefficient of determination:', r_sq)
print('intercept:', model.intercept_)
print('slope:', model.coef_)

g=np.linspace(les_g[0],les_g[les_g.size-1],100)

plt.xlabel(r'$-H_T$')
plt.ylabel(r'$\omega^2$')
plt.title('Growth rate in function of '+r'$H_T$ '+', Regression score = '+str("{:.4f}".format(r_sq)))

plt.scatter(les_g ,les_growthrates, label=r'-$H_T$ ',color='red')
plt.plot(g ,model.intercept_ + model.coef_*g, label=' Linear regression',color='blue')


plt.legend()
plt.savefig('regression_TMC_dHT.png')
