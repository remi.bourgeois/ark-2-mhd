import configparser


config= configparser.ConfigParser()

config.read('../experiment_magnetic_reconnection/ref_test_KH_with_resistivity.ini')

Res = [ i for i in range(10,511,20)]

for R in Res :
    cfg = open('../experiment_magnetic_reconnection/KH_resistivity_'+str(int(R))+'.ini', 'w')

    config.set('mesh', 'nx', str(int(R)))

    config.set('mesh', 'ny', str(int(2 * R)))

    config.set('output', 'prefix', '/tests_resistivity/output_resistivity_KH_'+str(int(R))+'_'+str(int(2 * R)))

    config.write(cfg)

    cfg.close()
