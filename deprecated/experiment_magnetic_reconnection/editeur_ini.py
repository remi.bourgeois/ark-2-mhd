import configparser
import numpy as np

config= configparser.ConfigParser()

config.read('../experiment_magnetic_reconnection/ref_test_KH.ini')

Res = np.array([ i for i in range(10,551,20)])

for R in Res :
    cfg = open('../experiment_magnetic_reconnection/test_kelvin_helmholtz_mhd_'+str(int(R))+'.ini', 'w')

    config.set('mesh', 'nx', str(int(R)))

    config.set('mesh', 'ny', str(int(2 * R)))

    config.set('output', 'prefix', 'output_KH_'+str(int(R))+'_'+str(int(2 * R)))

    config.write(cfg)

    cfg.close()
