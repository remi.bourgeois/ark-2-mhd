#pragma once

#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"

#include <cmath>

namespace hydro { namespace riemann
{

class AllRegime
{
public:
    using Euler           = MHDSystem;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState       = typename Euler::ConsState;
    using PrimState       = typename Euler::PrimState;

    AllRegime(const Params& params, const EquationOfState& eos);

    //AllRegime(const AllRegime& x) = default;

    //AllRegime(AllRegime&& x) = default;

    //~AllRegime() = default;

    //AllRegime& operator=(const AllRegime& x) = default;

    //AllRegime& operator=(AllRegime&& x) = default;

    template <int idir>
    KOKKOS_FORCEINLINE_FUNCTION ConsState
    operator()(const PrimState &q_L, const PrimState &q_R,
               std::integral_constant<int, idir> idir_tag, const int& side, bool st_powell) const;

  private:
    const EquationOfState m_eos;
    const Real m_K;
};

inline
AllRegime::AllRegime(const Params& params, const EquationOfState& eos)
    : m_eos ( eos )
    , m_K   ( params.hydro.K )
{
}

template<int idir>
KOKKOS_FORCEINLINE_FUNCTION
typename AllRegime::ConsState
AllRegime::operator()(const PrimState& q_L, const PrimState& q_R, std::integral_constant<int, idir> idir_tag, const int& side, bool st_powell) const
{
    using namespace constants;

    const Real un_L = q_L.v( idir_tag );
    const Real un_R = q_R.v( idir_tag );

    // Find the largest eigenvalues in the normal direction to the interface
    const Real c_L = Euler::computeSpeedOfSound(q_L, m_eos);
    const Real c_R = Euler::computeSpeedOfSound(q_R, m_eos);

    const Real a = m_K * std::fmax(q_R.d * c_R, q_L.d * c_L);

    const Real S_L = un_L - a/q_L.d;
    const Real S_R = un_R + a/q_R.d;

    const Real ustar = half * (un_R+un_L) - half * (q_R.p-q_L.p) / a;
    const Real Ma = std::fmax(std::fabs(un_L)/c_L, std::fabs(un_R)/c_R);
    const Real theta = std::fmin(Ma, one);
    const Real pistar = half * (q_R.p + q_L.p) - half * theta * a * (un_R - un_L);

    Real sign;
    PrimState q;
    if ( ustar > 0 )
    {
        sign = - one;
        q = q_L;
    }
    else
    {
        sign = + one;
        q = q_R;
    }
    const Real un = q.v( idir_tag );
    const Real eint = Euler::computeInternalEnergy(q, m_eos);
    const Real ekin = Euler::computeKineticEnergy(q);
    const Real etot = (eint + ekin) / q.d;

    Real un_o;
    Real ptot_o;
    if ( S_L*S_R > zero )
    {
        un_o = un;
        ptot_o = q.p;
    }
    else
    {
        un_o = ustar;
        ptot_o = pistar;
    }
    const Real d_o = q.d / (one - sign*q.d*(un_o-un)/a);
    const Real etot_o = etot - sign*(q.p*un-ptot_o*un_o)/a;

    ConsState flux;
    flux.d = d_o * un_o;
    flux.e = (d_o * etot_o + ptot_o) * un_o;
    flux.m = (d_o * un_o) * q.v;
    flux.m( idir_tag ) = d_o * un_o * un_o + ptot_o;
    flux.B = 0.0*q.B;

    return flux;
}

}}
