#include "MusclReconstructionKernel_consvar.hpp"

#include "HydroBaseKernel.hpp"
#include "HydroUniformGrid.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"

namespace hydro
{

MusclReconstructionKernel_consvar::MusclReconstructionKernel_consvar(const Params& params,
                                                          const Grid& grid,
                                                          Array q,
                                                          const Kokkos::Array<Array, 2*three_d>& qr,
                                                          Real dt)
    : m_params {params}
    , m_qr     {qr}
    , m_q      {q}
    , m_grid   {grid}
    , m_dl     {grid.m_dl}
    , m_dtdl   {}
    , m_slope_type{params.run.slope_type}
{
    for (int idim=0; idim<three_d; ++idim)
    {
        m_dtdl[idim] = dt / m_dl[idim];
    }
}

void MusclReconstructionKernel_consvar::apply(const Params& params, const Grid& grid,
                                           Array q, const Kokkos::Array<Array, 2*three_d>& qr, Real dt)
{
    MusclReconstructionKernel_consvar kernel(params, grid, q, qr, dt);

    const int league_size = Super::computeLeagueSize(grid.m_nbCells, ghostDepth);
    const int vector_length = TeamPolicy::vector_length_max();
    TeamPolicy policy {league_size, Kokkos::AUTO, vector_length};
    Kokkos::parallel_for("Muscl reconstruction kernel - TeamPolicy", policy, kernel);

}

KOKKOS_INLINE_FUNCTION
void MusclReconstructionKernel_consvar::operator()(const Team& team) const
{
    using namespace constants;

    // From given team compute coordinates of cell (0, jy, jz).
    const IntVector coord0 {Super::teamToCoord(team, m_grid, ghostDepth)};
    // Get linear index from cell (0, jy, jz).
    const int j0 {m_grid.coordToIndex(coord0)};

    const auto vec_loop = Super::ArkTeamVectorRange(team, m_grid.m_ghostWidths[IX]-ghostDepth, m_grid.m_nbCells[IX] + m_grid.m_ghostWidths[IX]+ghostDepth);
    Kokkos::parallel_for(Kokkos::ThreadVectorRange(team, vec_loop.start, vec_loop.end),
                         KOKKOS_LAMBDA(const int jx)
                         {
                             const int j{j0 + jx};
                             const ConsState q_j {Super::getCons(m_q, j)};

                             Kokkos::Array<ConsState, three_d> dq_j {};

                             ConsState qMinus {Super::getCons(m_q, j - m_grid.m_strides[IX])};
                             ConsState qPlus  {Super::getCons(m_q, j + m_grid.m_strides[IX])};
                             // get hydro slopes dq
                             dq_j[IX] = slope_unsplit_hydro_1d(q_j, qPlus, qMinus, m_slope_type);

                                       qMinus= {Super::getCons(m_q, j - m_grid.m_strides[IY])};
                                       qPlus = {Super::getCons(m_q, j + m_grid.m_strides[IY])};
                             // get hydro slopes dq
                             dq_j[IY] = slope_unsplit_hydro_1d(q_j, qPlus, qMinus, m_slope_type);

                                       qMinus ={Super::getCons(m_q, j - m_grid.m_strides[IZ])};
                                       qPlus  ={Super::getCons(m_q, j + m_grid.m_strides[IZ])};
                             // get hydro slopes dq
                             dq_j[IZ] = slope_unsplit_hydro_1d(q_j, qPlus, qMinus, m_slope_type);

                             //get un+1/2 with matrix
                             const ConsState sq0_j {trace_unsplit_hydro_along_dir(q_j, dq_j)};

                             // get edge values
                              ConsState qm_j;
                              ConsState qp_j;

                              qm_j.d = q_j.d - 0.5*dq_j[IX].d + 0.5*m_dtdl[IX]*sq0_j.d;
                              qp_j.d = q_j.d + 0.5*dq_j[IX].d + 0.5*m_dtdl[IX]*sq0_j.d;
                              qm_j.m = q_j.m - 0.5*dq_j[IX].m + 0.5*m_dtdl[IX]*sq0_j.m;
                              qp_j.m = q_j.m + 0.5*dq_j[IX].m + 0.5*m_dtdl[IX]*sq0_j.m;
                              qm_j.e = q_j.e - 0.5*dq_j[IX].e + 0.5*m_dtdl[IX]*sq0_j.e;
                              qp_j.e = q_j.e + 0.5*dq_j[IX].e + 0.5*m_dtdl[IX]*sq0_j.e;
                              qm_j.B = q_j.B - 0.5*dq_j[IX].B + 0.5*m_dtdl[IX]*sq0_j.B;
                              qp_j.B = q_j.B + 0.5*dq_j[IX].B + 0.5*m_dtdl[IX]*sq0_j.B;

                              Super::set(m_qr[0 + 2*IX], j, qm_j);
                              Super::set(m_qr[1 + 2*IX], j, qp_j);

                              qm_j.d = q_j.d - 0.5*dq_j[IY].d + 0.5*m_dtdl[IY]*sq0_j.d;
                              qp_j.d = q_j.d + 0.5*dq_j[IY].d + 0.5*m_dtdl[IY]*sq0_j.d;
                              qm_j.m = q_j.m - 0.5*dq_j[IY].m + 0.5*m_dtdl[IY]*sq0_j.m;
                              qp_j.m = q_j.m + 0.5*dq_j[IY].m + 0.5*m_dtdl[IY]*sq0_j.m;
                              qm_j.e = q_j.e - 0.5*dq_j[IY].e + 0.5*m_dtdl[IY]*sq0_j.e;
                              qp_j.e = q_j.e + 0.5*dq_j[IY].e + 0.5*m_dtdl[IY]*sq0_j.e;
                              qm_j.B = q_j.B - 0.5*dq_j[IY].B + 0.5*m_dtdl[IY]*sq0_j.B;
                              qp_j.B = q_j.B + 0.5*dq_j[IY].B + 0.5*m_dtdl[IY]*sq0_j.B;

                              Super::set(m_qr[0 + 2*IY], j, qm_j);
                              Super::set(m_qr[1 + 2*IY], j, qp_j);

                              qm_j.d = q_j.d - 0.5*dq_j[IZ].d + 0.5*m_dtdl[IZ]*sq0_j.d;
                              qp_j.d = q_j.d + 0.5*dq_j[IZ].d + 0.5*m_dtdl[IZ]*sq0_j.d;
                              qm_j.m = q_j.m - 0.5*dq_j[IZ].m + 0.5*m_dtdl[IZ]*sq0_j.m;
                              qp_j.m = q_j.m + 0.5*dq_j[IZ].m + 0.5*m_dtdl[IZ]*sq0_j.m;
                              qm_j.e = q_j.e - 0.5*dq_j[IZ].e + 0.5*m_dtdl[IZ]*sq0_j.e;
                              qp_j.e = q_j.e + 0.5*dq_j[IZ].e + 0.5*m_dtdl[IZ]*sq0_j.e;
                              qm_j.B = q_j.B - 0.5*dq_j[IZ].B + 0.5*m_dtdl[IZ]*sq0_j.B;
                              qp_j.B = q_j.B + 0.5*dq_j[IZ].B + 0.5*m_dtdl[IZ]*sq0_j.B;

                              Super::set(m_qr[0 + 2*IZ], j, qm_j);
                              Super::set(m_qr[1 + 2*IZ], j, qp_j);


                         });
}


KOKKOS_INLINE_FUNCTION
typename MusclReconstructionKernel_consvar::ConsState
MusclReconstructionKernel_consvar::trace_unsplit_hydro_along_dir(const ConsState& q,
                                                              const Kokkos::Array<ConsState, three_d>& dq) const
{
    using namespace constants;
    const Real gamma = m_params.thermo.gamma;
    ConsState sq0;

    // Real div_u = (dq[IX].m(IX) + dq[IY].m(IY) + dq[IZ].m(IZ));
    //
    // Vector<three_d, Real> grad_d =  {dq[IX].d    , dq[IY].d    , dq[IZ].d};
    // Vector<three_d, Real> grad_p =  {dq[IX].e    , dq[IY].e    , dq[IZ].e};
    // Vector<three_d, Real> grad_vx = {dq[IX].m(IX), dq[IY].m(IX), dq[IZ].m(IX)};
    // Vector<three_d, Real> grad_vy = {dq[IX].m(IY), dq[IY].m(IY), dq[IZ].m(IY)};
    // Vector<three_d, Real> grad_vz = {dq[IX].m(IZ), dq[IY].m(IZ), dq[IZ].m(IZ)};
    // Vector<three_d, Real> grad_Bx = {dq[IX].B(IX), dq[IY].B(IX), dq[IZ].B(IX)};
    // Vector<three_d, Real> grad_By = {dq[IX].B(IY), dq[IY].B(IY), dq[IZ].B(IY)};
    // Vector<three_d, Real> grad_Bz = {dq[IX].B(IZ), dq[IY].B(IZ), dq[IZ].B(IZ)};

    Real r = (1.0/q.d);

    sq0.d=0.0; sq0.e=0.0; sq0.B(IX) =0.0;sq0.B(IY) =0.0;sq0.B(IZ) =0.0; sq0.m(IX) =0.0;sq0.m(IY) =0.0;sq0.m(IZ) =0.0;

    //  sq0.d -= q.d*div_u + dot(q.m,grad_d);
    //  sq0.e -= gamma*q.e*div_u + dot(q.m,grad_p);
    // //
    //  sq0.m(IX) -= dot(q.m, grad_vx) + r*(dq[IX].e + dot(dq[IX].B,q.B) - dot(q.B,grad_Bx) );
    //  sq0.m(IY) -= dot(q.m, grad_vy) + r*(dq[IY].e + dot(dq[IY].B,q.B) - dot(q.B,grad_By) );
    //  sq0.m(IZ) -= dot(q.m, grad_vz) + r*(dq[IZ].e + dot(dq[IZ].B,q.B) - dot(q.B,grad_Bz) );
    // //
    //  sq0.B(IX) -= q.B(IX)*div_u - dot(q.B, grad_vx)-dot(q.m,grad_Bx);
    //  sq0.B(IY) -= q.B(IY)*div_u - dot(q.B, grad_vy)-dot(q.m,grad_By);
    //  sq0.B(IZ) -= q.B(IZ)*div_u - dot(q.B, grad_vz)-dot(q.m,grad_Bz);

    return sq0;
}

KOKKOS_INLINE_FUNCTION
typename MusclReconstructionKernel_consvar::ConsState
MusclReconstructionKernel_consvar::slope_unsplit_hydro_1d(const ConsState& q,
                                                       const ConsState& qPlus,
                                                       const ConsState& qMinus,
                                                       Real slope_type)
{
    using namespace constants;

    ConsState dq;

    Real dlft = slope_type*(q.d     - qMinus.d);
    Real drgt = slope_type*(qPlus.d - q.d     );
    Real dcen = 0.5 * (qPlus.d - qMinus.d);
    Real dsgn = (dcen > zero) ? one : -one;
    Real slop = std::fmin(std::fabs(dlft), std::fabs(drgt));
    Real dlim = (dlft*drgt) < zero ? zero : slop;

    dq.d = dsgn * std::fmin(dlim, std::fabs(dcen));

    dlft = slope_type*(q.e     - qMinus.e);
    drgt = slope_type*(qPlus.e - q.e     );
    dcen = 0.5 * (qPlus.e - qMinus.e);
    dsgn = (dcen > zero) ? one : -one;
    slop = std::fmin(std::fabs(dlft), std::fabs(drgt));
    dlim = (dlft*drgt) < zero ? zero : slop;

    dq.e = dsgn * std::fmin(dlim, std::fabs(dcen));

    dlft = slope_type*(q.m(IX)     - qMinus.m(IX));
    drgt = slope_type*(qPlus.m (IX)- q.m(IX)     );
    dcen = 0.5 * (qPlus.m(IX) - qMinus.m(IX));
    dsgn = (dcen > zero) ? one : -one;
    slop = std::fmin(std::fabs(dlft), std::fabs(drgt));
    dlim = (dlft*drgt) < zero ? zero : slop;

    dq.m(IX) = dsgn * std::fmin(dlim, std::fabs(dcen));

    dlft = slope_type*(q.B(IX)     - qMinus.B(IX));
    drgt = slope_type*(qPlus.B (IX)- q.B(IX)     );
    dcen = 0.5 * (qPlus.B(IX) - qMinus.B(IX));
    dsgn = (dcen > zero) ? one : -one;
    slop = std::fmin(std::fabs(dlft), std::fabs(drgt));
    dlim = (dlft*drgt) < zero ? zero : slop;

    dq.B(IX) = dsgn * std::fmin(dlim, std::fabs(dcen));

    dlft = slope_type*(q.m(IY)     - qMinus.m(IY));
    drgt = slope_type*(qPlus.m (IY)- q.m(IY)     );
    dcen = 0.5 * (qPlus.m(IY) - qMinus.m(IY));
    dsgn = (dcen > zero) ? one : -one;
    slop = std::fmin(std::fabs(dlft), std::fabs(drgt));
    dlim = (dlft*drgt) < zero ? zero : slop;

    dq.m(IY) = dsgn * std::fmin(dlim, std::fabs(dcen));

    dlft = slope_type*(q.B(IY)     - qMinus.B(IY));
    drgt = slope_type*(qPlus.B (IY)- q.B(IY)     );
    dcen = 0.5 * (qPlus.B(IY) - qMinus.B(IY));
    dsgn = (dcen > zero) ? one : -one;
    slop = std::fmin(std::fabs(dlft), std::fabs(drgt));
    dlim = (dlft*drgt) < zero ? zero : slop;

    dq.B(IY) = dsgn * std::fmin(dlim, std::fabs(dcen));

    dlft = slope_type*(q.m(IZ)     - qMinus.m(IZ));
    drgt = slope_type*(qPlus.m (IZ)- q.m(IZ)     );
    dcen = 0.5 * (qPlus.m(IZ) - qMinus.m(IZ));
    dsgn = (dcen > zero) ? one : -one;
    slop = std::fmin(std::fabs(dlft), std::fabs(drgt));
    dlim = (dlft*drgt) < zero ? zero : slop;

    dq.m(IZ) = dsgn * std::fmin(dlim, std::fabs(dcen));

    dlft = slope_type*(q.B(IZ)     - qMinus.B(IZ));
    drgt = slope_type*(qPlus.B (IZ)- q.B(IZ)     );
    dcen = 0.5 * (qPlus.B(IZ) - qMinus.B(IZ));
    dsgn = (dcen > zero) ? one : -one;
    slop = std::fmin(std::fabs(dlft), std::fabs(drgt));
    dlim = (dlft*drgt) < zero ? zero : slop;
    dq.B(IZ) = dsgn * std::fmin(dlim, std::fabs(dcen));

    return dq;
}

}  // hydro
