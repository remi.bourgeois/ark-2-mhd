#include "MagneticResistivityStepExecution.hpp"

#include "MagneticResistivityStepKernel.hpp"

#include "HydroBaseKernel.hpp"
#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"
#include "linalg/Tensor.hpp"
#include "linalg/Vector.hpp"

#include <Kokkos_Core.hpp>
#include <cmath>

namespace hydro
{

void
ExecuteMagneticResistivityStep( const Params& params, const UniformGrid& grid, const Array3d& q,
                    const Array3d& u, Real dt )
{
    using Kernel = MagneticResistivityStepKernel;
    using TeamPolicy = typename Kernel::TeamPolicy;
    Kernel kernel( params, grid, q, u, dt );
    const int league_size = Kernel::computeLeagueSize( grid.m_nbCells, Kernel::ghostDepth );
    const int vector_length = TeamPolicy::vector_length_max();
    TeamPolicy policy( league_size, Kokkos::AUTO, vector_length );
    Kokkos::parallel_for( "Resistivity kernel", policy, kernel );
}

ETI_ExecuteMagneticResistivityStep( );

} // namespace hydro
