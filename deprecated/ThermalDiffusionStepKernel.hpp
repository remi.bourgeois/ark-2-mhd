#pragma once

#include "HydroBaseKernel.hpp"
#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"
#include "linalg/Vector.hpp"

#include <Kokkos_Core.hpp>
#include <cmath>

namespace hydro
{

class ThermalDiffusionStepKernel : public BaseKernel
{
public:
    using Super = BaseKernel;

    using Euler = MHDSystem;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;
    using VC = typename Euler::VarCons;

    using IntVector = IntVectorNd< three_d >;
    using RealVector = RealVector3d;
    using TeamPolicy = Kokkos::TeamPolicy< Kokkos::IndexType< Int > >;
    using Team = typename TeamPolicy::member_type;

    static constexpr int ghostDepth = 0;

    ThermalDiffusionStepKernel( const Params& params, const UniformGrid& grid,
                                const Array3d& q, const Array3d& u, Real dt )
        : m_grid( grid )
        , m_q( q )
        , m_u( u )
        , m_eos( params.thermo )
        , m_kappa( params.hydro.kappa )
        , m_invdl {}
        , m_dt( dt )
    {
        for ( int idim = 0; idim < three_d ; ++idim )
        {
            m_invdl[ idim ] = constants::one / grid.m_dl[ idim ];
        }
    }

    template < int idim, int iside, std::enable_if_t< ( idim >= three_d ), int > = idim >
    KOKKOS_INLINE_FUNCTION void computeThermalFlux( std::integral_constant< int, idim >,
                                                    std::integral_constant< int, iside >, Int,
                                                    Real& ) const noexcept
    {
    }

    template < int idim, int iside, std::enable_if_t< ( idim < three_d ), int > = idim >
    KOKKOS_INLINE_FUNCTION void computeThermalFlux( std::integral_constant< int, idim > idir_tag,
                                                    std::integral_constant< int, iside > iside_tag,
                                                    Int j, Real& flux ) const noexcept
    {
        using namespace constants;

        const Int j_L = iside == 0 ? j - m_grid.m_strides[ idim ] : j;
        const Int j_R = iside == 0 ? j : j + m_grid.m_strides[ idim ];

        const PrimState q_L = Super::getPrim( m_q, j_L );
        const PrimState q_R = Super::getPrim( m_q, j_R );
        const Real T_L = Euler::computeTemperature( q_L, m_eos );
        const Real T_R = Euler::computeTemperature( q_R, m_eos );
        const Real gradT = ( T_R - T_L ) * m_invdl[ idim ];

        const Real side = iside == 0 ? -one : +one;
        const Real dtdSdV = m_dt * m_grid.getdSdV( j, idir_tag, iside_tag );
        flux += -side * dtdSdV * m_kappa * gradT;
    }

    KOKKOS_INLINE_FUNCTION
    void operator()( const Team& team ) const
    {
        using namespace constants;

        // From given team compute coordinates of cell (0, jy, jz).
        const IntVector coord0 = Super::teamToCoord( team, m_grid, ghostDepth );
        // Get linear index from cell (0, jy, jz).
        const int j0 = m_grid.coordToIndex( coord0 );

        Int start = m_grid.m_ghostWidths[ IX ] - ghostDepth;
        Int end = m_grid.m_ghostWidths[ IX ] + ( m_grid.m_nbCells[ IX ] + ghostDepth );
        Kokkos::parallel_for( Super::ArkTeamVectorRange( team, start, end ), [ & ]( const Int jx ) {
            const Int j = j0 + jx;

            Real flux_j = zero;

            computeThermalFlux( Tags::DirX, Tags::SideL, j, flux_j );
            computeThermalFlux( Tags::DirX, Tags::SideR, j, flux_j );
            computeThermalFlux( Tags::DirY, Tags::SideL, j, flux_j );
            computeThermalFlux( Tags::DirY, Tags::SideR, j, flux_j );
            computeThermalFlux( Tags::DirZ, Tags::SideL, j, flux_j );
            computeThermalFlux( Tags::DirZ, Tags::SideR, j, flux_j );

            m_u( j, VC::IE ) -= flux_j;
        } );
    }

private:
    UniformGrid m_grid;
    const ConstArray3d m_q;
    Array3d m_u;
    const EquationOfState m_eos;
    Real m_kappa;
    RealVector m_invdl;
    Real m_dt;
};

} // namespace hydro
