#!/bin/bash
#Test of the code with several test cases
#Compile the code with HDF5_C on and execute this script from this folder
#You need wget, python3, hdf5 for python, numpy and matplotlib

#import the reference solutions

read -p "Do you want to download the reference solutions (necessary in order to generate comparison plots) ?" yn
case $yn in
    [Nn]* ) echo"";;
    [Yy]* ) mkdir reference_solutions; cd reference_solutions;
            wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1xW5v4-30TzrNauEjRdqCqJWETNOc797P' -O reference_sod.h5;
            wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1rLTIHeEoQANH972d1Jn0s8ESpNVVxGvE' -O reference_RT_hllc.h5;
            wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1bonurApJ9shlBZG6dFf3Mw_EcycQnaaz' -O reference_RT_allregime.h5;
            wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1ty2NtXr_0cOy7TxgrkuHTC6Fx8Ojj4-R' -O reference_raref.h5;
            wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1rCBVApw902OkHhHJEjq7cAhcjCZP_B36' -O reference_dshock.h5;
            wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1CHXd3ebVTTpQkRob_4gcTR6GBIvBplmo' -O reference_2DRP.h5;
            wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1RBL_Cjgtq3yyTIZ0zLEzC5C_0YdhpCFY' -O reference_BW.h5;
            wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1pwcR9LYA2gRhCZCs2YZmJOjWVJ312er-' -O reference_Otang.h5;
            wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1JfL7EKMFhoaDDLy0Um8EGeJJPIcVoYUq' -O reference_SS.h5;
            cd .. ;;
    * ) echo "Please answer yes or no.";;
esac


#Create the output file
mkdir outputs
mkdir outputs/test_sod_allregime
mkdir outputs/test_sod_hllc
mkdir outputs/test_raref_allregime
mkdir outputs/test_raref_hllc
mkdir outputs/test_dshock_allregime
mkdir outputs/test_dshock_hllc
mkdir outputs/test_RT_allregime
mkdir outputs/test_RT_hllc
mkdir outputs/test_2DRP_allregime
mkdir outputs/test_2DRP_hllc
mkdir outputs/test_gresho_allregime_32
mkdir outputs/test_gresho_allregime_64
mkdir outputs/test_ATMO_allregime
mkdir outputs/test_BW_rusanov
mkdir outputs/test_BW_HLLD
mkdir outputs/test_BW_AR3
mkdir outputs/test_BW_B3
mkdir outputs/test_BW_B5
mkdir outputs/test_OTang_Rusanov
mkdir outputs/test_OTang_AR3
mkdir outputs/test_SS_rusanov
mkdir outputs/test_SS_HLLD
mkdir outputs/test_SS_AR3
mkdir outputs/test_SS_B3
mkdir outputs/test_SS_B5


#Exectute the simulations
cd ./../build/
echo " "
echo "=================================== 1D Problem runs ==================================="
echo "--Test: Sod Shock tube--"
echo "Riemann solver: All Regime"
./main ../validation_via_test_cases/settings/test_sod_allregime.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_sod_allregime/output_sod_allregime_time_000000002.h5
echo " "
echo "Riemann solver: HLLC"
./main ../validation_via_test_cases/settings/test_sod_hllc.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_sod_hllc/output_sod_hllc_time_000000002.h5
echo " "
echo "--Test: double rarefaction--"
echo "Riemann solver: All Regime"
./main ../validation_via_test_cases/settings/test_raref_allregime.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_raref_allregime/output_raref_allregime_time_000000002.h5
echo "Riemann solver: HLLC"
./main ../validation_via_test_cases/settings/test_raref_hllc.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_raref_hllc/output_raref_hllc_time_000000002.h5
echo " "
echo "--Test: double shock--"
echo "Riemann solver: All Regime"
./main ../validation_via_test_cases/settings/test_dshock_allregime.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_dshock_allregime/output_dshock_allregime_time_000000002.h5
echo "Riemann solver: HLLC"
./main ../validation_via_test_cases/settings/test_dshock_hllc.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_dshock_hllc/output_dshock_hllc_time_000000002.h5
echo " "
echo "--Test: Atmo at rest--"
echo "Riemann solver: All Regime"
./main ../validation_via_test_cases/settings/test_ATMO_allregime.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_ATMO_allregime/output_ATMO_allregime_time_000000002.h5
echo " "
echo "--Test: Brio Wu MHD Schock tube--"
echo "Riemann solver: rusanov"
./main ../validation_via_test_cases/settings/test_BW_rusanov.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_BW_rusanov/output_BW_rusanov_time_000000002.h5
echo "Riemann solver: All Regime 3 waves"
./main ../validation_via_test_cases/settings/test_BW_AR3.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_BW_AR3/output_BW_AR3_time_000000002.h5
echo "Riemann solver: HLLD"
./main ../validation_via_test_cases/settings/test_BW_HLLD.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_BW_HLLD/output_BW_HLLD_time_000000002.h5
echo "Riemann solver: Bouchut 3 waves"
./main ../validation_via_test_cases/settings/test_BW_B3.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_BW_B3/output_BW_B3_time_000000002.h5
echo "Riemann solver: Bouchut 5 waves"
./main ../validation_via_test_cases/settings/test_BW_B5.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_BW_B5/output_BW_B5_time_000000002.h5

echo "================================== 2D Problems runs ==================================="
echo "--Test: Rayleigh Taylor--"
echo "Riemann solver: All Regime"
./main ../validation_via_test_cases/settings/test_RT_allregime.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_RT_allregime/output_RT_allregime_time_000000002.h5
echo "Riemann solver: HLLC"
./main ../validation_via_test_cases/settings/test_RT_hllc.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_RT_hllc/output_RT_hllc_time_000000002.h5
echo " "
echo "--Test: 2D Riemann problem--"
echo "Riemann solver: All Regime"
./main ../validation_via_test_cases/settings/test_2DRP_allregime.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_2DRP_allregime/output_2DRP_allregime_time_000000002.h5
echo "Riemann solver: HLLC"
./main ../validation_via_test_cases/settings/test_2DRP_hllc.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_2DRP_hllc/output_2DRP_hllc_time_000000002.h5
echo " "
echo "--Test: Gresho vortex--"
echo "Riemann solver: All Regime, n=32"
./main ../validation_via_test_cases/settings/test_gresho_allregime_32.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_gresho_allregime_32/output_gresho_allregime_32_time_000000002.h5
echo " "
echo "Riemann solver: All Regime, n=64"
./main ../validation_via_test_cases/settings/test_gresho_allregime_64.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_gresho_allregime_64/output_gresho_allregime_64_time_000000002.h5
echo " "
echo "--Test: OTang vortex--"
echo "Riemann solver: Rusanov"
./main ../validation_via_test_cases/settings/test_OTang_Rusanov.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_OTang_Rusanov/output_Otang_Rusanov_time_000000002.h5
echo " "
echo "Riemann solver: All Regime 3 waves"
./main ../validation_via_test_cases/settings/test_OTang_AR3.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_OTang_AR3/output_OTang_AR3_time_000000002.h5
echo " "
echo "--Test: Skewed shock test Schock tube--"
echo "Riemann solver: rusanov"
./main ../validation_via_test_cases/settings/test_SS_rusanov.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_SS_rusanov/output_SS_rusanov_time_000000002.h5
echo "Riemann solver: All Regime 3 waves"
./main ../validation_via_test_cases/settings/test_SS_AR3.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_SS_AR3/output_SS_AR3_time_000000002.h5
echo "Riemann solver: HLLD"
./main ../validation_via_test_cases/settings/test_SS_HLLD.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_SS_HLLD/output_SS_HLLD_time_000000002.h5
echo "Riemann solver: Bouchut 3 waves"
./main ../validation_via_test_cases/settings/test_SS_B3.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_SS_B3/output_SS_B3_time_000000002.h5
echo "Riemann solver: Bouchut 5 waves"
./main ../validation_via_test_cases/settings/test_SS_B5.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_SS_B5/output_SS_B5_time_000000002.h5
rm out.txt
echo "=============================== Generating output plots: =============================="
echo " "
#Treat the ouputs and produce plots
cd ../validation_via_test_cases/scripts/plot_scripts
python3 plot_sod.py
echo "Sod                plot generated"
python3 plot_raref.py
echo "Double rarefaction plot generated"
python3 plot_dshock.py
echo "Double shock       plot generated"
python3 plot_RT.py
echo "Rayleigh Taylor    plot generated"
python3 plot_2DRP.py
echo "2D Riemann problem plot generated"
python3 plot_BW_rusanov.py
python3 plot_BW_AR3.py
python3 plot_BW_HLLD.py
python3 plot_BW_B3.py
python3 plot_BW_B5.py
echo "BW MHD     problem plot generated"
python3 plot_OTang.py
echo "OTang vortex       plot generated"
python3 plot_SS_rusanov.py
python3 plot_SS_AR3.py
python3 plot_SS_HLLD.py
python3 plot_SS_B3.py
python3 plot_SS_B5.py
echo "Skewed shock       plot generated"


cd ../../
echo " "
echo "=========================== Test for   all regime  behavior ==========================="
echo " "
python3 ../validation_via_test_cases/scripts/error_script/error_script.py  ../validation_via_test_cases/outputs/test_gresho_allregime_32/output_gresho_allregime_32_time_000000000.h5 ../validation_via_test_cases/outputs/test_gresho_allregime_32/output_gresho_allregime_32_time_000000002.h5
python3 ../validation_via_test_cases/scripts/error_script/error_script.py  ../validation_via_test_cases/outputs/test_gresho_allregime_64/output_gresho_allregime_64_time_000000000.h5 ../validation_via_test_cases/outputs/test_gresho_allregime_64/output_gresho_allregime_64_time_000000002.h5
echo " "
echo "The two errors should be of the same order of magnitude (E-3)"
echo " "
echo "=========================== Test for well balanced behavior ==========================="
echo " "
python3 ../validation_via_test_cases/scripts/WB_script/WB_script.py  ../validation_via_test_cases/outputs/test_ATMO_allregime/output_ATMO_allregime_time_000000002.h5
echo " "


#echo "Press enter when you are done"

#Delete the outputs
rm -r outputs
#rm *.pdf *.png

echo "========================================= END ========================================="
