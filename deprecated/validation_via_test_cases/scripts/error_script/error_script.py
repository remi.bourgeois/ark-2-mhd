import numpy as np
import matplotlib.pyplot as plt
import h5py
import math
import sys

mystringBeg = sys.argv[1]
mystringEnd = sys.argv[2]

f_Beg = h5py.File(mystringBeg)
f_End = h5py.File(mystringEnd)

f_Beg_mx = f_Beg['mx'][:]
f_Beg_my = f_Beg['my'][:]

f_End_mx = f_End['mx'][:]
f_End_my = f_End['my'][:]

(nx, ny, nz)=f_Beg_mx.shape

error = 0.0

for ix in range(nx):
    for iy in range(ny):
        for iz in range ( nz):
            error = error + abs(f_Beg_mx[ix][iy][iz] - f_End_mx[ix][iy][iz])
            error = error + abs(f_Beg_my[ix][iy][iz] - f_End_my[ix][iy][iz])

error = error/(nx*ny*nz)

print("Gresho error with resolution= ",str(ny),'^2 =',error)
