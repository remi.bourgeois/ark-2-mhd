import numpy as np
import matplotlib.pyplot as plt
import h5py


f_ar = h5py.File('../../outputs/test_RT_allregime/output_RT_allregime_time_000000002.h5')
f_hllc = h5py.File('../../outputs/test_RT_hllc/output_RT_hllc_time_000000002.h5')
f_hllc_ref = h5py.File('../../reference_solutions/reference_RT_hllc.h5')
f_ar_ref =   h5py.File('../../reference_solutions/reference_RT_allregime.h5')


ar   = f_ar['d'][:]
hllc   = f_hllc['d'][:]
ar_ref   = f_ar_ref['d'][:]
hllc_ref   = f_hllc_ref['d'][:]


ar1 = np.squeeze(ar)
hllc1 = np.squeeze(hllc)


ar_ref1 = np.squeeze(ar_ref)
hllc_ref1 = np.squeeze(hllc_ref)

fig, (axAR, axAR_ref, axHLLC, axHLLC_ref) = plt.subplots(1, 4)

axAR.imshow(ar1, interpolation='none')
axAR.set_title('AR')
axHLLC.set_title('HLLC')

axAR_ref.set_title('AR reference')
axHLLC_ref.set_title('HLLC reference')

axHLLC.imshow(hllc1, interpolation='none')
axAR_ref.imshow(ar_ref1, interpolation='none')
axHLLC_ref.imshow(hllc_ref1, interpolation='none')

plt.savefig('../../comparison_RT.png')
plt.savefig('../../comparison_RT.pdf')
