import numpy as np
import matplotlib.pyplot as plt
import h5py

n_simu=128
n_ref =1025

f_ref = h5py.File('../../reference_solutions/reference_SS.h5')
f_simu = h5py.File('../../outputs/test_SS_B5/output_SS_B5_time_000000002.h5')

out_d=[]
out_vx=[]
out_vy=[]
out_Bx=[]
out_By=[]
out_Bz=[]

for i in range(n_simu):
    out_d.append(f_simu['d'][0,i,i])
    out_vx.append(f_simu['mx'][0,i,i]/f_simu['d'][0,i,i])
    out_vy.append(f_simu['my'][0,i,i]/f_simu['d'][0,i,i])
    out_Bx.append(f_simu['Bx'][0,i,i])
    out_By.append(f_simu['By'][0,i,i])
    out_Bz.append(f_simu['Bz'][0,i,i])


x=np.linspace(1,n_simu,n_simu)
x_ref=np.linspace(1,n_simu,n_ref-1)

plt.xlabel('x')
plt.ylabel('density')
plt.title(r" Comparison B5 / reference solution." "\n" r"Check the wave speeds and look for unphysical oscillations")


plt.plot(x    , out_d,label= 'd,  B5, n='+str(n_simu),color='blue')
plt.plot(x    ,out_vx,label='vx, B5, n='+str(n_simu),color='green')
#plt.plot(x    ,out_vy,label='vy, B5, n='+str(n_simu),color='cyan')
plt.plot(x    ,out_Bx,label='Bx, B5, n='+str(n_simu),color='red')
plt.plot(x    ,out_By,label='By, B5, n='+str(n_simu),color='purple')



plt.plot(x_ref,f_ref['d'][:] ,label='reference solution',color='black')
plt.plot(x_ref ,f_ref['vx'][:],color='black')
#plt.plot(x_ref ,f_ref['vy'][:],color='black')
plt.plot(x_ref ,f_ref['Bx'][:],color='black')
plt.plot(x_ref ,f_ref['By'][:],color='black')




plt.legend()
plt.savefig('../../comparison_SS_B5.png')
plt.savefig('../../comparison_SS_B5.pdf')
