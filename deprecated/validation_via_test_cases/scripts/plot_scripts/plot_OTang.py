import numpy as np
import matplotlib.pyplot as plt
import h5py


f_ar = h5py.File('../../outputs/test_OTang_AR3/output_OTang_AR3_time_000000002.h5')
f_hllc = h5py.File('../../outputs/test_OTang_Rusanov/output_OTang_Rusanov_time_000000002.h5')
f_ref = h5py.File('../../reference_solutions/reference_OTang.h5')


ar   = f_ar['d'][:]
hllc   = f_hllc['d'][:]
ref   = f_ref['d'][:]


ar1 = np.squeeze(ar)
hllc1 = np.squeeze(hllc)


ref1 = np.squeeze(ref)

fig, (axAR, axref, axHLLC) = plt.subplots(1, 3)

axAR.imshow(ar1, interpolation='none')
axAR.set_title('AR3')
axHLLC.set_title('Rusanov')

axref.set_title('Reference')

axHLLC.imshow(hllc1, interpolation='none')
axref.imshow(ref1, interpolation='none')

plt.savefig('../../comparison_OTang.png')
plt.savefig('../../comparison_OTang.pdf')
