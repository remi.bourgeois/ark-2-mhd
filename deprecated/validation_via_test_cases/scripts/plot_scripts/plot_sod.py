import numpy as np
import matplotlib.pyplot as plt
import h5py

n_simu=64
n_ref =2048

f_ar = h5py.File('../../outputs/test_sod_allregime/output_sod_allregime_time_000000002.h5')
f_hllc = h5py.File('../../outputs/test_sod_hllc/output_sod_hllc_time_000000002.h5')
f_ref = h5py.File('../../reference_solutions/reference_sod.h5')

ar   = f_ar['d'][:]
hllc = f_hllc['d'][:]
ref  = f_ref['d'][:]


ar1 = np.squeeze(ar)
hllc1 = np.squeeze(hllc)
ref1 = np.squeeze(ref)

x=np.linspace(1,n_simu,n_simu)
x_ref=np.linspace(1,n_simu,n_ref)

plt.xlabel('x')
plt.ylabel('density')
plt.title(r"Comparison of the HLLC and AllRegime siumlations with a reference solution." "\n" r"Check the wave speeds and look for unphysical oscillations")


plt.plot(x    ,ar1  ,label='all_regime, n='+str(n_simu),color='red')
plt.plot(x    ,hllc1,label='hllc, n='+str(n_simu),color='blue')
plt.plot(x_ref,ref1 ,label='reference solution',color='black')
plt.legend()
plt.savefig('../../comparison_sod.png')
plt.savefig('../../comparison_sod.pdf')
