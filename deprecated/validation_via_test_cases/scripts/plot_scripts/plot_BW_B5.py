import numpy as np
import matplotlib.pyplot as plt
import h5py

n_simu=512
n_ref =4096

f_hllc = h5py.File('../../outputs/test_BW_B5/output_BW_B5_time_000000002.h5')
f_ref = h5py.File('../../reference_solutions/reference_BW.h5')

hllc_d = f_hllc['d'][:]
ref_d  = f_ref['d'][:]

hllc_mx = f_hllc['mx'][:]
ref_mx  = f_ref['mx'][:]

hllc_By = f_hllc['By'][:]
ref_By  = f_ref['By'][:]

hllc_vx = hllc_mx/hllc_d
ref_vx  = ref_mx/ref_d

hllc_d1 = np.squeeze(hllc_d)
ref_d1 = np.squeeze(ref_d)

hllc_vx1 = np.squeeze(hllc_vx)
ref_vx1 = np.squeeze(ref_vx)

hllc_By1 = np.squeeze(hllc_By)
ref_By1 = np.squeeze(ref_By)

x=np.linspace(1,n_simu,n_simu)
x_ref=np.linspace(1,n_simu,n_ref)

plt.xlabel('x')
plt.ylabel('quantities')
plt.title(r"Comparison of the B5 with a reference solution." "\n" r"Check the wave speeds and look for unphysical oscillations")


plt.plot(x    ,hllc_d1,label='Density, AR 3 waves n='+str(n_simu),color='blue')
plt.plot(x_ref,ref_d1 ,label='reference solution',color='black')

plt.plot(x    ,hllc_vx1,label='x velocity AR 3 waves n='+str(n_simu),color='red')
plt.plot(x_ref,ref_vx1 ,color='black')

plt.plot(x    ,hllc_By1,label='y Mag Field AR 3 waves n='+str(n_simu),color='purple')
plt.plot(x_ref,ref_By1 ,color='black')


plt.legend()
plt.savefig('../../comparison_BW_B5.png')
plt.savefig('../../comparison_BW_B5.pdf')
