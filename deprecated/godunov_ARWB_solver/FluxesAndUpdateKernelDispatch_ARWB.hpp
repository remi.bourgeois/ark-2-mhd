#pragma once

#include "HydroParams.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"

#include <Kokkos_Core.hpp>

namespace hydro
{

template < class D, class... P >
void FluxesAndUpdateKernelDispatch_ARWB( const Params& params, const UniformGrid& grid,
                                    const Kokkos::View< D, P... >& u,
                                    const Kokkos::View< D, P... >& q, Real dt );

#define ETI_FluxesAndUpdateKernelDispatch_ARWB()                                                   \
    void FluxesAndUpdateKernelDispatch_ARWB( const Params& params,                               \
                                        const UniformGrid& grid, const Array3d& u,   \
                                        const Array3d& q, Real dt )

extern template ETI_FluxesAndUpdateKernelDispatch_ARWB();

} // namespace hydro
