#pragma once

#include "HydroBaseKernel.hpp"
#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"
#include "ars/HydroRiemann_ARWB.hpp"
#include "ars/MHDRiemann_ARWB_3W.hpp"

#include <Kokkos_Core.hpp>

namespace hydro
{

template <class RiemannSolver >
class FluxesAndUpdateKernel_ARWB : public BaseKernel
{
public:
    using Super = BaseKernel;

    using Euler = MHDSystem;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;
    using RealVector = RealVector3d;

    using IntVector = IntVectorNd< three_d>;
    using TeamPolicy = Kokkos::TeamPolicy< Kokkos::IndexType< Int > >;
    using Team = typename TeamPolicy::member_type;
    static constexpr int ghostDepth = 0;

    FluxesAndUpdateKernel_ARWB( const Params& params, const UniformGrid& grid,
                           const Array3d& u, const Array3d& q, Real dt )
        : m_u( u )
        , m_cu( u )
        , m_q( q )
        , m_grid( grid )
        , m_eos( params.thermo )
        , m_riemann_solver( params, m_eos, grid )
        , m_dt( dt )
    {
    }

    KOKKOS_INLINE_FUNCTION
    void operator()( const Team& team ) const
    {
        using namespace constants;

        // From given team compute coordinates of cell (0, jy, jz).
        const IntVector coord0 = Super::teamToCoord( team, m_grid, ghostDepth );
        // Get linear index from cell (0, jy, jz).
        const Int j0 = m_grid.coordToIndex( coord0 );

        const auto vec_loop = Super::ArkTeamVectorRange(
            team, m_grid.m_ghostWidths[ IX ] - ghostDepth,
            m_grid.m_nbCells[ IX ] + m_grid.m_ghostWidths[ IX ] + ghostDepth );
        Kokkos::parallel_for( Kokkos::ThreadVectorRange( team, vec_loop.start, vec_loop.end ),
                              [ & ]( const Int jx ) {
                                  const Int j = j0 + jx;
                                  ConsState u_j = Super::getCons( m_cu, j );

                                  updateAlongFace( Tags::DirX, Tags::SideL, j, u_j );
                                  updateAlongFace( Tags::DirX, Tags::SideR, j, u_j );
                                  updateAlongFace( Tags::DirY, Tags::SideL, j, u_j );
                                  updateAlongFace( Tags::DirY, Tags::SideR, j, u_j );
                                  updateAlongFace( Tags::DirZ, Tags::SideL, j, u_j );
                                  updateAlongFace( Tags::DirZ, Tags::SideR, j, u_j );

                                  Super::set( m_u, j, u_j );
                              } );
    }

    template < int idir, int iside, std::enable_if_t< ( idir >= three_d ), int > = 0 >
    KOKKOS_FORCEINLINE_FUNCTION void updateAlongFace( std::integral_constant< int, idir >,
                                                      std::integral_constant< int, iside >, Int,
                                                      ConsState& ) const
    {
    }

    template < int idir, int iside, std::enable_if_t< ( idir < three_d ), int > = 0 >
    KOKKOS_FORCEINLINE_FUNCTION void
    updateAlongFace( std::integral_constant< int, idir > idir_tag,
                     std::integral_constant< int, iside > iside_tag, Int j, ConsState& u_j ) const
    {
        using namespace constants;

        const Int j_L = iside == 0 ? j - m_grid.m_strides[ idir ] : j;
        const Int j_R = iside == 0 ? j : j + m_grid.m_strides[ idir ];

        const PrimState q_L = Super::getPrim( m_q, j_L );
        const PrimState q_R = Super::getPrim( m_q, j_R );

        const Real side = iside == 0 ? -one : +one;

        IntVector coord=m_grid.indexToCoord(j);

        bool flag_bc = false;
        // Are we on an edge ? (top / bottom )
        if (idir == IZ)
        {
          if (coord[IZ] == m_grid.m_ghostWidths[IZ])
          {
            //std::cout<<coord[IZ]<< std::endl;
            //if (side==-1){flag_bc=true;}

          }

          if (coord[IZ] == m_grid.m_nbCells[IZ]-1+m_grid.m_ghostWidths[IZ])
          {
            //std::cout<<coord[IZ]<< std::endl;
            //if (side==1){flag_bc=true;}
          }
        }



        const ConsState flux = m_riemann_solver( q_L, q_R, idir_tag, side, flag_bc);

        const Real sdtdSdV = side * m_dt * m_grid.getdSdV( j, idir_tag, iside_tag );
        u_j.d -= sdtdSdV * flux.d;
        u_j.m -= sdtdSdV * flux.m;
        u_j.e -= sdtdSdV * flux.e;
        u_j.B -= sdtdSdV * flux.B;
        u_j.dX -= sdtdSdV * flux.dX;


    }

    const Array3d m_u;
    const ConstArray3d m_cu;
    const ConstArray3d m_q;
    const UniformGrid m_grid;
    const EquationOfState m_eos;
    const RiemannSolver m_riemann_solver;
    Real m_dt;
};

} // namespace hydro
