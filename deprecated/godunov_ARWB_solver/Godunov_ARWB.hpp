#pragma once

#include "cxxtimer.hpp"
#include "HydroConstants.hpp"
#include "HydroProblem.hpp"
#include "HydroSolver.hpp"
#include "HydroTypes.hpp"
#include "io/Writer.hpp"
#include "Utils.hpp"
#include "TimeStep.hpp"

#include <memory>
#include <vector>

namespace hydro
{

class Godunov_ARWB_Solver: public Solver
{
    using Super           = Solver;

    using Euler           = MHDSystem;
    using EquationOfState = typename Euler::EquationOfState;
    using VC              = typename Euler::VarCons;
    using VP              = typename Euler::VarPrim;
    static constexpr int nbvar = Euler::nbvar;

    static constexpr Int ghostWidth {2};

    using Array           = Array3d;
    using HostArray       = HostArray3d;

public:
    Godunov_ARWB_Solver(std::shared_ptr<Problem> problem);
    Godunov_ARWB_Solver(const Godunov_ARWB_Solver& x) = default;
    Godunov_ARWB_Solver(Godunov_ARWB_Solver&& x) = default;
#if defined(__INTEL_COMPILER) && __INTEL_COMPILER <= 1900
    ~Godunov_ARWB_Solver() override {};
#else
    ~Godunov_ARWB_Solver() override = default;
#endif
  //  Godunov_ARWB_Solver& operator=(const Godunov_ARWB_Solver& x) = default;
  //  Godunov_ARWB_Solver& operator=(Godunov_ARWB_Solver&& x) = default;

    Real computeTimeStep() final;
    void nextIteration(Real dt) final;
    void prepareNextOutput(Real& dt) final;
    void saveOutput() final;
    bool finished() const final;
    void printMonitoring(double t_tot) const final;
    bool shouldPrintInformation() const final;
    void printInformation() const final;
    double memoryUsage() const final;

private:
    void computeFluxesAndUpdate(Real dt);

private:
    std::shared_ptr<Problem> m_problem;
    std::shared_ptr<Params> m_params;
    const UniformGrid m_grid;
    std::shared_ptr<io::WriterBase> m_writer;
    bool m_should_save;

    const Array m_u;
    const Array m_q;
    const HostArray m_u_host;

    Int m_nStepmax;
    Real m_tEnd;
    TimeStep m_dt;

    std::vector<cxxtimer::Timer> timers;
};
 class Godunov_ARWB_Solver;

}
