#pragma once

#include "HydroBaseKernel.hpp"
#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"
#include "ars/HydroRiemann_ARWB.hpp"
#include "ars/MHDRiemann_ARWB_3W.hpp"


#include <Kokkos_Core.hpp>

namespace hydro
{

template <class RiemannSolver >
void ExecuteFluxesAndUpdate_ARWB( const Params& params, const UniformGrid& grid,
                             const Array3d& u, const Array3d& q, Real dt );

#define ETI_ExecuteFluxesAndUpdate_ARWB( ars )                                                     \
    void ExecuteFluxesAndUpdate_ARWB< ars >(                                                       \
        const Params& params, const UniformGrid& grid, const Array3d& u,      \
        const Array3d& q, Real dt )

extern template ETI_ExecuteFluxesAndUpdate_ARWB( riemann::ARWB );
extern template ETI_ExecuteFluxesAndUpdate_ARWB( riemann::MHD_ARWB_3W );

} // namespace hydro
