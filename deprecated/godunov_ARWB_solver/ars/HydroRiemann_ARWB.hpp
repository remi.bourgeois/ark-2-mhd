#pragma once

#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"

#include <cmath>

namespace hydro { namespace riemann
{

class ARWB
{
public:
    using Euler           = MHDSystem;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState       = typename Euler::ConsState;
    using PrimState       = typename Euler::PrimState;

    ARWB(const Params& params, const EquationOfState& eos, const UniformGrid& grid);
    //ARWB(const ARWB& x) = default;
    //ARWB(ARWB&& x) = default;
    ~ARWB() = default;
    //ARWB& operator=(const ARWB& x) = default;
    //ARWB& operator=(ARWB&& x) = default;

    template <int idir>
    KOKKOS_FORCEINLINE_FUNCTION ConsState
    operator()(const PrimState &q_L, const PrimState &q_R,
               std::integral_constant<int, idir> idir_tag,
             const int& side, const bool& flag_bc) const;

  private:
    const EquationOfState m_eos;
    const Real m_K;
    const RealVector3d m_g;
    const RealVector3d m_dl;


};
inline
ARWB::ARWB(const Params& params, const EquationOfState& eos, const UniformGrid& grid)
    : m_eos {eos}
    , m_K   {params.hydro.K}
    , m_g   {params.hydro.g}
    , m_dl  {grid.m_dl}
{
}

template<int idir>
KOKKOS_FORCEINLINE_FUNCTION
typename ARWB::ConsState
ARWB::operator()(const PrimState &q_L, const PrimState &q_R,
                              std::integral_constant<int, idir> idir_tag,
                            const int& side, const bool& flag_bc) const
{
    const Real un_L = q_L.v( idir_tag );
    const Real un_R = q_R.v( idir_tag );

    // Find the largest eigenvalues in the normal direction to the interface
    const Real c_L = Euler::computeSpeedOfSound(q_L, m_eos);
    const Real c_R = Euler::computeSpeedOfSound(q_R, m_eos);

    const Real a = 1.1 * std::fmax(q_R.d * c_R, q_L.d * c_L);

    const Real M = 0.5*(q_L.d + q_R.d)*m_g[idir]*m_dl[idir];

    const Real ustar = 0.5 * (un_R+un_L) - 0.5 * (q_R.p-q_L.p - M)/a;

    const Real Ma = std::fmax(std::fabs(un_L)/c_L, std::fabs(un_R)/c_R);
    const Real theta = std::fmin(Ma, 1.0);
    const Real pistar = 0.5 * (q_R.p + q_L.p) - 0.5 * theta * a * (un_R - un_L);


    PrimState q;
    if ( ustar > 0.0 )
    {
        q = q_L;
    }
    else
    {
        q = q_R;
    }
    const Real eint = Euler::computeInternalEnergy(q, m_eos);
    const Real ekin = Euler::computeKineticEnergy(q);
    const Real rhoE = eint+ekin;

    ConsState flux;
    flux.d = q.d * ustar;
    flux.e = (rhoE + pistar) * ustar - side*M*ustar*0.5;
    flux.m = (q.d * ustar) * q.v;
    flux.m ( idir_tag ) += pistar -side*M*0.5;
    flux.B = 0.0*q.B;
    flux.dX = 0.0;

    return flux;
}

}}
