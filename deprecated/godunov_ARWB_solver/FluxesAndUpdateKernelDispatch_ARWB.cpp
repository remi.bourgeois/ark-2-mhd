#include "FluxesAndUpdateKernelDispatch_ARWB.hpp"

#include "FluxesAndUpdateExecution_ARWB.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"
#include "ars/HydroRiemann_ARWB.hpp"
#include "ars/MHDRiemann_ARWB_3W.hpp"


#include <Kokkos_Core.hpp>

namespace hydro
{

template <class D, class... P >
void
FluxesAndUpdateKernelDispatch_ARWB( const Params& params, const UniformGrid& grid,
                               const Kokkos::View< D, P... >& u, const Kokkos::View< D, P... >& q,
                               Real dt )
{
    if ( params.run.riemann == "ARWB" )
    {
        ExecuteFluxesAndUpdate_ARWB< riemann::ARWB >( params, grid, u, q, dt );
    }
    else if ( params.run.riemann == "mhd_arwb_3w" )
    {
        ExecuteFluxesAndUpdate_ARWB< riemann::MHD_ARWB_3W >( params, grid, u, q, dt );
    }
    else
    {
        throw;
    }
}

template ETI_FluxesAndUpdateKernelDispatch_ARWB();

} // namespace hydro
