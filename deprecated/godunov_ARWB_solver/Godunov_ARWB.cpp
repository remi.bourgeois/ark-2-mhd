#include "Godunov_ARWB.hpp"

#include "ConservativeToPrimitiveExecution.hpp"
#include "cxxtimer.hpp"
#include "DistributedMemorySession.hpp"
#include "FluxesAndUpdateKernelDispatch_ARWB.hpp"
#include "HydroConstants.hpp"
#include "HydroProblem.hpp"
#include "HydroSolver.hpp"
#include "HydroTypes.hpp"
#include "io/Reader.hpp"
#include "io/Writer.hpp"
#include "Print.hpp"
#include "ConvectionSourceTermExecution.hpp"
#include "TimeStep.hpp"
#include "TimeStepExecution.hpp"
#include "Utils.hpp"

#include <chrono>
#include <iomanip>
#include <limits>
#include <memory>
#include <utility>
#include <vector>

namespace hydro
{

Godunov_ARWB_Solver::Godunov_ARWB_Solver(std::shared_ptr<Problem> problem)
    : Solver           {}
    , m_problem        {problem}
    , m_params         {problem->m_params}
    , m_grid           {m_params->mesh.low, m_params->mesh.up, m_params->mesh.nbCells,
                        m_params->mesh.dom, ghostWidth}
    , m_writer         {}
    , m_should_save    {false}
    , m_u              {"U", static_cast<typename Array::size_type>(m_grid.nbCells())}
    , m_q              {"Q", static_cast<typename Array::size_type>(m_grid.nbCells())}
    , m_u_host         {Kokkos::create_mirror(m_u)}
    , m_nStepmax {m_params->run.nStepmax}
    , m_tEnd {m_params->run.tEnd}
    , m_dt {}
    , timers {}
{
    std::vector<std::string> var_names = Euler::cons_names();
    std::vector<std::pair<int, std::string>> variables_to_save;
    for (int ivar = 0; ivar < nbvar; ++ivar)
    {
        variables_to_save.push_back(std::make_pair(ivar, var_names[ivar]));
    }

    m_writer = io::WriterFactory::New(m_grid, *m_params,
                                           m_params->output.type,
                                           m_params->output.prefix,
                                           variables_to_save);

    if (m_params->run.restart)
    {
        Int outputId = -1;
        Int restartId = -1;
        io::Reader reader(m_grid, *m_params, variables_to_save);
        reader.read(m_u_host, m_grid, Super::m_iteration, Super::m_t, outputId, restartId);
        Kokkos::deep_copy(m_u, m_u_host);
        m_writer->setOutputId(++outputId);
        m_writer->setRestartId(++restartId);
        m_should_save = false;
    }
    else
    {
        m_problem->initialize(m_u, m_grid);
        m_should_save = true;
    }
    m_problem->make_boundaries(m_u, m_grid);
    ExecuteConservativeToPrimitive(*m_params, m_grid, m_u, m_q);
    timers.push_back(cxxtimer::Timer ("Hydro (Muscl) step"));
    timers.push_back(cxxtimer::Timer ("Gravity step"));
    timers.push_back(cxxtimer::Timer ("Boundaries"));
    timers.push_back(cxxtimer::Timer ("Cons to prim step"));
    timers.push_back(cxxtimer::Timer ("I/O"));
    timers.push_back(cxxtimer::Timer ("Time step"));
    timers.push_back(cxxtimer::Timer ("Viscosity step"));
    timers.push_back(cxxtimer::Timer ("Thermal diffusion step"));
    timers.push_back(cxxtimer::Timer ("Convection Source terms"));

}

Real Godunov_ARWB_Solver::computeTimeStep()
{
    timers[5].start();
    m_dt = ExecuteTimeStep(*m_params, m_grid, m_q);
    const Real dt_s {m_dt.min()};
    const Real dt {Super::m_t + dt_s > m_tEnd ? utils::adjust(Super::m_t, m_tEnd) : dt_s};
    timers[5].stop();
    return dt;
}

void Godunov_ARWB_Solver::nextIteration(Real dt)
{
    if (m_params->hydro.hydro_enabled)
    {
        timers[0].start();
        FluxesAndUpdateKernelDispatch_ARWB(*m_params, m_grid, m_u, m_q, dt);
        timers[0].stop();
    }

    if (m_params->hydro.convection_source_term_enabled)
    {
        timers[8].start();
        ExecuteConvectionSourceTerm(*m_params, m_grid, m_u, m_q, dt);
        timers[8].stop();
    }

    // fill ghost cell in data_in
    timers[2].start();
    m_problem->make_boundaries(m_u, m_grid);
    timers[2].stop();

    // convert conservative variable into primitives ones for the entire domain
    timers[3].start();
    ExecuteConservativeToPrimitive(*m_params, m_grid, m_u, m_q);
    timers[3].stop();

    Super::m_t += dt;
    Super::m_iteration++;
}

void Godunov_ARWB_Solver::prepareNextOutput(Real& dt)
{
    m_should_save = false;
    auto dt_io = m_params->output.dt_io;
    const auto dt_old = dt;
    if (dt_io > 0.0)
    {
        // Next physical time to do output
        auto t_io = (std::floor(Super::m_t / dt_io) + constants::one)*dt_io;
        if (Super::m_t + dt >= t_io)
        {
            // Make sure dt is big enough such that next t_io is different than m_t+dt
            dt = utils::adjust(Super::m_t, t_io,
                               [dt_io](Real v1, Real v2, Real delta)
                               {
                                   return (delta + v1 < v2)
                                       || ((std::floor((v1+delta) / dt_io) + constants::one)*dt_io) <= v2;
                               });
            if (dt_old < dt)
            {
                throw std::runtime_error("Time step is increasing whereas it should decrease.\n");
            }
            m_should_save = true;
        }
    }

    if (m_t + dt >= m_tEnd)
    {
        m_should_save = true;
    }

    if (m_params->output.nOutput < 0)
    {
        m_should_save = true;
    }

    if ((m_params->output.nOutput != 0) &&
        (Super::m_iteration % m_params->output.nOutput == 0))
    {
        m_should_save = true;
    }
}

void Godunov_ARWB_Solver::saveOutput()
{
    timers[4].start();
    if (m_should_save)
    {
        Kokkos::deep_copy(m_u_host, m_u);
        m_writer->write(m_u_host, m_grid, Super::m_iteration, Super::m_t,
                        m_params->thermo.gamma, m_params->thermo.mmw);
    }
    timers[4].stop();
}

bool Godunov_ARWB_Solver::finished() const
{
    return (Super::m_t >= m_tEnd || Super::m_iteration >= m_nStepmax);
}

void Godunov_ARWB_Solver::printMonitoring(double t_tot) const
{
    using float_duration = std::chrono::duration<double>;

    Print() << "Total time " << t_tot << '\n';
    for (const auto& timer : timers)
    {
        double time {timer.template count<float_duration>()};
        Print() << " - " << timer.name() << ' ' << time;
        Print() << " [" << std::setw(5) << std::setfill(' ') << std::setprecision(1) << std::fixed << time/t_tot*100.0 << "%]\n";
    }
    const double w_perf {Session::getNProc() * static_cast<double>(Super::m_iteration) * static_cast<double>(m_grid.nbCells()) / t_tot * 1.0E-6};
    Print() << "Perf (Wall clock): " << w_perf << " Mcell-updates/s\n";
}

bool Godunov_ARWB_Solver::shouldPrintInformation() const
{
    if (m_params->run.info == 0)
    {
        return false;
    }

    if (finished())
    {
        return true;
    }

    return Super::m_iteration%m_params->run.info == 0;
}

void Godunov_ARWB_Solver::printInformation() const
{
    Print oss {};
    oss << std::setprecision(std::numeric_limits<Real>::digits10);
    oss << std::scientific;
    oss << "Step n=";
    oss << std::setw(std::numeric_limits<int>::digits10) << std::setfill('.') << Super::m_iteration;
    oss << "; time t=" << Super::m_t;
    oss << " [" << std::setw(5) << std::setfill(' ') << std::setprecision(1) << std::fixed << 100.0*static_cast<double>(Super::m_t/m_tEnd) << "%]\n";
    oss << std::setprecision(std::numeric_limits<Real>::digits10);
    oss << std::scientific;
    oss << " * Time step dt=" << m_dt.min() << std::endl;
}

double Godunov_ARWB_Solver::memoryUsage() const
{
    auto memory = m_u.span() + m_q.span();
    return static_cast<double>(memory * sizeof(Real)) * 1E-6;
}
class Godunov_ARWB_Solver;
}
