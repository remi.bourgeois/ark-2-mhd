#include "FluxesAndUpdateExecution_ARWB.hpp"
#include "GodunovFluxesKernel_ARWB.hpp"

#include "HydroBaseKernel.hpp"
#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"
#include "ars/HydroRiemann_ARWB.hpp"
#include "ars/MHDRiemann_ARWB_3W.hpp"


#include <Kokkos_Core.hpp>

namespace hydro
{

template < class RiemannSolver >
void
ExecuteFluxesAndUpdate_ARWB( const Params& params, const UniformGrid& grid,
                        const Array3d& u, const Array3d& q, Real dt )
{
    using Kernel = FluxesAndUpdateKernel_ARWB< RiemannSolver >;
    using TeamPolicy = typename Kernel::TeamPolicy;
    FluxesAndUpdateKernel_ARWB< RiemannSolver > kernel( params, grid, u, q, dt );
    const int league_size = Kernel::computeLeagueSize( grid.m_nbCells, Kernel::ghostDepth );
    const int vector_length = TeamPolicy::vector_length_max();
    TeamPolicy policy( league_size, Kokkos::AUTO, vector_length );
    Kokkos::parallel_for( "Godunov kernel - TeamPolicy", policy, kernel );
}

template ETI_ExecuteFluxesAndUpdate_ARWB(riemann::ARWB );
template ETI_ExecuteFluxesAndUpdate_ARWB(riemann::MHD_ARWB_3W );


} // namespace hydro
