#include "IsothermalAtmosphereAtRestProblem.hpp"

#include "IsothermalAtmosphereAtRestBoundariesKernel.hpp"
#include "IsothermalAtmosphereAtRestInitKernel.hpp"
#include "IsothermalAtmosphereAtRestParams.hpp"
#include "HydroUniformGrid.hpp"
#include "HydroParams.hpp"
#include "HydroProblem.hpp"
#include "HydroTypes.hpp"

#include <memory>

namespace hydro { namespace problems
{

IsothermalAtmosphereAtRestProblem::IsothermalAtmosphereAtRestProblem(const std::shared_ptr<Params>& params)
    : Problem {params}
    , m_prob_params  {std::make_shared<IsothermalAtmosphereAtRestParams>(params->reader)}
{
}

void IsothermalAtmosphereAtRestProblem::initialize(Array u, const UniformGrid& grid) const
{
    Print() << "Initializing Isothermal atmosphere at equilibrium problem... ";

    IsothermalAtmosphereAtRestInitKernel::apply(*m_params, *m_prob_params, u, grid);

    Print() << "done" << std::endl;
}

void IsothermalAtmosphereAtRestProblem::make_boundaries_user(Array u, const UniformGrid& grid, int, int iside)
{
    IsothermalAtmosphereAtRestBoundariesKernel::apply(u, grid, *m_params, *m_prob_params, iside);
}

}}
