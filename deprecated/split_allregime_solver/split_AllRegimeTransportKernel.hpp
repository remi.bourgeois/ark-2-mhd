#pragma once

#include "HydroBaseKernel.hpp"
#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"

#include <Kokkos_Core.hpp>
#include <cmath>

namespace hydro
{

class TransportStepKernel : public BaseKernel
{
public:
    using Super = BaseKernel;

    using Euler = MHDSystem;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;

    using IntVector = IntVectorNd< three_d >;
    using TeamPolicy = Kokkos::TeamPolicy< Kokkos::IndexType< Int > >;
    using Team = typename TeamPolicy::member_type;

    static constexpr int ghostDepth = 0;

    TransportStepKernel( const Params& params, const UniformGrid& grid,
                         const Array3d& u, const Array3d& uAc,
                         const Array3d& q, Real dt )
        : m_K( params.hydro.K )
        , m_eos( params.thermo )
        , m_grid( grid )
        , m_u( u )
        , m_uAc( uAc )
        , m_q( q )
        , m_dt( dt )
        , m_dphi {}
    {
        for ( int idir = 0; idir < three_d; ++idir )
        {
            m_dphi( idir ) = -params.hydro.g[ idir ] * grid.dl( idir );
        }
    }

    template < int idir, int iside, std::enable_if_t< ( idir >= three_d ), int > = idir >
    KOKKOS_INLINE_FUNCTION void updateAlongFace( std::integral_constant< int, idir >,
                                                 std::integral_constant< int, iside >, const Int,
                                                 const ConsState&, ConsState& ) const noexcept
    {
    }

    template < int idir, int iside, std::enable_if_t< ( idir < three_d ), int > = idir >
    KOKKOS_INLINE_FUNCTION void updateAlongFace( std::integral_constant< int, idir > idir_tag,
                                                 std::integral_constant< int, iside > iside_tag,
                                                 const Int j, const ConsState& uAc_j,
                                                 ConsState& u_j ) const noexcept
    {
        using namespace constants;

        const Int j_L = iside == 0 ? j - m_grid.m_strides[ idir ] : j;
        const Int j_R = iside == 0 ? j : j + m_grid.m_strides[ idir ];

        const PrimState q_L = Super::getPrim( m_q, j_L );
        const PrimState q_R = Super::getPrim( m_q, j_R );

        const Real c_L = Euler::computeSpeedOfSound( q_L, m_eos );
        const Real c_R = Euler::computeSpeedOfSound( q_R, m_eos );

        const Real vn_L = q_L.v( idir );
        const Real vn_R = q_R.v( idir );

        const Real grav_corr = -half * ( q_L.d + q_R.d ) * m_dphi( idir );

        const Real a = m_K * std::fmax( q_L.d * c_L, q_R.d * c_R );
        const Real uStar = +half * ( vn_L + vn_R ) - half * ( q_R.p - q_L.p - grav_corr ) / a;

        const Int j_upwind = uStar > zero ? j_L : j_R;
        const ConsState uAc_upwind = Super::getCons( m_uAc, j_upwind );
        const Real side = iside == 0 ? -one : +one;
        const Real dtdSdV = m_dt * m_grid.getdSdV( j, idir_tag, iside_tag );
        const Real factor = side * dtdSdV * uStar;
        u_j.d += factor * ( uAc_j.d - uAc_upwind.d );
        u_j.e += factor * ( uAc_j.e - uAc_upwind.e );
        u_j.m += factor * ( uAc_j.m - uAc_upwind.m );
    }

    KOKKOS_INLINE_FUNCTION
    void operator()( const Team& team ) const
    {
        using namespace constants;

        // From given team compute coordinates of cell (0, jy, jz).
        const IntVector coord0 = Super::teamToCoord( team, m_grid, ghostDepth );
        // Get linear index from cell (0, jy, jz).
        const int j0 = m_grid.coordToIndex( coord0 );

        Int start = m_grid.m_ghostWidths[ IX ] - ghostDepth;
        Int end = m_grid.m_ghostWidths[ IX ] + ( m_grid.m_nbCells[ IX ] + ghostDepth );
        Kokkos::parallel_for( Super::ArkTeamVectorRange( team, start, end ), [ & ]( const Int jx ) {
            const int j = j0 + jx;
            const ConsState uAc_j = Super::getCons( m_uAc, j );
            ConsState u_j = uAc_j;

            updateAlongFace( Tags::DirX, Tags::SideL, j, uAc_j, u_j );
            updateAlongFace( Tags::DirX, Tags::SideR, j, uAc_j, u_j );
            updateAlongFace( Tags::DirY, Tags::SideL, j, uAc_j, u_j );
            updateAlongFace( Tags::DirY, Tags::SideR, j, uAc_j, u_j );
            updateAlongFace( Tags::DirZ, Tags::SideL, j, uAc_j, u_j );
            updateAlongFace( Tags::DirZ, Tags::SideR, j, uAc_j, u_j );

            Super::set( m_u, j, u_j );
        } );
    }

private:
    const Real m_K;
    const EquationOfState m_eos;
    const UniformGrid m_grid;
    const Array3d m_u;
    const ConstArray3d m_uAc;
    const ConstArray3d m_q;
    const Real m_dt;
    Vector< three_d, Real > m_dphi;
};

} // namespace hydro
