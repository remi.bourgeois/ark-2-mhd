#pragma once

#include "cxxtimer.hpp"
#include "HydroUniformGrid.hpp"
#include "HydroConstants.hpp"
#include "HydroProblem.hpp"
#include "HydroSolver.hpp"
#include "HydroTypes.hpp"
#include "io/Writer.hpp"
#include "Utils.hpp"
#include "TimeStep.hpp"

#include <memory>
#include <vector>

namespace hydro
{
class split_AllRegime : public Solver
{
    using Super           = Solver;

    using Euler           = MHDSystem;
    using EquationOfState = typename Euler::EquationOfState;
    using VC              = typename Euler::VarCons;
    using VP              = typename Euler::VarPrim;
    static constexpr int nbvar = Euler::nbvar;

    static constexpr Int ghostWidth {2};

    using Array       = Array3d;
    using HostArray   = HostArray3d;

public:
    split_AllRegime(std::shared_ptr<Problem> problem);
    split_AllRegime(const split_AllRegime& x) = default;
    split_AllRegime(split_AllRegime&& x) = default;
#if defined(__INTEL_COMPILER) && __INTEL_COMPILER <= 1900
    ~split_AllRegime() override {};
#else
    ~split_AllRegime() override = default;
#endif
    //split_AllRegime& operator=(const split_AllRegime& x) = default;
    //split_AllRegime& operator=(split_AllRegime&& x) = default;

    Real computeTimeStep() final;
    void nextIteration(Real dt) final;
    void saveOutput() final;
    void prepareNextOutput(Real& dt) final;
    bool finished() const final;
    void printMonitoring(double t_tot) const final;
    bool shouldPrintInformation() const final;
    void printInformation() const final;
    double memoryUsage() const final;

private:
    std::shared_ptr<Problem> m_problem;
    std::shared_ptr<Params> m_params;
    const UniformGrid m_grid;
    std::shared_ptr<io::WriterBase> m_writer;
    bool m_should_save;

    const Array m_u;
    const Array m_uAc;
    const Array m_q;
    const HostArray m_u_host;

    Int m_nStepmax;
    Real m_tEnd;
    TimeStep m_dt;

    std::vector<cxxtimer::Timer> timers;
};

class split_AllRegime;

}
