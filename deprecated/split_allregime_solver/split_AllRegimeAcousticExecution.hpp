#pragma once

#include "HydroBaseKernel.hpp"
#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"
#include "linalg/Vector.hpp"

#include <Kokkos_Core.hpp>
#include <cmath>

namespace hydro
{

void ExecuteAcousticStep( const Params& params, const UniformGrid& grid,
                          const Array3d& u, const Array3d& uAc,
                          const Array3d& q, Real dt );

#define ETI_ExecuteAcousticStep(  )                                                             \
    void ExecuteAcousticStep( const Params& params, const UniformGrid& grid,         \
                              const Array3d& u, const Array3d& uAc,                  \
                              const Array3d& q, Real dt )

ETI_ExecuteAcousticStep( );

} // namespace hydro
