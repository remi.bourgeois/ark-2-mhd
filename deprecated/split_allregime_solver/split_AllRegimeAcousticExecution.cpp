#include "split_AllRegimeAcousticExecution.hpp"

#include "split_AllRegimeAcousticKernel.hpp"

#include "HydroBaseKernel.hpp"
#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"
#include "linalg/Vector.hpp"

#include <Kokkos_Core.hpp>
#include <cmath>

namespace hydro
{

void
ExecuteAcousticStep( const Params& params, const UniformGrid& grid,
                     const Array3d& u, const Array3d& uAc, const Array3d& q,
                     Real dt )
{
    using Kernel = AcousticStepKernel;
    using TeamPolicy = typename Kernel::TeamPolicy;
    Kernel kernel( params, grid, u, uAc, q, dt );
    const int league_size = Kernel::computeLeagueSize( grid.m_nbCells, Kernel::ghostDepth );
    const int vector_length = TeamPolicy::vector_length_max();
    TeamPolicy policy { league_size, Kokkos::AUTO, vector_length };
    Kokkos::parallel_for( "Acoustic kernel - TeamPolicy", policy, kernel );
}

ETI_ExecuteAcousticStep( );

} // namespace hydro
