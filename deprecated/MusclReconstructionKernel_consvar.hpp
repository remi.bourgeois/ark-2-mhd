#pragma once

#include "HydroBaseKernel.hpp"
#include "HydroUniformGrid.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"

namespace hydro
{

class MusclReconstructionKernel_consvar : public BaseKernel
{
    using Super           = BaseKernel;

    using MHD           = MHDSystem;
    using EquationOfState = typename MHD::EquationOfState;
    using ConsState           = typename MHD::ConsState;

    using RealVector = RealVector3d;
    using VC              = typename MHD::VarCons;
    static constexpr int nbvar = MHD::nbvar;

    using Grid       = UniformGrid;

    using Array      = Array3d;
    using ConstArray = ConstArray3d;

    using TeamPolicy = Kokkos::TeamPolicy<Kokkos::IndexType<Int>>;
    using Team       = typename TeamPolicy::member_type;
    static constexpr int ghostDepth = 1;

public:
    MusclReconstructionKernel_consvar(const Params& params, const Grid& grid,
                              Array q, const Kokkos::Array<Array, 2*three_d>& qr, Real dt);

    static void apply(const Params& params, const Grid& grid,
                      Array q, const Kokkos::Array<Array, 2*three_d>& qr, Real dt);

    KOKKOS_INLINE_FUNCTION
    void operator()(const Team& team) const;


    KOKKOS_INLINE_FUNCTION
    ConsState trace_unsplit_hydro_along_dir(const ConsState& q, const Kokkos::Array<ConsState, three_d>& dq) const;

    KOKKOS_INLINE_FUNCTION
    static ConsState slope_unsplit_hydro_1d(const ConsState& q,
                                        const ConsState& qPlus,
                                        const ConsState& qMinus,
                                        Real slope_type);

    const Params m_params;
    Kokkos::Array<Array, 2*three_d> m_qr;
    const ConstArray m_q;
    const Grid m_grid;
    const RealVector m_dl;
    RealVector m_dtdl;
    const Real m_slope_type;
};

}
