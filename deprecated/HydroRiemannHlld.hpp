#pragma once

#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"

#include <cmath>

namespace hydro { namespace riemann
{

class Hlld
{
public:
    using MHD           = MHDSystem;
    using EquationOfState = typename MHD::EquationOfState;
    using ConsState       = typename MHD::ConsState;
    using PrimState       = typename MHD::PrimState;

    Hlld(const Params& params, const EquationOfState& eos);

    //Hlld(const Hlld& x) = default;

    //Hlld(Hlld&& x) = default;

    //~Hlld() = default;

    //Hlld& operator=(const Hlld& x) = default;

   //Hlld& operator=(Hlld&& x) = default;

    template<int idir>
    KOKKOS_FORCEINLINE_FUNCTION
    ConsState operator()(const PrimState& q_L, const PrimState& q_R, std::integral_constant<int, idir> idir_tag, const int& side, bool st_powell) const;

private:
    const EquationOfState m_eos;
};

inline
Hlld::Hlld(const Params&, const EquationOfState& eos)
    : m_eos {eos}
{
}

template<int idir>
KOKKOS_FORCEINLINE_FUNCTION
typename Hlld::ConsState
Hlld::operator()(const PrimState& q_L, const PrimState& q_R, std::integral_constant<int, idir> idir_tag, const int& side, bool st_powell) const
{
  using namespace constants;

  ConsState flux;

  const Real un_L = q_L.v( idir_tag );
  const Real un_R = q_R.v( idir_tag );

  const Real cfL = MHD::computeFastMagnetoAcousticSpeed(q_L, m_eos, idir);
  const Real cfR = MHD::computeFastMagnetoAcousticSpeed(q_R, m_eos, idir);

  const Real SL = fmin(un_L, un_R) - fmax(cfL, cfR);
  const Real SR = fmax(un_L, un_R) + fmax(cfL, cfR);

  if (SL > 0.0)
  {
    flux = MHD::template Flux<idir>(q_L, m_eos);
    return flux;
  }
  else if (SR < 0.0)
  {
    flux = MHD::template Flux<idir>(q_R, m_eos);
    return flux;
  }

  const Real emagR = MHD::computeMagneticEnergy(q_R);
  const Real emagL = MHD::computeMagneticEnergy(q_L);

  const Real pTR = q_R.p + emagR;
  const Real pTL = q_L.p + emagL;

  const Real Bn = 0.5*(q_R.B(idir_tag) + q_L.B(idir_tag));

  const Real SmurL=(SL - un_L)*q_L.d;
  const Real SmurR=(SR - un_R)*q_R.d;

  const Real SM = (SmurR*un_R - SmurL*un_L - pTR + pTL)
  / (SmurR - SmurL);

  const Real rho_Rstar = SmurR/(SR-SM); const Real sq_rho_Rstar = sqrt(rho_Rstar);
  const Real rho_Lstar = SmurL/(SL-SM); const Real sq_rho_Lstar = sqrt(rho_Lstar);

  const Real S_Lstar = SM - abs(Bn)/sq_rho_Lstar;
  const Real S_Rstar = SM + abs(Bn)/sq_rho_Rstar;

  const Real pTstar = (SmurR*pTR - SmurL*pTL + SmurL*SmurR*(un_R-un_L) )
  / ( SmurR - SmurL);

  Vector<three_d, Real> ustarL, ustarR;

  const Real coefuL = Bn*(SM - un_L)/(SmurL*(SL-SM)-Bn*Bn); // faire check 0
  const Real coefuR = Bn*(SM - un_R)/(SmurR*(SR-SM)-Bn*Bn);

  ustarL = q_L.v - coefuL*q_L.B;
  ustarR = q_R.v - coefuR*q_R.B;
  ustarR(idir) = SM;
  ustarL(idir) = SM;


  Vector<three_d, Real> BstarL, BstarR;

  const Real coefBL = (q_L.d*(SL-un_L)*(SL-un_L)-Bn*Bn)/(SmurL*(SL-SM)-Bn*Bn);
  const Real coefBR = (q_R.d*(SR-un_R)*(SR-un_R)-Bn*Bn)/(SmurR*(SR-SM)-Bn*Bn);

  BstarL = coefBL*q_L.B;
  BstarR = coefBR*q_R.B;
  BstarL(idir) = Bn;
  BstarR(idir) = Bn;

  const Real E_L = emagL + MHD::computeInternalEnergy(q_L, m_eos) +  MHD::computeKineticEnergy(q_L);
  const Real E_R = emagR + MHD::computeInternalEnergy(q_R, m_eos) +  MHD::computeKineticEnergy(q_R);

  const Real E_Lstar = ((SL - un_L)*E_L - pTL*un_L + pTstar*SM + Bn*(dot(q_L.v,q_L.B)-dot(ustarL, BstarL)))
  / (SL - SM);
  const Real E_Rstar = ((SR - un_R)*E_R - pTR*un_R + pTstar*SM + Bn*(dot(q_R.v,q_R.B)-dot(ustarR, BstarR)))
  / (SR - SM);

  if ((SL <= 0.0)&&(0.0<=S_Lstar))
  {
    PrimState  qLstar;
    ConsState  ULstar;
    qLstar.d = rho_Lstar;
    qLstar.v = ustarL;
    qLstar.B = BstarL;
    const Real eint = E_Lstar - half*dot(qLstar.B,qLstar.B) - half*qLstar.d*dot(qLstar.v,qLstar.v);
    qLstar.p = m_eos.computePressure(rho_Lstar, eint);

    ULstar       = MHD::primitiveToConservative(qLstar, m_eos);
    ConsState UL = MHD::primitiveToConservative(q_L   , m_eos);
    ConsState FL = MHD::template Flux<idir>(q_L, m_eos);

    flux.d = FL.d + SL*(ULstar.d - UL.d);
    flux.m = FL.m + SL*(ULstar.m - UL.m);
    flux.e = FL.e + SL*(ULstar.e - UL.e);
    flux.B = FL.B + SL*(ULstar.B - UL.B);

    return flux;
  }
  else if ((S_Rstar <= 0.0)&&(0.0<=SR))
  {
    PrimState  qRstar;
    ConsState  URstar;
    qRstar.d = rho_Rstar;
    qRstar.v = ustarR;
    qRstar.B = BstarR;
    const Real eint = E_Rstar - half*dot(qRstar.B,qRstar.B) - half*qRstar.d*dot(qRstar.v,qRstar.v);
    qRstar.p = m_eos.computePressure(rho_Rstar, eint);

    URstar       = MHD::primitiveToConservative(qRstar, m_eos);
    ConsState UR = MHD::primitiveToConservative(q_R   , m_eos);
    ConsState FR = MHD::template Flux<idir>(q_R, m_eos);

    flux.d = FR.d + SR*(URstar.d - UR.d);
    flux.m = FR.m + SR*(URstar.m - UR.m);
    flux.e = FR.e + SR*(URstar.e - UR.e);
    flux.B = FR.B + SR*(URstar.B - UR.B);

    return flux;
  }

  Vector<three_d, Real> uss, Bss;

  const Real signBn = 1.0*((Bn > 0) - (Bn < 0));

  uss = (1.0/(sq_rho_Lstar + sq_rho_Rstar))*(sq_rho_Lstar*ustarL + sq_rho_Rstar*ustarR + signBn*(BstarR - BstarL));

  Bss = (1.0/(sq_rho_Lstar + sq_rho_Rstar))*(sq_rho_Lstar*BstarL + sq_rho_Rstar*BstarR + signBn*sq_rho_Lstar*sq_rho_Rstar*(ustarR - ustarL));

  const Real rhoEssL = E_Lstar - sq_rho_Lstar*(dot(ustarL, BstarL) - dot(uss, Bss))*signBn;
  const Real rhoEssR = E_Rstar + sq_rho_Rstar*(dot(ustarR, BstarR) - dot(uss, Bss))*signBn;

  if ((SM <= 0.0)&&(0.0<=S_Rstar))
  {
    PrimState  qRsstar;
    ConsState  URsstar;
    qRsstar.d = rho_Rstar;
    qRsstar.v = uss;
    qRsstar.B = Bss;
    Real eint = rhoEssR - half*dot(qRsstar.B,qRsstar.B) - half*qRsstar.d*dot(qRsstar.v,qRsstar.v);
    qRsstar.p = m_eos.computePressure(rho_Rstar, eint);
    URsstar   = MHD::primitiveToConservative(qRsstar, m_eos);

    PrimState  qRstar;
    ConsState  URstar;
    qRstar.d = rho_Rstar;
    qRstar.v = ustarR;
    qRstar.B = BstarR;
    eint = E_Rstar - half*dot(qRstar.B,qRstar.B) - half*qRstar.d*dot(qRstar.v,qRstar.v);
    qRstar.p = m_eos.computePressure(rho_Rstar, eint);

    URstar       = MHD::primitiveToConservative(qRstar, m_eos);
    ConsState UR = MHD::primitiveToConservative(q_R   , m_eos);
    ConsState FR = MHD::template Flux<idir>(q_R, m_eos);

    flux.d = FR.d + S_Rstar*URsstar.d - (S_Rstar - SR)*URstar.d - SR*UR.d;
    flux.m = FR.m + S_Rstar*URsstar.m - (S_Rstar - SR)*URstar.m - SR*UR.m;
    flux.e = FR.e + S_Rstar*URsstar.e - (S_Rstar - SR)*URstar.e - SR*UR.e;
    flux.B = FR.B + S_Rstar*URsstar.B - (S_Rstar - SR)*URstar.B - SR*UR.B;

    return flux;

  }
  else if ((S_Lstar <= 0.0)&&(0.0<=SM))
  {
    PrimState  qLsstar;
    ConsState  ULsstar;
    qLsstar.d = rho_Lstar;
    qLsstar.v = uss;
    qLsstar.B = Bss;
    Real eint = rhoEssL - half*dot(qLsstar.B,qLsstar.B) - half*qLsstar.d*dot(qLsstar.v,qLsstar.v);
    qLsstar.p = m_eos.computePressure(rho_Lstar, eint);
    ULsstar   = MHD::primitiveToConservative(qLsstar, m_eos);

    PrimState  qLstar;
    ConsState  ULstar;
    qLstar.d = rho_Lstar;
    qLstar.v = ustarL;
    qLstar.v(idir) = SM;
    qLstar.B = BstarL;
    qLstar.B(idir) = Bn;
    eint = E_Lstar - half*dot(qLstar.B,qLstar.B) - half*qLstar.d*dot(qLstar.v,qLstar.v);
    qLstar.p = m_eos.computePressure(rho_Lstar, eint);

    ULstar       = MHD::primitiveToConservative(qLstar, m_eos);
    ConsState UL = MHD::primitiveToConservative(q_L   , m_eos);
    ConsState FL = MHD::template Flux<idir>(q_L, m_eos);

    flux.d = FL.d + S_Lstar*ULsstar.d - (S_Lstar - SL)*ULstar.d - SL*UL.d;
    flux.m = FL.m + S_Lstar*ULsstar.m - (S_Lstar - SL)*ULstar.m - SL*UL.m;
    flux.e = FL.e + S_Lstar*ULsstar.e - (S_Lstar - SL)*ULstar.e - SL*UL.e;
    flux.B = FL.B + S_Lstar*ULsstar.B - (S_Lstar - SL)*ULstar.B - SL*UL.B;

    return flux;

  }

  return flux;
}

}}
