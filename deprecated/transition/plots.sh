#!/bin/bash

python3 plot_speed_evolution.py 00 5 32
python3 plot_speed_evolution.py 01 5 26
python3 plot_speed_evolution.py 02 21 36
python3 plot_speed_evolution.py 03 6 30
python3 plot_speed_evolution.py 04 7 24
python3 plot_speed_evolution.py 05 8 27
python3 plot_speed_evolution.py 06 9 43
python3 plot_speed_evolution.py 07 12 39
python3 plot_speed_evolution.py 08 1 36

for i in {9..30}
do
  if [  "$i" -lt "10" ]
  then
    python3 plot_speed_evolution.py 0$i 5 18
  else
    python3 plot_speed_evolution.py $i 5 18
  fi
done
