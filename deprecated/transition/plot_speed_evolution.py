import numpy as np
import matplotlib.pyplot as plt
import h5py
import os
import sys
import statistics


import os

import sys


time_list=[]
speed_list=[]
for root, dirs, files in os.walk(".", topdown=False):
   for name in files:
      if name.startswith(("output_convection_QA_0."+sys.argv[1]+"_time_")):
          print(name)
          f = h5py.File(name)
          mz = f["mz"][:]
          d  = f["d"][:]
          uz = mz/d

          if (f["time"][()]> 0.):
              time_list.append(f["time"][()])
              speed_list.append(np.sum(abs(uz)))

#sort the speed list with respect to time
zipped_lists = zip(time_list, speed_list)
sorted_zipped_lists = sorted(zipped_lists)
sorted_speed_list = [element for _, element in sorted_zipped_lists]
time_list.sort()

#compute the log of speeds
log_sorted_speed_list=np.log10(sorted_speed_list)
#amount of data
N=len(time_list)
time_index = range(N)


plt.xlabel('Time')
plt.ylabel('total abs Vertical speed')
plt.title(r"Growth of the instability")

x_ticks = np.arange(0, len(time_index), 5)
plt.xticks(x_ticks)

plt.plot(time_index ,log_sorted_speed_list, label=' ',color='red')

plt.legend()
plt.grid()
plt.savefig('./growth'+str(sys.argv[1])+'.png')

index1=int(sys.argv[2])
plt.axvline(x=time_index[index1])
plt.savefig('./growth'+str(sys.argv[1])+'.png')

index2=int(sys.argv[3])


plt.axvline(x=time_index[index2])
plt.savefig('./growth'+str(sys.argv[1])+'.png')


#Calcul du growth rate
growth_rate=(np.log(sorted_speed_list[int(index2)]) - np.log(sorted_speed_list[int(index1)]))/(time_list[int(index2)]-time_list[int(index1)])

file_object = open('growth_rates_list.txt', 'a')
file_object.write(str(sys.argv[1])+"    "+str(growth_rate)+'\n')
file_object.close()
