import numpy as np
import sys
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text', usetex=True)
# Physical parameters
HT =-0.4#0.0#-0.2

g = -2.0
Cv = 1.0
rho_ground = 10.0

gamma = 1.4

kx = np.pi
ky = 0.0
kz = np.pi

temp_gradient = -20.0
temp_ground = 100

B0 = 0.3
z = 0.50

# Checks
if (HT > 0.0 or g > 0):
    print("error")
    sys.exit()
#Deduced parameters

k2 = kx*kx + ky*ky + kz*kz
beta = g/( Cv*temp_gradient*(gamma-1.0) )

T = temp_ground + z * temp_gradient
rho = rho_ground*(T/temp_ground)**(beta-1.0)
dlogPdz = beta*temp_gradient/T

hp = - 1.0/dlogPdz

Nabla_ad = (gamma-1.0)/gamma
Nabla_T = 1.0/beta

# Some prints
print(" Parameters :")

print( "HT =",HT)
print( "g = ",g)
print( "Cv =", Cv)
print( "rho_ground =", rho_ground)

print( "gamma =", gamma)

print( "kx =", kx)
print( "ky =", ky)
print( "kz =", kz)

print( "temp_gradient =", temp_gradient)
print( "temp_ground =", temp_ground)

print( "B0 =", B0)
print( "z =", z)
print("hp =",hp)
#Criterions
g=-g
print("Schwarzchild, ", Nabla_T-Nabla_ad)

QAmin = 0.0
QAmax = 0.30

N = 1000

sign_a1 = np.zeros(N)
sign_a0 = np.zeros(N)

list_a1 = np.zeros(N)
list_a0 = np.zeros(N)

unstable_a1=np.zeros(N)
unstable_a0=np.zeros(N)


roots_squared = np.zeros(N)
QA_list = np.zeros(N)

a1_0 = 0
a0_0 = 0
omega2_0=0

for k in range(N-1,-1,-1):

    QA =  -(QAmin + (QAmax-QAmin)*k*1.0/(N-1))

    a2 = -HT-QA
    a1 = HT*QA + kx*kx*(B0*B0/rho - (g/(k2*hp))*(Nabla_T-Nabla_ad) )
    a0 = kx*kx*( (g/(k2*hp))*(Nabla_T - Nabla_ad)*QA - (B0*B0/rho)*HT)

    a =[1.0, a2, a1, a0]


    roots = np.roots(a)
    largest_root = max(np.real(roots))
    #print(largest_root)

    if (k==N-1):
        omega2_0 = largest_root**2

    sign_a1[k] = np.sign(a1)
    sign_a0[k] = np.sign(a0)

    if (a1 < 0):
        unstable_a1[k] = 1
    else:
        unstable_a1[k] = 0

    if (a0 < 0):
        unstable_a0[k] = 1
    else:
        unstable_a0[k] = 0

    list_a1[k] = a1
    list_a0[k] = a0

    if (largest_root < 1e-10):
        roots_squared[k] =-1
    else :
        roots_squared[k] = (largest_root**2)
    QA_list[k] = QA

k_Q=0
while list_a0[k_Q]>0:
    k_Q+=1
Q_trans = -QA_list[k_Q]

fig = plt.figure()
ax = fig.add_subplot(111, label="1")
ax2 = fig.add_subplot(111, label="2", frame_on=False)


ax.plot(-QA_list[0:k_Q] , np.sqrt(roots_squared[0:k_Q]),label='Analytical '+r'$\omega$'+', '+r'$a_1 <0 \ \& \ a_0 >0$', color='blue', linewidth=1.75)
ax.plot(-QA_list[k_Q:] , np.sqrt(roots_squared[k_Q:]),label='Analytical '+r'$\omega$'+', '+r'$a_1 <0 \ \& \ a_0 <0$', color='red', linewidth=1.75)

ax.set_ylabel('Analytical '+r'$\omega$', color="black")
ax.set_xlabel(r'$-Q_A$', color="black")



data = np.loadtxt("./growth_rates_list28.txt")
n=data.shape[0]

x=np.zeros(n)
y=np.zeros(n)
for i in range(31):
    x[i] = data[i][0]
    y[i] = data[i][1]

ax2.scatter(x, y, color="black",label ='Numerical '+r'$\omega$',s=3.0)


ax2.axes.get_xaxis().set_visible(False)
ax2.yaxis.tick_right()
ax2.set_ylabel('Numerical '+r'$\omega$', color="black")
ax2.yaxis.set_label_position('right')
ax2.tick_params(axis='y', colors="black")
ax2.legend(loc='lower right')
ax.legend()
ax.set_title('Adiabatic to Diabatic branch transition')
plt.savefig('out_instability28.png',figsize=(16, 12), dpi=200)

fig.clf()
plt.close()
