#pragma once

#include "HydroBaseKernel.hpp"
#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroProblem.hpp"
#include "HydroSolver.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"

#include <Kokkos_Core.hpp>

namespace hydro
{

class GravityStepKernel : public BaseKernel
{
public:
    using Super = BaseKernel;

    using Euler = MHDSystem;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;
    using RealVector = RealVector3d;

    using IntVector = IntVectorNd< three_d >;
    using TeamPolicy = Kokkos::TeamPolicy< Kokkos::IndexType< Int > >;
    using Team = typename TeamPolicy::member_type;
    static constexpr int ghostDepth = 0;

    GravityStepKernel( const Params& params, const UniformGrid& grid,
                       const Array3d& u, const Array3d& q, Real dt )
        : m_u( u )
        , m_q( q )
        , m_g( params.hydro.g )
        , m_grid( grid )
        , m_dt( dt )
    {
    }

    KOKKOS_INLINE_FUNCTION
    void operator()( const Team& team ) const
    {
        using namespace constants;

        // From given team compute coordinates of cell (0, jy, jz).
        const IntVector coord0 = Super::teamToCoord( team, m_grid, ghostDepth );
        // Get linear index from cell (0, jy, jz).
        const Int j0 = m_grid.coordToIndex( coord0 );

        const auto vec_loop = Super::ArkTeamVectorRange(
            team, m_grid.m_ghostWidths[ IX ] - ghostDepth,
            m_grid.m_nbCells[ IX ] + m_grid.m_ghostWidths[ IX ] + ghostDepth );
        Kokkos::parallel_for( Kokkos::ThreadVectorRange( team, vec_loop.start, vec_loop.end ),
                              [ & ]( const Int jx ) {
                                  const Int j = j0 + jx;
                                  ConsState u_j = Super::getCons( m_u, j );
                                  const PrimState q_j = Super::getPrim( m_q, j );
                                  for ( int idim = 0; idim < three_d ; ++idim )
                                  {
                                      const Real dt_rho_g = m_dt * q_j.d * m_g[ idim ];
                                      u_j.m( idim ) += dt_rho_g;
                                      u_j.e += dt_rho_g * q_j.v( idim );
                                  }
                                  Super::set( m_u, j, u_j );
                              } );
    }

    const Array3d m_u;
    const ConstArray3d m_q;
    const RealVector m_g;
    const UniformGrid m_grid;
    const Real m_dt;
};

} // namespace hydro
