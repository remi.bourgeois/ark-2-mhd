#pragma once

#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"

#include <cmath>

namespace hydro { namespace riemann
{

class Hllc
{
public:
    using Euler           = MHDSystem;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState       = typename Euler::ConsState;
    using PrimState       = typename Euler::PrimState;

    Hllc(const Params& params, const EquationOfState& eos);

    //Hllc(const Hllc& x) = default;

    //Hllc(Hllc&& x) = default;

    //~Hllc() = default;

    //Hllc& operator=(const Hllc& x) = default;

   //Hllc& operator=(Hllc&& x) = default;

    template<int idir>
    KOKKOS_FORCEINLINE_FUNCTION
    ConsState operator()(const PrimState& q_L, const PrimState& q_R, std::integral_constant<int, idir> idir_tag, const int& side, bool st_powell) const;

private:
    const EquationOfState m_eos;
};

inline
Hllc::Hllc(const Params&, const EquationOfState& eos)
    : m_eos {eos}
{
}

template<int idir>
KOKKOS_FORCEINLINE_FUNCTION
typename Hllc::ConsState
Hllc::operator()(const PrimState& q_L, const PrimState& q_R, std::integral_constant<int, idir> idir_tag, const int& side, bool st_powell) const
{
    using namespace constants;

    const Real un_L = q_L.v( idir_tag );
    const Real un_R = q_R.v( idir_tag );

    const Real c_L = Euler::computeSpeedOfSound(q_L, m_eos);
    const Real c_R = Euler::computeSpeedOfSound(q_R, m_eos);

    const Real S_L = std::fmin(un_L, un_R) - std::fmax(c_L, c_R);
    const Real S_R = std::fmax(un_L, un_R) + std::fmax(c_L, c_R);

    const Real rc_L = q_L.d*(S_L-un_L);
    const Real rc_R = q_R.d*(S_R-un_R);

    // Compute acoustic star states
    const Real ustar = (q_R.p-q_L.p+rc_L*un_L-rc_R*un_R)/(rc_L-rc_R);
    const Real pstar = half*(q_L.p+q_R.p+rc_L*(ustar-un_L)+rc_R*(ustar-un_R));

    Real S;
    PrimState q;
    if ( ustar > 0 )
    {
        S = S_L;
        q = q_L;
    }
    else
    {
        S = S_R;
        q = q_R;
    }

    const Real un = q.v( idir_tag );
    const Real eint = m_eos.computeInternalEnergy(q.d, q.p);
    const Real ekin = Euler::computeKineticEnergy(q);
    const Real etot = eint + ekin;

    Real un_o;
    Real ptot_o;
    if ( S_L*S_R > zero )
    {
        un_o = un;
        ptot_o = q.p;
    }
    else
    {
        un_o = ustar;
        ptot_o = pstar;
    }
    const Real d_o = (S-un)/(S-un_o) * q.d;
    const Real etot_o = (S-un)/(S-un_o) * etot + (ptot_o*un_o-q.p*un)/(S-ustar);

    ConsState flux;
    flux.d = d_o * un_o;
    flux.e = (etot_o + ptot_o) * un_o;
    flux.m = (d_o * un_o) * q.v;
    flux.m(idir_tag) = d_o * un_o * un_o + ptot_o;
    flux.B(IX) = 0.0;
    flux.B(IY) = 0.0;
    flux.B(IZ) = 0.0;

    return flux;
}

}}
