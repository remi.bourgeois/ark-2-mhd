#pragma once

#include "HydroBaseKernel.hpp"
#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"
#include "ars/HydroRiemannAllRegime.hpp"
#include "ars/HydroRiemannAllRegime_dev.hpp"
#include "ars/MHDRiemann_AllRegime_3W.hpp"
#include "ars/MHDRiemann_AllRegime_5W.hpp"
#include "ars/MHDRiemann_AllRegime_5W_sophisticated.hpp"
#include "ars/MHDRiemann_Bouchut_3W.hpp"
#include "ars/MHDRiemann_Bouchut_5W.hpp"
#include "ars/HydroRiemannHllc.hpp"
#include "ars/HydroRiemannHlld.hpp"
#include "ars/HydroRiemannRusanov.hpp"

#include <Kokkos_Core.hpp>

namespace hydro
{

template < class RiemannSolver >
class FluxesAndUpdateKernel : public BaseKernel
{
public:
    using Super = BaseKernel;

    using MHD = MHDSystem;
    using EquationOfState = typename MHD::EquationOfState;
    using ConsState = typename MHD::ConsState;
    using PrimState = typename MHD::PrimState;
    using RealVector = RealVector3d;
    using BoolArray = BoolArrayDyn;

    using IntVector = IntVectorNd< three_d >;
    using TeamPolicy = Kokkos::TeamPolicy< Kokkos::IndexType< Int > >;
    using Team = typename TeamPolicy::member_type;
    static constexpr int ghostDepth = 0;

    FluxesAndUpdateKernel( const Params& params, const UniformGrid& grid,
                           const Array3d& u, const Array3d& q,   const Kokkos::Array<Array3d, 2*three_d>& qr, BoolArray& to_be_recomputed, bool fallback, Real dt )
        : m_u( u )
        , m_cu( u )
        , m_q( q )
        , m_qr( qr )
        , m_grid( grid )
        , m_eos( params.thermo )
        , m_riemann_solver( params, m_eos )
        , m_prim_limiting (params.run.prim_limiting)
        , m_powell_st_when_negative_eint(params.run.powell_st_when_negative_eint)
        , m_eint_treshold_fix(params.run.eint_treshold_fix)
        , m_powell_st_when_low_plasma_beta(params.run.powell_st_when_low_plasma_beta)
        , m_to_be_recomputed(to_be_recomputed)
        , m_fallback(fallback)
        , m_dt( dt )

    {
    }

    KOKKOS_INLINE_FUNCTION
    void operator()( const Team& team ) const
    {
        using namespace constants;

        // From given team compute coordinates of cell (0, jy, jz).
        const IntVector coord0 = Super::teamToCoord( team, m_grid, ghostDepth );
        // Get linear index from cell (0, jy, jz).
        const Int j0 = m_grid.coordToIndex( coord0 );

        const auto vec_loop = Super::ArkTeamVectorRange(
            team, m_grid.m_ghostWidths[ IX ] - ghostDepth,
            m_grid.m_nbCells[ IX ] + m_grid.m_ghostWidths[ IX ] + ghostDepth );
        Kokkos::parallel_for( Kokkos::ThreadVectorRange( team, vec_loop.start, vec_loop.end ),
                              [ & ]( const Int jx )
                              {

                                const Int j = j0 + jx;

                                PrimState q_j;
                                ConsState u_j;
                                bool st_powell;
                                Vector<three_d, Real> ustarL, ustarR;


                                if (m_fallback)
                                {
                                  if (m_to_be_recomputed(j))
                                  {
                                    //printf("in fallback");

                                    q_j = Super::getPrim( m_q, j );
                                    // re-initialize the value from state n
                                    u_j =  MHD::primitiveToConservative(q_j, m_eos);
                                    Vector<three_d, Real> B_L, B_R;

                                    B_L(IX) = Super::getPrim( m_qr[0+2*IX], j).B(IX);
                                    B_R(IX) = Super::getPrim( m_qr[1+2*IX], j).B(IX);

                                    B_L(IY) = Super::getPrim( m_qr[0+2*IY], j).B(IY);
                                    B_R(IY) = Super::getPrim( m_qr[1+2*IY], j).B(IY);

                                    B_L(IZ) = Super::getPrim( m_qr[0+2*IZ], j).B(IZ);
                                    B_R(IZ) = Super::getPrim( m_qr[1+2*IZ], j).B(IZ);

                                    st_powell = true;

                                    updateAlongFace( Tags::DirX, Tags::SideL, j, u_j, st_powell, ustarL );
                                    updateAlongFace( Tags::DirX, Tags::SideR, j, u_j, st_powell, ustarR );
                                    //u_j.B -= (B_R(IX) - B_L(IX))*m_dt*m_grid.m_invdl[IX]*q_j.v;
                                    u_j.B -= (B_R(IX) - B_L(IX))*m_dt*m_grid.m_invdl[IX]*0.5*(ustarL+ustarR);


                                    updateAlongFace( Tags::DirY, Tags::SideL, j, u_j, st_powell, ustarL );
                                    updateAlongFace( Tags::DirY, Tags::SideR, j, u_j, st_powell, ustarR );
                                    //u_j.B -= (B_R(IY) - B_L(IY))*m_dt*m_grid.m_invdl[IY]*q_j.v;
                                    u_j.B -= (B_R(IY) - B_L(IY))*m_dt*m_grid.m_invdl[IY]*0.5*(ustarL+ustarR);


                                    updateAlongFace( Tags::DirZ, Tags::SideL, j, u_j, st_powell, ustarL );
                                    updateAlongFace( Tags::DirZ, Tags::SideR, j, u_j, st_powell, ustarR );
                                    //u_j.B -= (B_R(IZ) - B_L(IZ))*m_dt*m_grid.m_invdl[IZ]*q_j.v;
                                    u_j.B -= (B_R(IZ) - B_L(IZ))*m_dt*m_grid.m_invdl[IZ]*0.5*(ustarL+ustarR);
                                    Super::set( m_u, j, u_j );

                                  }
                                }
                                else
                                {
                                  u_j = Super::getCons( m_cu, j );
                                  q_j = Super::getPrim( m_q, j );

                                  st_powell = false;
                                  Real beta = 1.0;
                                  if (m_powell_st_when_low_plasma_beta)
                                  {
                                    beta = q_j.p/MHD::computeMagneticEnergy(q_j);

                                    if (beta < 0.001) st_powell = true;
                                  }
                                  //
                                  updateAlongFace( Tags::DirX, Tags::SideL, j, u_j, st_powell, ustarL);
                                  updateAlongFace( Tags::DirX, Tags::SideR, j, u_j, st_powell, ustarR);
                                  updateAlongFace( Tags::DirY, Tags::SideL, j, u_j, st_powell, ustarL);
                                  updateAlongFace( Tags::DirY, Tags::SideR, j, u_j, st_powell, ustarR);
                                  updateAlongFace( Tags::DirZ, Tags::SideL, j, u_j, st_powell, ustarL);
                                  updateAlongFace( Tags::DirZ, Tags::SideR, j, u_j, st_powell, ustarR);

                                  if ( (m_powell_st_when_negative_eint)||(m_eint_treshold_fix) )
                                  {

                                    // Check positivity
                                    const Real emag = MHD::computeMagneticEnergy(u_j);
                                    const Real ecin = MHD::computeKineticEnergy(u_j);
                                    const Real eint = u_j.e - emag - ecin;

                                    if (eint <= 0.0)
                                    {
                                      if (m_powell_st_when_negative_eint)
                                      {
                                        // re-initialize the value
                                        u_j =  MHD::primitiveToConservative(q_j, m_eos);

                                        Vector<three_d, Real> B_L, B_R;

                                        B_L(IX) = Super::getPrim( m_qr[0+2*IX], j).B(IX);
                                        B_R(IX) = Super::getPrim( m_qr[1+2*IX], j).B(IX);

                                        B_L(IY) = Super::getPrim( m_qr[0+2*IY], j).B(IY);
                                        B_R(IY) = Super::getPrim( m_qr[1+2*IY], j).B(IY);

                                        B_L(IZ) = Super::getPrim( m_qr[0+2*IZ], j).B(IZ);
                                        B_R(IZ) = Super::getPrim( m_qr[1+2*IZ], j).B(IZ);

                                        st_powell = true;

                                        updateAlongFace( Tags::DirX, Tags::SideL, j, u_j, st_powell, ustarL );
                                        updateAlongFace( Tags::DirX, Tags::SideR, j, u_j, st_powell, ustarR );
                                        u_j.B -= (B_R(IX) - B_L(IX))*m_dt*m_grid.m_invdl[IX]*q_j.v;
                                        //u_j.B -= (B_R(IX) - B_L(IX))*m_dt*m_grid.m_invdl[IX]*0.5*(ustarL+ustarR);


                                        updateAlongFace( Tags::DirY, Tags::SideL, j, u_j, st_powell, ustarL );
                                        updateAlongFace( Tags::DirY, Tags::SideR, j, u_j, st_powell, ustarR );
                                        u_j.B -= (B_R(IY) - B_L(IY))*m_dt*m_grid.m_invdl[IY]*q_j.v;
                                        //u_j.B -= (B_R(IY) - B_L(IY))*m_dt*m_grid.m_invdl[IY]*0.5*(ustarL+ustarR);


                                        updateAlongFace( Tags::DirZ, Tags::SideL, j, u_j, st_powell, ustarL );
                                        updateAlongFace( Tags::DirZ, Tags::SideR, j, u_j, st_powell, ustarR );
                                        u_j.B -= (B_R(IZ) - B_L(IZ))*m_dt*m_grid.m_invdl[IZ]*q_j.v;
                                        //u_j.B -= (B_R(IZ) - B_L(IZ))*m_dt*m_grid.m_invdl[IZ]*0.5*(ustarL+ustarR);
                                      }
                                      else if (m_eint_treshold_fix)
                                      {
                                        u_j.e += -eint + 1.0e-5;
                                      }
                                    }
                                  }

                                  if ( (m_powell_st_when_low_plasma_beta)&&(beta<0.001) )
                                  {

                                    Vector<three_d, Real> B_L, B_R;

                                    B_L(IX) = Super::getPrim( m_qr[0+2*IX], j).B(IX);
                                    B_R(IX) = Super::getPrim( m_qr[1+2*IX], j).B(IX);

                                    B_L(IY) = Super::getPrim( m_qr[0+2*IY], j).B(IY);
                                    B_R(IY) = Super::getPrim( m_qr[1+2*IY], j).B(IY);

                                    B_L(IZ) = Super::getPrim( m_qr[0+2*IZ], j).B(IZ);
                                    B_R(IZ) = Super::getPrim( m_qr[1+2*IZ], j).B(IZ);

                                    u_j.B -= (B_R(IX) - B_L(IX))*m_dt*m_grid.m_invdl[IX]*q_j.v;
                                    u_j.B -= (B_R(IY) - B_L(IY))*m_dt*m_grid.m_invdl[IY]*q_j.v;
                                    u_j.B -= (B_R(IZ) - B_L(IZ))*m_dt*m_grid.m_invdl[IZ]*q_j.v;

                                  }

                                  Super::set( m_u, j, u_j );

                                }


                              } );
    }

    template < int idir, int iside, std::enable_if_t< ( idir >= three_d ), int > = 0 >
    KOKKOS_FORCEINLINE_FUNCTION void updateAlongFace( std::integral_constant< int, idir >,
                                                      std::integral_constant< int, iside >, Int,
                                                      ConsState& , bool) const
    {
    }

    template < int idir, int iside, std::enable_if_t< ( idir < three_d ), int > = 0 >
    KOKKOS_FORCEINLINE_FUNCTION void
    updateAlongFace( std::integral_constant< int, idir > idir_tag,
                     std::integral_constant< int, iside > iside_tag, Int j, ConsState& u_j, bool st_powell, Vector<three_d, Real>& ustar_out) const
    {
        using namespace constants;

        const Int j_L = iside == 0 ? j - m_grid.m_strides[ idir ] : j;
        const Int j_R = iside == 0 ? j : j + m_grid.m_strides[ idir ];

        PrimState q_L, q_R;

        q_L = Super::getPrim( m_qr[1+2*idir], j_L );
        q_R = Super::getPrim( m_qr[0+2*idir], j_R );

        const Real side = iside == 0 ? -one : +one;

        const ConsState flux = m_riemann_solver( q_L, q_R, idir_tag, side, st_powell, ustar_out);

        const Real sdtdSdV = side * m_dt * m_grid.getdSdV( j, idir_tag, iside_tag );
        u_j.d -= sdtdSdV * flux.d;
        u_j.m -= sdtdSdV * flux.m;
        u_j.e -= sdtdSdV * flux.e;
        u_j.B -= sdtdSdV * flux.B;

    }

    const Array3d m_u;
    const ConstArray3d m_cu;
    const ConstArray3d m_q;
    const Kokkos::Array<Array3d, 2*three_d> m_qr;
    const UniformGrid m_grid;
    const EquationOfState m_eos;
    const RiemannSolver m_riemann_solver;
    const bool m_prim_limiting;
    const bool m_powell_st_when_negative_eint;
    const bool m_eint_treshold_fix;
    const bool m_powell_st_when_low_plasma_beta;
    BoolArray  m_to_be_recomputed;
    bool m_fallback;
    Real m_dt;
};

} // namespace hydro
