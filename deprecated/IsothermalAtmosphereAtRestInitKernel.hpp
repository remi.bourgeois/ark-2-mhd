#pragma once

#include "IsothermalAtmosphereAtRestParams.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"
#include "HydroConstants.hpp"
#include "HydroParams.hpp"
#include "HydroProblem.hpp"
#include "HydroBaseKernel.hpp"
#include "MHDSystem.hpp"

namespace hydro { namespace problems
{

class IsothermalAtmosphereAtRestInitKernel : BaseKernel
{
    using Super           = BaseKernel;

    using Euler           = MHDSystem;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState       = typename Euler::ConsState;
    using PrimState       = typename Euler::PrimState;
    using RealVector      = RealVector3d;
    using VC              = typename Euler::VarCons;
    using VP              = typename Euler::VarPrim;
    static constexpr int nbvar = Euler::nbvar;

    using Array           = Array3d;
    using IntVector       = IntVectorNd<three_d>;

public:
    IsothermalAtmosphereAtRestInitKernel(const Params& params,
                                         const IsothermalAtmosphereAtRestParams& prob_params,
                                         Array u,
                                         const UniformGrid& grid)
        : m_params {params}
        , m_prob_params {prob_params}
        , m_eos {params.thermo}
        , m_u {u}
        , m_grid {grid}
        , Rstar {code_units::constants::Rstar_h}
    {
        Rstar = code_units::constants::Rstar_h / m_params.thermo.mmw;
    }

    static void apply(const Params& params,
                      const IsothermalAtmosphereAtRestParams& prob_params,
                      Array u,
                      const UniformGrid& grid)
    {

      IsothermalAtmosphereAtRestInitKernel kernel {params, prob_params, u, grid};

      Int start_y = 0;
      Int start_z = 0;

      Int end_y = grid.m_nbCells[IY]+2*grid.m_ghostWidths[IY];
      Int end_z = grid.m_nbCells[IZ]+2*grid.m_ghostWidths[IZ];

      Kokkos::MDRangePolicy<Kokkos::Rank<3>> policy ({0, start_y, start_z}, {1, end_y, end_z});
      Kokkos::parallel_for(policy, kernel);
    }

    KOKKOS_INLINE_FUNCTION
    Real phi(Int index) const
    {
        const RealVector OX{m_grid.getCellCenter(index)};
        Real phi{};
        for (Int idim = 0; idim < three_d; ++idim)
        {
            phi -= m_params.hydro.g[idim] * OX[idim];
        }
        return phi;
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(Int p, Int iy, Int iz) const
    {
        using namespace constants;

        if (p==0)
        {
            for (Int ix=m_grid.m_ghostWidths[IX];
                 ix<m_grid.m_nbCells[IX]+m_grid.m_ghostWidths[IX]; ++ix)
            {
                const IntVector coord {{ix, iy, iz}};
                const Int j {m_grid.coordToIndex(coord)};
                const IntVector coord_k {{ix-1, iy, iz}};
                const Int k {m_grid.coordToIndex(coord_k)};

                const ConsState u_k {getCons(m_u, k)};
                const PrimState q_k {Euler::conservativeToPrimitive(u_k, m_eos)};

                const Real rho_k {ix==m_grid.m_ghostWidths[IX] ? m_prob_params.pressure_bottom /(Rstar*m_prob_params.temperature) : q_k.d};

                PrimState q_j;
                q_j.d = rho_k * ((two * Rstar * m_prob_params.temperature + (phi(k) - phi(j)))/
                                        (two * Rstar * m_prob_params.temperature - (phi(k) - phi(j))));
                q_j.p = q_j.d * Rstar * m_prob_params.temperature;
                q_j.v( IX ) = zero;
                q_j.v( IY ) = zero;
                q_j.v( IZ ) = zero;
                q_j.B( IX ) = m_prob_params.Bnormal_bottom;
                q_j.B( IY ) = m_prob_params.Btrans_bottom;
                q_j.B( IZ ) = zero;

                const ConsState u_j {Euler::primitiveToConservative(q_j, m_eos)};
                set(m_u, j, u_j);
            }
        }
    }

    Params m_params;
    IsothermalAtmosphereAtRestParams m_prob_params;
    EquationOfState m_eos;
    Array m_u;
    UniformGrid m_grid;
    Real Rstar;
};

}}
