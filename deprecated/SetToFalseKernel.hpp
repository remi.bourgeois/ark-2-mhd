#pragma once

#include "HydroBaseKernel.hpp"
#include "HydroUniformGrid.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"

namespace hydro
{

class SetToFalseKernel : public BaseKernel
{
    using Super           = BaseKernel;

    using MHD           = MHDSystem;
    using EquationOfState = typename MHD::EquationOfState;

    using Grid       = UniformGrid;

    using BoolArray = BoolArrayDyn;

    using TeamPolicy = Kokkos::TeamPolicy<Kokkos::IndexType<Int>>;
    using Team       = typename TeamPolicy::member_type;
    static constexpr int ghostDepth = 2;

public:
    SetToFalseKernel(const Params& params, const Grid& grid, BoolArray& to_be_recomputed);

    static void apply(const Params& params, const Grid& grid,BoolArray& to_be_recomputed);

    KOKKOS_INLINE_FUNCTION
    void operator()(const Team& team) const;


    const Params m_params;
    const Grid m_grid;
    BoolArray  m_to_be_recomputed;

};

}
