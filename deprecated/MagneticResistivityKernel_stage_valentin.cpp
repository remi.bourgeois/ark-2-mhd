#pragma once

#include "HydroBaseKernel.hpp"
#include "HydroConstants.hpp"
#include "MHDSystem.hpp"
#include "HydroProblem.hpp"
#include "HydroSolver.hpp"
#include "HydroTypes.hpp"
#include "HydroUniformGrid.hpp"

#include <Kokkos_Core.hpp>

namespace hydro
{

class MagneticResistivityKernel : public BaseKernel
{
public:
    using Super = BaseKernel;

    using MHD = MHDSystem;
    using EquationOfState = typename MHD::EquationOfState;
    using ConsState = typename MHD::ConsState;
    using PrimState = typename MHD::PrimState;
    using RealVector = RealVector3d;

    using IntVector = IntVectorNd< three_d >;
    using TeamPolicy = Kokkos::TeamPolicy< Kokkos::IndexType< Int > >;
    using Team = typename TeamPolicy::member_type;
    static constexpr int ghostDepth = 2;

    MagneticResistivityKernel( const Params& params, const UniformGrid& grid,
                       const Array3d& u, const Array3d& q, Real dt )
        : m_u( u )
        , m_q( q )
        , m_grid( grid )
        , m_dt( dt )
        , m_D( params.hydro.resistivity_coefficient)
        , m_dl  {grid.m_dl}
        , m_splitted_diffusion_step( params.hydro.splitted_diffusion_step)
        , m_conservative_diffusion_update( params.hydro.conservative_diffusion_update)
        , m_x_start(grid.m_ghostWidths[ IX ] - ghostDepth)
        , m_x_end(grid.m_nbCells[ IX ] + grid.m_ghostWidths[ IX ] + ghostDepth)

    {
    }

    KOKKOS_INLINE_FUNCTION
    void operator()( const Team& team ) const
    {
        using namespace constants;

        // From given team compute linear index of coordinates of cell (0, jy, jz).
        const Int j0 = Super::teamToLinearIndex( team, m_grid, ghostDepth );

        Kokkos::parallel_for( hydro::TeamVectorRange( team, m_x_start, m_x_end ),
                              [ & ]( const Int jx ) {

                                  // Notation : La cellule centrale est q_j puis le reste est sous le format suivant :
                                  //            q_ _ _ _ les 3 derniers tirets représentent la coordonées i,j,k
                                  //            On passe à la cellule suivante en remplaçant le tiret avec p (plus) ou m (moins) dans la direction choisie
                                  //            Si on souhaite aller deux cases dans une direction, on mettra pp ou mm
                                  // Exemple : q_pp_ = u(i+1,j+1,k) ; q_pp__ = u(i+2,j,k) ; q_p_m = u(i+1,j,k-1) ; q_ppmmp = u(i+2,j-2,k+1)

                                  if (m_splitted_diffusion_step){

                                    const Int j = j0 + jx;
                                    ConsState u_j = Super::getCons( m_u, j );

                                    Int j_p__ = j + m_grid.m_strides[ IX ];
                                    ConsState u_p__ = Super::getCons( m_u, j_p__);

                                    Int j_m__ = j - m_grid.m_strides[ IX ];
                                    ConsState u_m__ = Super::getCons( m_u, j_m__);

                                    Int j__m_ = j - m_grid.m_strides[ IY ];
                                    ConsState u__m_ = Super::getCons( m_u, j__m_);

                                    Int j__p_ = j + m_grid.m_strides[ IY ];
                                    ConsState u__p_ = Super::getCons( m_u, j__p_);

                                    Int j___m = j - m_grid.m_strides[ IZ ];
                                    ConsState u___m = Super::getCons( m_u, j___m);

                                    Int j___p = j + m_grid.m_strides[ IZ ];
                                    ConsState u___p = Super::getCons( m_u, j___p);

                                    Int j_pp_ =  j + m_grid.m_strides[ IX ] + m_grid.m_strides[ IY] ;
                                    ConsState u_pp_ = Super::getCons( m_u, j_pp_ );

                                    Int j_pm_ =  j + m_grid.m_strides[ IX ] - m_grid.m_strides[ IY];
                                    ConsState u_pm_ = Super::getCons( m_u, j_pm_ );

                                    Int j_mp_ =  j - m_grid.m_strides[ IX ] + m_grid.m_strides[ IY] ;
                                    ConsState u_mp_ = Super::getCons( m_u, j_mp_ );

                                    Int j_mm_ =  j - m_grid.m_strides[ IX ] - m_grid.m_strides[ IY];
                                    ConsState u_mm_ = Super::getCons( m_u, j_mm_ );

                                    Int j_p_p =  j + m_grid.m_strides[ IX ] + m_grid.m_strides[ IZ] ;
                                    ConsState u_p_p = Super::getCons( m_u, j_p_p );

                                    Int j_p_m =  j + m_grid.m_strides[ IX ] - m_grid.m_strides[ IZ];
                                    ConsState u_p_m = Super::getCons( m_u, j_p_m );

                                    Int j_m_p =  j - m_grid.m_strides[ IX ] + m_grid.m_strides[ IZ] ;
                                    ConsState u_m_p = Super::getCons( m_u, j_m_p );

                                    Int j_m_m =  j - m_grid.m_strides[ IX ] - m_grid.m_strides[ IZ];
                                    ConsState u_m_m = Super::getCons( m_u, j_m_m );

                                    Int j__pp =  j + m_grid.m_strides[ IY ] + m_grid.m_strides[ IZ] ;
                                    ConsState u__pp = Super::getCons( m_u, j__pp );

                                    Int j__pm =  j + m_grid.m_strides[ IY ] - m_grid.m_strides[ IZ];
                                    ConsState u__pm = Super::getCons( m_u, j__pm );

                                    Int j__mp =  j - m_grid.m_strides[ IY ] + m_grid.m_strides[ IZ] ;
                                    ConsState u__mp = Super::getCons( m_u, j__mp );

                                    Int j__mm =  j - m_grid.m_strides[ IY ] - m_grid.m_strides[ IZ];
                                    ConsState u__mm = Super::getCons( m_u, j__mm );

                                    Int j_mm__ =  j - 2 * m_grid.m_strides[ IX ];
                                    ConsState u_mm__ = Super::getCons( m_u, j_mm__ );

                                    Int j_pp__ =  j + 2 * m_grid.m_strides[ IX ];
                                    ConsState u_pp__ = Super::getCons( m_u, j_pp__ );

                                    Int j__mm_ =  j - 2 * m_grid.m_strides[ IY ];
                                    ConsState u__mm_ = Super::getCons( m_u, j__mm_ );

                                    Int j__pp_ =  j + 2 * m_grid.m_strides[ IY ];
                                    ConsState u__pp_ = Super::getCons( m_u, j__pp_ );

                                    Int j___mm =  j - 2 * m_grid.m_strides[ IZ ];
                                    ConsState u___mm = Super::getCons( m_u, j___mm );

                                    Int j___pp =  j + 2 * m_grid.m_strides[ IZ ];
                                    ConsState u___pp = Super::getCons( m_u, j___pp );

                                    if (m_conservative_diffusion_update){
                                      // On calcule les nouvelles qqtés (met les formules là)
                                      Real Bx_flux_resistivite =  m_D * m_dt * ( (u_p__.B(IX) + u_m__.B(IX) - 2 * u_j.B(IX) ) / (m_dl[IX] * m_dl[IX]) +  (u__p_.B(IX) + u__m_.B(IX) - 2 * u_j.B(IX) ) / (m_dl[IY] * m_dl[IY]) + (u___p.B(IX) + u___m.B(IX) - 2 * u_j.B(IX) ) / (m_dl[IZ] * m_dl[IZ]) );
                                      Real By_flux_resistivite =  m_D * m_dt * ( (u_p__.B(IY) + u_m__.B(IY) - 2 * u_j.B(IY) ) / (m_dl[IX] * m_dl[IX]) +  (u__p_.B(IY) + u__m_.B(IY) - 2 * u_j.B(IY) ) / (m_dl[IY] * m_dl[IY]) + (u___p.B(IY) + u___m.B(IY) - 2 * u_j.B(IY) ) / (m_dl[IZ] * m_dl[IZ]) );
                                      Real Bz_flux_resistivite =  m_D * m_dt * ( (u_p__.B(IZ) + u_m__.B(IZ) - 2 * u_j.B(IZ) ) / (m_dl[IX] * m_dl[IX]) +  (u__p_.B(IZ) + u__m_.B(IZ) - 2 * u_j.B(IZ) ) / (m_dl[IY] * m_dl[IY]) + (u___p.B(IZ) + u___m.B(IZ) - 2 * u_j.B(IZ) ) / (m_dl[IZ] * m_dl[IZ]) );

                                      Real dE_flux_resistivite =  - m_D * m_dt * ( u_p__.B(IY) * ( ( u_pp__.B(IY) - u_j.B(IY)) / m_dl[IX] - ( u_pp_.B(IX) - u_pm_.B(IX)) / m_dl[IY] )  - u_p__.B(IZ) * (( u_p_p.B(IX) - u_p_m.B(IX)) / m_dl[IZ] - ( u_pp__.B(IZ) - u_j.B(IZ)) / m_dl[IX] ) - u_m__.B(IY) * ( ( u_j.B(IY) - u_mm__.B(IY)) / m_dl[IX] - ( u_mp_.B(IX) - u_mm_.B(IX)) / m_dl[IY] ) + u_m__.B(IZ) * (( u_m_p.B(IX) - u_m_m.B(IX)) / m_dl[IZ] - ( u_j.B(IZ) - u_mm__.B(IZ)) / m_dl[IX] ) ) / ( 4 * m_dl[IX]);

                                           dE_flux_resistivite += - m_D * m_dt * ( u__p_.B(IZ) * ( (u__pp_.B(IZ) - u_j.B(IZ)) / m_dl[IY] - ( u__pp.B(IY) - u__pm.B(IY) ) / m_dl[IZ]  ) - u__p_.B(IX) * ( ( u_pp_.B(IY) - u_mp_.B(IY)) / m_dl[IX] - ( u__pp_.B(IX) - u_j.B(IX)) / m_dl[IY] ) - u__m_.B(IZ) * ( (u_j.B(IZ) - u__mm_.B(IZ)) / m_dl[IY] - ( u__mp.B(IY) - u__mm.B(IY) ) / m_dl[IZ] ) + u__m_.B(IX) * ( ( u_pm_.B(IY) - u_mm_.B(IY) ) / m_dl[IX] - ( u_j.B(IX) - u__mm_.B(IX)) / m_dl[IY])) / (4 * m_dl[IY]);

                                           dE_flux_resistivite += - m_D * m_dt * ( u___p.B(IX) * ( (u___pp.B(IX) - u_j.B(IX)) / m_dl[IZ] - ( u_p_p.B(IZ) - u_m_p.B(IZ)) / m_dl[IX]) - u___p.B(IY) * ( ( u__pp.B(IZ) - u__mp.B(IZ) ) / m_dl[IY] - ( u___pp.B(IY) - u_j.B(IY)) / m_dl[IZ]) - u___m.B(IX) * ( (u_j.B(IX) - u___mm.B(IX)) / m_dl[IZ] - ( u_p_m.B(IZ) - u_m_m.B(IZ)) / m_dl[IX]) + u___m.B(IY) * ( ( u__pm.B(IZ) - u__mm.B(IZ) ) / m_dl[IY] - ( u_j.B(IY) - u___mm.B(IY)) / m_dl[IZ])) / ( 4 * m_dl[IZ]);


                                      u_j.B(IX) += Bx_flux_resistivite,
                                      u_j.B(IY) += By_flux_resistivite,
                                      u_j.B(IZ) += Bz_flux_resistivite,
                                      u_j.e += dE_flux_resistivite;

                                      Super::set( m_u, j, u_j );

                                    }
                                    else {
                                      Real Bx_flux_resistivite =  m_D * m_dt * ( (u_p__.B(IX) + u_m__.B(IX) - 2 * u_j.B(IX) ) / (m_dl[IX] * m_dl[IX]) +  (u__p_.B(IX) + u__m_.B(IX) - 2 * u_j.B(IX) ) / (m_dl[IY] * m_dl[IY]) + (u___p.B(IX) + u___m.B(IX) - 2 * u_j.B(IX) ) / (m_dl[IZ] * m_dl[IZ]) );
                                      Real By_flux_resistivite =  m_D * m_dt * ( (u_p__.B(IY) + u_m__.B(IY) - 2 * u_j.B(IY) ) / (m_dl[IX] * m_dl[IX]) +  (u__p_.B(IY) + u__m_.B(IY) - 2 * u_j.B(IY) ) / (m_dl[IY] * m_dl[IY]) + (u___p.B(IY) + u___m.B(IY) - 2 * u_j.B(IY) ) / (m_dl[IZ] * m_dl[IZ]) );
                                      Real Bz_flux_resistivite =  m_D * m_dt * ( (u_p__.B(IZ) + u_m__.B(IZ) - 2 * u_j.B(IZ) ) / (m_dl[IX] * m_dl[IX]) +  (u__p_.B(IZ) + u__m_.B(IZ) - 2 * u_j.B(IZ) ) / (m_dl[IY] * m_dl[IY]) + (u___p.B(IZ) + u___m.B(IZ) - 2 * u_j.B(IZ) ) / (m_dl[IZ] * m_dl[IZ]) );

                                      Real dE_non_conservatif = ( ( u_j.B(IX) + Bx_flux_resistivite ) * ( u_j.B(IX) + Bx_flux_resistivite ) +  ( u_j.B(IY) + By_flux_resistivite ) * ( u_j.B(IY) + By_flux_resistivite ) + ( u_j.B(IZ) + Bz_flux_resistivite ) * ( u_j.B(IZ) + Bz_flux_resistivite ) - u_j.B(IX) * u_j.B(IX) - u_j.B(IY) * u_j.B(IY) - u_j.B(IZ) * u_j.B(IZ)) / 2;

                                      Real Rot_x = ( u__p_.B(IZ) - u__m_.B(IZ)) / m_dl[IY] - ( u___p.B(IY) - u___m.B(IY) ) / m_dl[IZ];

                                      Real Rot_y = (u___p.B(IX) - u___m.B(IX)) / m_dl[IZ] - ( u_p__.B(IZ) - u_m__.B(IZ)) / m_dl[IX];

                                      Real Rot_z = ( u_p__.B(IY) - u_m__.B(IY)) / m_dl[IX] - ( u__p_.B(IX) - u__m_.B(IX)) / m_dl[IY];

                                           dE_non_conservatif += m_D * m_dt / 4 * ( Rot_x * Rot_x + Rot_y * Rot_y + Rot_z * Rot_z);


                                      u_j.B(IX) += Bx_flux_resistivite,
                                      u_j.B(IY) += By_flux_resistivite,
                                      u_j.B(IZ) += Bz_flux_resistivite,
                                      u_j.e += dE_non_conservatif;

                                      Super::set( m_u, j, u_j );

                                      }
                                  }
                                 else {

                                  const Int j = j0 + jx;
                                  PrimState q_j = Super::getPrim( m_q, j );
                                  ConsState u_j = Super::getCons( m_u, j );

                                  Int j_p__ = j + m_grid.m_strides[ IX ];
                                  PrimState q_p__ = Super::getPrim( m_q, j_p__);

                                  Int j_m__ = j - m_grid.m_strides[ IX ];
                                  PrimState q_m__ = Super::getPrim( m_q, j_m__);

                                  Int j__m_ = j - m_grid.m_strides[ IY ];
                                  PrimState q__m_ = Super::getPrim( m_q, j__m_);

                                  Int j__p_ = j + m_grid.m_strides[ IY ];
                                  PrimState q__p_ = Super::getPrim( m_q, j__p_);

                                  Int j___m = j - m_grid.m_strides[ IZ ];
                                  PrimState q___m = Super::getPrim( m_q, j___m);

                                  Int j___p = j + m_grid.m_strides[ IZ ];
                                  PrimState q___p = Super::getPrim( m_q, j___p);

                                  Int j_pp_ =  j + m_grid.m_strides[ IX ] + m_grid.m_strides[ IY] ;
                                  PrimState q_pp_ = Super::getPrim( m_q, j_pp_ );

                                  Int j_pm_ =  j + m_grid.m_strides[ IX ] - m_grid.m_strides[ IY];
                                  PrimState q_pm_ = Super::getPrim( m_q, j_pm_ );

                                  Int j_mp_ =  j - m_grid.m_strides[ IX ] + m_grid.m_strides[ IY] ;
                                  PrimState q_mp_ = Super::getPrim( m_q, j_mp_ );

                                  Int j_mm_ =  j - m_grid.m_strides[ IX ] - m_grid.m_strides[ IY];
                                  PrimState q_mm_ = Super::getPrim( m_q, j_mm_ );

                                  Int j_p_p =  j + m_grid.m_strides[ IX ] + m_grid.m_strides[ IZ] ;
                                  PrimState q_p_p = Super::getPrim( m_q, j_p_p );

                                  Int j_p_m =  j + m_grid.m_strides[ IX ] - m_grid.m_strides[ IZ];
                                  PrimState q_p_m = Super::getPrim( m_q, j_p_m );

                                  Int j_m_p =  j - m_grid.m_strides[ IX ] + m_grid.m_strides[ IZ] ;
                                  PrimState q_m_p = Super::getPrim( m_q, j_m_p );

                                  Int j_m_m =  j - m_grid.m_strides[ IX ] - m_grid.m_strides[ IZ];
                                  PrimState q_m_m = Super::getPrim( m_q, j_m_m );

                                  Int j__pp =  j + m_grid.m_strides[ IY ] + m_grid.m_strides[ IZ] ;
                                  PrimState q__pp = Super::getPrim( m_q, j__pp );

                                  Int j__pm =  j + m_grid.m_strides[ IY ] - m_grid.m_strides[ IZ];
                                  PrimState q__pm = Super::getPrim( m_q, j__pm );

                                  Int j__mp =  j - m_grid.m_strides[ IY ] + m_grid.m_strides[ IZ] ;
                                  PrimState q__mp = Super::getPrim( m_q, j__mp );

                                  Int j__mm =  j - m_grid.m_strides[ IY ] - m_grid.m_strides[ IZ];
                                  PrimState q__mm = Super::getPrim( m_q, j__mm );

                                  Int j_mm__ =  j - 2 * m_grid.m_strides[ IX ];
                                  PrimState q_mm__ = Super::getPrim( m_q, j_mm__ );

                                  Int j_pp__ =  j + 2 * m_grid.m_strides[ IX ];
                                  PrimState q_pp__ = Super::getPrim( m_q, j_pp__ );

                                  Int j__mm_ =  j - 2 * m_grid.m_strides[ IY ];
                                  PrimState q__mm_ = Super::getPrim( m_q, j__mm_ );

                                  Int j__pp_ =  j + 2 * m_grid.m_strides[ IY ];
                                  PrimState q__pp_ = Super::getPrim( m_q, j__pp_ );

                                  Int j___mm =  j - 2 * m_grid.m_strides[ IZ ];
                                  PrimState q___mm = Super::getPrim( m_q, j___mm );

                                  Int j___pp =  j + 2 * m_grid.m_strides[ IZ ];
                                  PrimState q___pp = Super::getPrim( m_q, j___pp );


                                  if (m_conservative_diffusion_update){
                                    // On calcule les nouvelles qqtés (met les formules là)
                                    Real Bx_flux_resistivite =  m_D * m_dt * ( (q_p__.B(IX) + q_m__.B(IX) - 2 * q_j.B(IX) ) / (m_dl[IX] * m_dl[IX]) +  (q__p_.B(IX) + q__m_.B(IX) - 2 * q_j.B(IX) ) / (m_dl[IY] * m_dl[IY]) + (q___p.B(IX) + q___m.B(IX) - 2 * q_j.B(IX) ) / (m_dl[IZ] * m_dl[IZ]) );
                                    Real By_flux_resistivite =  m_D * m_dt * ( (q_p__.B(IY) + q_m__.B(IY) - 2 * q_j.B(IY) ) / (m_dl[IX] * m_dl[IX]) +  (q__p_.B(IY) + q__m_.B(IY) - 2 * q_j.B(IY) ) / (m_dl[IY] * m_dl[IY]) + (q___p.B(IY) + q___m.B(IY) - 2 * q_j.B(IY) ) / (m_dl[IZ] * m_dl[IZ]) );
                                    Real Bz_flux_resistivite =  m_D * m_dt * ( (q_p__.B(IZ) + q_m__.B(IZ) - 2 * q_j.B(IZ) ) / (m_dl[IX] * m_dl[IX]) +  (q__p_.B(IZ) + q__m_.B(IZ) - 2 * q_j.B(IZ) ) / (m_dl[IY] * m_dl[IY]) + (q___p.B(IZ) + q___m.B(IZ) - 2 * q_j.B(IZ) ) / (m_dl[IZ] * m_dl[IZ]) );

                                    Real dE_flux_resistivite =  - m_D * m_dt * ( q_p__.B(IY) * ( ( q_pp__.B(IY) - q_j.B(IY)) / m_dl[IX] - ( q_pp_.B(IX) - q_pm_.B(IX)) / m_dl[IY] )  - q_p__.B(IZ) * (( q_p_p.B(IX) - q_p_m.B(IX)) / m_dl[IZ] - ( q_pp__.B(IZ) - q_j.B(IZ)) / m_dl[IX] ) - q_m__.B(IY) * ( ( q_j.B(IY) - q_mm__.B(IY)) / m_dl[IX] - ( q_mp_.B(IX) - q_mm_.B(IX)) / m_dl[IY] ) + q_m__.B(IZ) * (( q_m_p.B(IX) - q_m_m.B(IX)) / m_dl[IZ] - ( q_j.B(IZ) - q_mm__.B(IZ)) / m_dl[IX] ) ) / ( 4 * m_dl[IX]);

                                         dE_flux_resistivite += - m_D * m_dt * ( q__p_.B(IZ) * ( (q__pp_.B(IZ) - q_j.B(IZ)) / m_dl[IY] - ( q__pp.B(IY) - q__pm.B(IY) ) / m_dl[IZ]  ) - q__p_.B(IX) * ( ( q_pp_.B(IY) - q_mp_.B(IY)) / m_dl[IX] - ( q__pp_.B(IX) - q_j.B(IX)) / m_dl[IY] ) - q__m_.B(IZ) * ( (q_j.B(IZ) - q__mm_.B(IZ)) / m_dl[IY] - ( q__mp.B(IY) - q__mm.B(IY) ) / m_dl[IZ] ) + q__m_.B(IX) * ( ( q_pm_.B(IY) - q_mm_.B(IY) ) / m_dl[IX] - ( q_j.B(IX) - q__mm_.B(IX)) / m_dl[IY])) / (4 * m_dl[IY]);

                                         dE_flux_resistivite += - m_D * m_dt * ( q___p.B(IX) * ( (q___pp.B(IX) - q_j.B(IX)) / m_dl[IZ] - ( q_p_p.B(IZ) - q_m_p.B(IZ)) / m_dl[IX]) - q___p.B(IY) * ( ( q__pp.B(IZ) - q__mp.B(IZ) ) / m_dl[IY] - ( q___pp.B(IY) - q_j.B(IY)) / m_dl[IZ]) - q___m.B(IX) * ( (q_j.B(IX) - q___mm.B(IX)) / m_dl[IZ] - ( q_p_m.B(IZ) - q_m_m.B(IZ)) / m_dl[IX]) + q___m.B(IY) * ( ( q__pm.B(IZ) - q__mm.B(IZ) ) / m_dl[IY] - ( q_j.B(IY) - q___mm.B(IY)) / m_dl[IZ])) / ( 4 * m_dl[IZ]);


                                    u_j.B(IX) += Bx_flux_resistivite,
                                    u_j.B(IY) += By_flux_resistivite,
                                    u_j.B(IZ) += Bz_flux_resistivite,
                                    u_j.e += dE_flux_resistivite;

                                    Super::set( m_u, j, u_j );

                                  }
                                  else {
                                    Real Bx_flux_resistivite =  m_D * m_dt * ( (q_p__.B(IX) + q_m__.B(IX) - 2 * q_j.B(IX) ) / (m_dl[IX] * m_dl[IX]) +  (q__p_.B(IX) + q__m_.B(IX) - 2 * q_j.B(IX) ) / (m_dl[IY] * m_dl[IY]) + (q___p.B(IX) + q___m.B(IX) - 2 * q_j.B(IX) ) / (m_dl[IZ] * m_dl[IZ]) );
                                    Real By_flux_resistivite =  m_D * m_dt * ( (q_p__.B(IY) + q_m__.B(IY) - 2 * q_j.B(IY) ) / (m_dl[IX] * m_dl[IX]) +  (q__p_.B(IY) + q__m_.B(IY) - 2 * q_j.B(IY) ) / (m_dl[IY] * m_dl[IY]) + (q___p.B(IY) + q___m.B(IY) - 2 * q_j.B(IY) ) / (m_dl[IZ] * m_dl[IZ]) );
                                    Real Bz_flux_resistivite =  m_D * m_dt * ( (q_p__.B(IZ) + q_m__.B(IZ) - 2 * q_j.B(IZ) ) / (m_dl[IX] * m_dl[IX]) +  (q__p_.B(IZ) + q__m_.B(IZ) - 2 * q_j.B(IZ) ) / (m_dl[IY] * m_dl[IY]) + (q___p.B(IZ) + q___m.B(IZ) - 2 * q_j.B(IZ) ) / (m_dl[IZ] * m_dl[IZ]) );

                                    Real Rot_x = ( q__p_.B(IZ) - q__m_.B(IZ)) / m_dl[IY] - ( q___p.B(IY) - q___m.B(IY) ) / m_dl[IZ];
                                    Real Rot_y = (q___p.B(IX) - q___m.B(IX)) / m_dl[IZ] - ( q_p__.B(IZ) - q_m__.B(IZ)) / m_dl[IX];
                                    Real Rot_z = ( q_p__.B(IY) - q_m__.B(IY)) / m_dl[IX] - ( q__p_.B(IX) - q__m_.B(IX)) / m_dl[IY];
                                    //Real dE_non_conservatif = ( ( u_j.B(IX) + Bx_flux_resistivite ) * ( u_j.B(IX) + Bx_flux_resistivite ) +  ( u_j.B(IY) + By_flux_resistivite ) * ( u_j.B(IY) + By_flux_resistivite ) + ( u_j.B(IZ) + Bz_flux_resistivite ) * ( u_j.B(IZ) + Bz_flux_resistivite ) - u_j.B(IX) * u_j.B(IX) - u_j.B(IY) * u_j.B(IY) - u_j.B(IZ) * u_j.B(IZ)) / 2 ;
                                    Real dE_non_conservatif = ( q_j.B(IX) * Bx_flux_resistivite + q_j.B(IY) * By_flux_resistivite + q_j.B(IZ) * Bz_flux_resistivite );

                                         dE_non_conservatif += m_D * m_dt / 4 * ( Rot_x * Rot_x + Rot_y * Rot_y + Rot_z * Rot_z );

                                    u_j.B(IX) += Bx_flux_resistivite,
                                    u_j.B(IY) += By_flux_resistivite,
                                    u_j.B(IZ) += Bz_flux_resistivite,
                                    u_j.e += dE_non_conservatif;

                                    Super::set( m_u, j, u_j );

                                  }

                                  }


                              } );
    }

    const Array3d m_u;
    const ConstArray3d m_q;
    const UniformGrid m_grid;
    const Real m_dt;
    const Real m_D;
    const RealVector3d m_dl;
    const bool m_splitted_diffusion_step;
    const bool m_conservative_diffusion_update;

    Int m_x_start;
    Int m_x_end;


};

} // namespace hydro
