#pragma once

#include "HydroConstants.hpp"
#include "HydroParams.hpp"
#include "inih/INIReader.hpp"

namespace hydro { namespace problems
{

struct IsothermalAtmosphereAtRestParams
{
    IsothermalAtmosphereAtRestParams(const INIReader& reader)
    {
        pressure_bottom = reader.GetReal("problem", "pressure_bottom", pressure_bottom);
        Bnormal_bottom = reader.GetReal("problem", "Bnormal_bottom", Bnormal_bottom);
        Btrans_bottom = reader.GetReal("problem", "Btrans_bottom", Btrans_bottom);
        temperature     = reader.GetReal("problem", "temperature", temperature);
    }

    Real pressure_bottom = constants::hundred*constants::thousand;
    Real Bnormal_bottom = 0.0;
    Real Btrans_bottom  = 0.0;
    Real temperature = constants::three*constants::hundred;
};

}}
