#include "SetToFalseKernel.hpp"

#include "HydroBaseKernel.hpp"
#include "HydroUniformGrid.hpp"
#include "HydroParams.hpp"
#include "HydroTypes.hpp"

namespace hydro
{

SetToFalseKernel::SetToFalseKernel(const Params& params, const Grid& grid, BoolArray& to_be_recomputed)
    : m_params {params}
    , m_grid   {grid}
    , m_to_be_recomputed {to_be_recomputed}

{
}

void SetToFalseKernel::apply(const Params& params, const Grid& grid, BoolArray& to_be_recomputed)
{
    SetToFalseKernel kernel(params, grid, to_be_recomputed);

    const int league_size = Super::computeLeagueSize(grid.m_nbCells, ghostDepth);
    const int vector_length = TeamPolicy::vector_length_max();
    TeamPolicy policy {league_size, Kokkos::AUTO, vector_length};
    Kokkos::parallel_for("Set To False Kernel - TeamPolicy", policy, kernel);

}

KOKKOS_INLINE_FUNCTION
void SetToFalseKernel::operator()(const Team& team) const
{
    using namespace constants;

    // From given team compute coordinates of cell (0, jy, jz).
    const IntVector coord0 {Super::teamToCoord(team, m_grid, ghostDepth)};
    // Get linear index from cell (0, jy, jz).
    const int j0 {m_grid.coordToIndex(coord0)};

    const auto vec_loop = Super::ArkTeamVectorRange(team, m_grid.m_ghostWidths[IX]-ghostDepth, m_grid.m_nbCells[IX] + m_grid.m_ghostWidths[IX]+ghostDepth);
    Kokkos::parallel_for(Kokkos::ThreadVectorRange(team, vec_loop.start, vec_loop.end),
                         KOKKOS_LAMBDA(const int jx)
                         {
                              const int j{j0 + jx};
                              m_to_be_recomputed(j) = 0;
                         });
}

}  // hydro
